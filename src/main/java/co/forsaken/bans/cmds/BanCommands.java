/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.bans.cmds;

import java.util.Date;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.PunishmentFactory.PunishmentTypes;
import co.forsaken.api.factories.obj.Punishment;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class BanCommands {
  @Command(aliases = { "mute" }, desc = "Stops a user from talking", usage = "[user] [message...]", anyFlags = true, min = 2, max = -1)
  @CommandPermissions("bans.mute")
  public static void mute(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    String reason = args.getJoinedStrings(1);
    int hours = 0;
    int minutes = 15;
    Date expiry = new Date();
    expiry.setTime(System.currentTimeMillis() + (hours * 60 * 60 * 1000) + (minutes * 60 * 1000));
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      masochist.punish(user.getId(), PunishmentTypes.MUTE, reason, expiry);
      user.sendMessage("&7Successfully muted " + masochist.getName() + " until " + MessageUtils.getTimeFormater().format(expiry));
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas muted &6" + masochist.getFormattedName() + " &euntil &6"
          + MessageUtils.getTimeFormater().format(expiry) + " &efor \"&6" + reason + "&e\"", true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  @Command(aliases = { "tempban", "tb", "warn" }, desc = "Removed a user from the server temporarily", usage = "[user] <-d#> <-h#> <-m#> [message...]", anyFlags = true, min = 2, max = -1)
  @CommandPermissions("bans.tempban")
  public static void tempBan(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    String reason = args.getJoinedStrings(1);
    int days = 2;
    int hours = 0;
    int minutes = 0;
    if (args.hasFlag('d')) {
      days = args.getFlagInteger('d');
    }
    if (args.hasFlag('D')) {
      days = args.getFlagInteger('D');
    }
    if (args.hasFlag('h')) {
      hours = args.getFlagInteger('h');
    }
    if (args.hasFlag('H')) {
      hours = args.getFlagInteger('H');
    }
    if (args.hasFlag('m')) {
      minutes = args.getFlagInteger('m');
    }
    if (args.hasFlag('M')) {
      minutes = args.getFlagInteger('M');
    }
    String[] reasonArgs = reason.split(" ");
    reason = "";
    for (String s : reasonArgs) {
      if (!(s.toLowerCase().startsWith("-d") || s.toLowerCase().startsWith("-h") || s.toLowerCase().startsWith("-m"))) {
        if (!reason.isEmpty()) {
          reason += " ";
        }
        reason += s;
      }
    }
    Date expiry = new Date();
    expiry.setTime(System.currentTimeMillis() + (days * 24 * 60 * 60 * 1000) + (hours * 60 * 60 * 1000) + (minutes * 60 * 1000));
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      masochist.punish(user.getId(), PunishmentTypes.TEMP_BAN, reason, expiry);
      user.sendMessage("&7Successfully temp-banned " + masochist.getName() + " until " + MessageUtils.getTimeFormater().format(expiry));

      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas temp-banned &6" + masochist.getFormattedName() + " &euntil &6"
          + MessageUtils.getTimeFormater().format(expiry) + " &efor \"&6" + reason + "&e\"" + reason + "&e\"", true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  @Command(aliases = { "ban" }, desc = "Removed a user from the server", usage = "[user] [message...]", anyFlags = true, min = 2, max = -1)
  @CommandPermissions("bans.ban")
  public static void ban(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    String reason = args.getJoinedStrings(1);
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      masochist.punish(user.getId(), PunishmentTypes.BAN, reason, null);
      user.sendMessage("&7Successfully banned " + masochist.getName());
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas banned &6" + masochist.getFormattedName() + " &efor \"&6" + reason + "&e\"", true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  @Command(aliases = { "kick" }, desc = "Kicks a user", usage = "[user] [reason...]", anyFlags = true, min = 2, max = -1)
  @CommandPermissions("bans.kick")
  public static void kick(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    String reason = args.getJoinedStrings(1);
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      masochist.punish(user.getId(), PunishmentTypes.KICK, reason, null);
      user.sendMessage("&7Successfully kicked " + masochist.getName());
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas kicked &6" + masochist.getFormattedName() + " &efor \"&6" + reason + "&e\"", true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  @Command(aliases = { "unban" }, desc = "Removes a ban", usage = "[user]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions("bans.unban")
  public static void unban(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      if (args.hasFlag('c')) {
        ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "pex user " + masochist.getName() + " group set member");
        masochist.forceRemoveCredits(masochist.getCreditAmount(), "admin_unban_clear");
        masochist.addVoteTokens(0 - masochist.getVoteTokens());
        if (masochist.getPlayer() != null) {
          masochist.getPlayer().getInventory().clear();
        }
      }
      Punishment p = masochist.getCurrentPunishment();
      if (p != null && p.getType() != PunishmentTypes.MUTE) {
        p.setActive(false);
      }
      user.sendMessage("&7Successfully unbanned " + masochist.getName());
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas unbanned &6" + masochist.getFormattedName(), true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  @Command(aliases = { "unmute" }, desc = "Removes an active mute", usage = "[user]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions("bans.unmute")
  public static void unmute(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    try {
      User masochist = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      Punishment p = masochist.getCurrentPunishment();
      if (p != null && p.getType() == PunishmentTypes.MUTE) {
        p.setActive(false);
      }
      user.sendMessage("&7Successfully unmuted " + masochist.getName());
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(user.getFormattedName() + " &ehas unmuted &6" + masochist.getFormattedName(), true);
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }
}
