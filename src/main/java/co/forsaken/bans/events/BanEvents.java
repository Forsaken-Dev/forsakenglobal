/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.bans.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.PunishmentFactory.PunishmentTypes;
import co.forsaken.api.factories.obj.Punishment;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

public class BanEvents extends BaseListener {

  public BanEvents(ForsakenGlobal plugin) {
    super(plugin);
  }

  @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
  public void onPlayerChat(AsyncPlayerChatEvent event) {
    try {
      User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
      Punishment currentPunishment = user.getCurrentPunishment();
      if (((currentPunishment != null) && (currentPunishment.getType() == PunishmentTypes.MUTE)) || (_plugin.getServer().getMotd().equalsIgnoreCase("lobby") && user.isBanned())) {
        event.setCancelled(true);
        user.sendMessage(currentPunishment.toString());
        event.setMessage("");
      }
    } catch (Exception e) {
    }
  }

  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
  public void onPlayerLogin(PlayerLoginEvent event) {
    try {
      User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
      user.initPunishments();
      if (user.isBanned()) {
        if (!_plugin.getServer().getMotd().equalsIgnoreCase("lobby")) {
          event.disallow(PlayerLoginEvent.Result.KICK_BANNED, MessageUtils.format(user.getCurrentPunishment().toString(), true));
          event.setResult(PlayerLoginEvent.Result.KICK_BANNED);
        }
      }
    } catch (Exception e) {
      event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Could not load your player profile properly, please attempt to reconnect");
      event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
    }
  }

  @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
  public void onPlayerJoin(PlayerJoinEvent event) {
    try {
      final User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
      if (user.isBanned()) {
        if (_plugin.getServer().getMotd().equalsIgnoreCase("lobby")) {
          user.sendMessage(MessageUtils.format(user.getCurrentPunishment().toString(), true));
          user.sendMessage("&cYou can only &e/register &chere");
        } else {
          ForsakenGlobal.getInstance().sendPluginMessage("Connect", "lobby", user.getPlayer());
          ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), new Runnable() {
            public void run() {
              if (user.getPlayer() != null && user.getPlayer().isOnline()) {
                user.getPlayer().kickPlayer(MessageUtils.format(user.getCurrentPunishment().toString(), true));
              }
            }
          }, 30L);
        }
      }
    } catch (Exception e) {
    }
  }
}
