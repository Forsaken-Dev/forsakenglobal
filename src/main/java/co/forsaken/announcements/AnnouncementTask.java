/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.announcements;

import java.sql.SQLDataException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseResults;

public class AnnouncementTask implements Runnable {
  private ForsakenGlobal       _plugin;
  private int                  _taskId             = -1;
  private List<Announcement>   _announcements      = new ArrayList<Announcement>();
  private Map<String, Integer> _currentIndex       = new HashMap<String, Integer>();
  private static final int     NUM_MINUTES_BETWEEN = 2;

  private class Announcement {
    private String _message;
    private String _permissionNode;

    public Announcement(String message, String permissionNode) {
      _message = message;
      _permissionNode = permissionNode;
    }
  }

  public AnnouncementTask(ForsakenGlobal plugin) {
    _plugin = plugin;
    reload();
  }

  public void reload() {
    if (_taskId > -1) {
      _plugin.getServer().getScheduler().cancelTask(_taskId);
    }
    try {
      loadAnnouncements();
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (_announcements.size() > 0) {
      _taskId = _plugin.getServer().getScheduler().runTaskTimer(_plugin, this, 20 * 60 * NUM_MINUTES_BETWEEN, 20 * 60 * NUM_MINUTES_BETWEEN).getTaskId();
    }
    getNoLifeList();
    getTopVoters();
  }

  public void loadAnnouncements() throws Exception {
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT message, permission_node FROM forsaken_announcements WHERE server_id = ? OR server_id = 0;", _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    if (query != null && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        _announcements.add(new Announcement(query.getString(i, "message"), query.getString(i, "permission_node")));
      }
    }
  }

  public void run() {
    try {
      for (co.forsaken.api.factories.obj.User u : _plugin.getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
        if (!_currentIndex.containsKey(u.getName().toLowerCase())) {
          _currentIndex.put(u.getName().toLowerCase(), 0);
        }
        Announcement announcement = _announcements.get(_currentIndex.get(u.getName().toLowerCase()));
        while (!u.getPlayer().hasPermission(announcement._permissionNode)) {
          _currentIndex.put(u.getName().toLowerCase(), _currentIndex.get(u.getName().toLowerCase()) + 1 % _announcements.size());
          announcement = _announcements.get(_currentIndex.get(u.getName().toLowerCase()));
        }
        u.sendMessage("&8[&l&6TIP&r&8] &a" + announcement._message.replaceAll("%NO_LIFERS%", getNoLifeList()).replaceAll("%TOP_VOTERS%", getTopVoters()));
        _currentIndex.put(u.getName().toLowerCase(), _currentIndex.get(u.getName().toLowerCase()) + 1 % _announcements.size());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private ArrayList<OnlineUser> _onlineThisWeek   = new ArrayList<OnlineUser>();
  private long                  _lastOnlineUpdate = 0;

  private class OnlineUser {
    private String _name;
    private String _amount;

    public OnlineUser(String name, String amount) {
      _name = name;
      _amount = amount;
    }
  }

  private String getNoLifeList() {
    if (_onlineThisWeek.isEmpty() || System.currentTimeMillis() - _lastOnlineUpdate >= 1000 * 60 * 15) {
      _onlineThisWeek.clear();
      DatabaseResults query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT SUM(UNIX_TIMESTAMP(zus.logout_time)) - SUM(UNIX_TIMESTAMP(zus.login_time)) as onlineTime, zu.name as username FROM forsaken_user_sessions zus, forsaken_users zu WHERE zu.id = zus.user_id  AND YEAR(zus.logout_time) = YEAR(now()) AND WEEK(zus.logout_time) = WEEK(NOW()) GROUP BY zus.user_id ORDER BY onlineTime DESC LIMIT 5;");

      if (query != null && query.hasRows()) {
        for (int i = 0; i < query.rowCount(); i++) {
          double hours = Double.parseDouble("" + query.rawResults.get(i).get(0)) / Math.pow(60D, 2D);
          _onlineThisWeek.add(new OnlineUser(query.rawResults.get(i).get(1) + "", (new DecimalFormat("#.#")).format(hours)));
        }
      }
      _lastOnlineUpdate = System.currentTimeMillis();
    }
    String message = "&ePlayers longest online this week: ";
    int i = 0;
    for (OnlineUser s : _onlineThisWeek) {
      if (i > 0) {
        message += "&e, ";
      }
      message += "&6" + s._name + " &8[&7" + s._amount + "hrs&8]";
      i++;
    }
    return message;
  }

  private String getTopVoters() {
    String message = "&eTop Voters this month: ";
    List<OnlineUser> topVoters = getTopVoters(2);
    int i = 0;
    for (OnlineUser s : topVoters) {
      if (i > 0) {
        message += "&e, ";
      }
      message += "&6" + s._name + " &8[&7" + s._amount + "&8]";
      i++;
    }

    message += "&/&8[&l&6TIP&r&8] &eVote to receive a donation package or bonus voting tokens!!";
    return message;
  }

  private static List<OnlineUser> voters           = new ArrayList<OnlineUser>();
  private long                    _lastVoterUpdate = 0;

  public List<OnlineUser> getTopVoters(int amount) {
    if (voters.isEmpty() || System.currentTimeMillis() - _lastVoterUpdate >= 1000 * 60 * 15) {
      voters.clear();
      DatabaseResults query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT zu.name as name, COUNT( * ) AS count FROM `forsaken_vote_sites` vs, `forsaken_user_votes` uv, `forsaken_users` zu WHERE zu.id = uv.user_id AND (YEAR(`date`) = YEAR(NOW( )) AND MONTH(`date`) = MONTH(NOW( ))) GROUP BY uv.user_id  HAVING count >= (SELECT DISTINCT COUNT(*) as count FROM forsaken_user_votes WHERE (YEAR(`date`) = YEAR(NOW( )) AND MONTH(`date`) = MONTH(NOW( ))) group by user_id ORDER BY count DESC LIMIT ?, 1)  ORDER BY count DESC LIMIT 5",
              amount - 1);
      if (query != null && query.hasRows()) {
        int pos = 0;
        int posValue = 0;
        for (int i = 0; i < query.rowCount(); i++) {
          if (posValue != Integer.valueOf(query.rawResults.get(i).get(1) + "")) {
            posValue = Integer.valueOf(query.rawResults.get(i).get(1) + "");
            pos++;
          }
          try {
            voters.add(new OnlineUser(query.getString(i, "name"), query.rawResults.get(i).get(1) + ""));
          } catch (SQLDataException e) {
            e.printStackTrace();
          }
        }
      }
      _lastVoterUpdate = System.currentTimeMillis();
    }
    return voters;
  }
}
