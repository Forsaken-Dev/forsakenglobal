/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

public class ChatMessenger {

  private static Map<String, String>            _userReplyMap         = new HashMap<String, String>();
  private static Map<String, String>            _activeChannel        = new HashMap<String, String>();
  private static Map<String, ArrayList<String>> _connectedChannels    = new HashMap<String, ArrayList<String>>();
  public static final String                    CHANNEL_IRC_SUFFIX    = "-chat";
  private static final String                   USER_WEB_SUFFIX       = "-web";
  private static final String                   COLOUR_CHANNEL_BASE   = "7";
  private static final String                   COLOUR_CHANNEL_GLOBAL = "f";
  private static final String                   COLOUR_CHANNEL_STAFF  = "c";

  public static void initUser(String username) throws Exception {
    User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
    if (user == null) { return; }
    _connectedChannels.put(username.toLowerCase(), new ArrayList<String>());
    _connectedChannels.get(username.toLowerCase()).add("server");
    _connectedChannels.get(username.toLowerCase()).add("global");
    if (user.getPrimaryRank().isStaff()) {
      _connectedChannels.get(username.toLowerCase()).add("staff");
    }

    setActiveChannel(username, "global");
  }

  public static boolean setActiveChannel(String username, String channel) {
    if (channel.equalsIgnoreCase("server") || channel.equalsIgnoreCase("global") || channel.equalsIgnoreCase("staff")) {
      try {
        User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
        if (channel.equalsIgnoreCase("server")) {
          channel = user.getActiveServer().toLowerCase();
        } else if (channel.equals("staff") && !user.getPrimaryRank().isStaff()) { return false; }
        if (_connectedChannels.containsKey(user.getName().toLowerCase()) && _connectedChannels.get(user.getName().toLowerCase()).contains(channel)) {
          _connectedChannels.get(user.getName().toLowerCase()).add(channel);
        }
        _activeChannel.put(username.toLowerCase(), channel);
        return true;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return false;
  }

  public static boolean leaveChannel(String username, String channel) {
    try {
      User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      if (_connectedChannels.containsKey(username.toLowerCase()) && _connectedChannels.get(username.toLowerCase()).contains(channel)) {
        _connectedChannels.get(username.toLowerCase()).remove(channel);
        return true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  public static void chat(String username, String message) throws Exception {
    if (!_activeChannel.containsKey(username.toLowerCase())) {
      setActiveChannel(username, "global");
    }
    chat(_activeChannel.get(username.toLowerCase()), username, message, false);
  }

  public static void chat(String channel, String sender, String message, boolean fromIRC) throws Exception {
    User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(sender);
    channel = channel.replaceAll(ChatMessenger.CHANNEL_IRC_SUFFIX, "");
    if (!channel.equals("global") && !channel.equals("staff")) {
      channel = "server";
    }

    if (!fromIRC && !channel.equals("server")) {
      ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", sender + "|" + channel + "|chat=" + message);
    }

    String colour = COLOUR_CHANNEL_BASE;
    if (channel.equals("global")) {
      colour = COLOUR_CHANNEL_GLOBAL;
    } else if (channel.equals("staff")) {
      colour = COLOUR_CHANNEL_STAFF;
    } else if (channel.equals("server")) {
      colour = COLOUR_CHANNEL_BASE;
    }
    String name;
    String channelDisplay = "#" + channel;
    try {
      name = user.getFormattedName();
      channelDisplay = "@" + user.getActiveServer().replaceAll("_", " ");
    } catch (Exception e) {
      name = "&7" + sender;
    }
    String msg = MessageUtils.format((!channel.equals("server") ? "&8[&" + colour + channelDisplay + "&8] " : "") + name + "&7: " + "&" + colour, true)
        + MessageUtils.format(message, (user != null ? user.canUseColours() : false));

    for (Player p : ForsakenGlobal.getInstance().getServer().getOnlinePlayers()) {
      if (_connectedChannels.containsKey(p.getName().toLowerCase()) && _connectedChannels.get(p.getName().toLowerCase()).contains(channel)) {
        User u = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(p.getName());
        if (u.ignores(user.getId())) { return; }
        ForsakenGlobal.getInstance().getAPI().getMessageManager().sendMessage(u.getPlayer(), msg, false, false);
      }
    }
  }

  public static void sendTell(String userNameFrom, String userNameTo, String message) {
    User userFrom = null, userTo = null;
    try {
      userFrom = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(userNameFrom);
      userTo = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(userNameTo);
    } catch (Exception e) {
      e.printStackTrace();
    }
    _userReplyMap.put(userNameTo.toLowerCase(), userNameFrom);
    if ((userFrom != null) && (userTo != null)) {
      if (userTo.ignores(userFrom.getId())) { return; }
      message = MessageUtils.format(message, false);
      if (message.isEmpty() || message.equalsIgnoreCase("")) { return; }
      if ((userFrom.getPlayer() != null) && userFrom.getPlayer().isOnline()) {
        ForsakenGlobal.getInstance().getAPI().getMessageManager()
            .sendMessage(userFrom.getPlayer(), "&o&6" + "To " + userTo.getFormattedName() + "&e: " + message);
        if ((userTo.getPlayer() == null) || ((userTo.getPlayer() != null) && !userTo.getPlayer().isOnline())) {
          ForsakenGlobal.getBaseIRCBot().sendChannelMessage("global-connections", userFrom.getName() + "|" + userTo.getName() + "|tell=" + message);
        }
      }
      if ((userTo.getPlayer() != null) && userTo.getPlayer().isOnline()) {
        ForsakenGlobal.getInstance().getAPI().getMessageManager()
            .sendMessage(userTo.getPlayer(), "&o&6" + "From " + userFrom.getFormattedName() + "&e: " + message);
      }
      ForsakenGlobal
          .getInstance()
          .getAPI()
          .getMessageManager()
          .sendDebugMessage("&o&6" + userFrom.getName() + " &e=> &6" + userTo.getName() + ": &e" + message, new int[] { 5, 6 });
    }
  }

  public static String getReplyee(String username) {
    return _userReplyMap.get(username.toLowerCase());
  }
}
