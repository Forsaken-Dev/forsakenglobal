/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.chat.cmds;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.irc.IRCMessageReceiver;
import co.forsaken.api.manager.APIManager;
import co.forsaken.chat.ChatMessenger;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class ChatRegistrars {

  @Command(aliases = { "channel", "ch" }, desc = "Channel Commands", min = 0, max = -1)
  @NestedCommand(value = { ChatCommands.class })
  public static void channel(CommandContext args, User user) throws CommandException {
  }

  @Command(aliases = { "global", "g" }, desc = "Talk directly to the global server", usage = "", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("chat.channel.join")
  public static void global(CommandContext args, User user) throws CommandException {
    try {
      ChatMessenger.chat("global", user.getName(), args.getJoinedStrings(0), false);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Command(aliases = { "staff", "s" }, desc = "Talk directly to the staff server", usage = "", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("chat.channel.join")
  public static void staff(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRank().isStaff()) {
      try {
        ChatMessenger.chat("staff", user.getName(), args.getJoinedStrings(0), false);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Command(aliases = { "server", "local", "l" }, desc = "Talk directly to the server server", usage = "", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("chat.channel.join")
  public static void server(CommandContext args, User user) throws CommandException {
    try {
      ChatMessenger.chat("server", user.getName(), args.getJoinedStrings(0), false);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static class ChatCommands {
    @Command(aliases = { "join" }, desc = "Sets your active channel", usage = "[name]", anyFlags = true, min = 1, max = -1)
    @CommandPermissions("chat.channel.join")
    public static void join(CommandContext args, User user) throws CommandException {
      setActiveChannel(args.getString(0), user);
    }

    @Command(aliases = { "leave" }, desc = "Hides a channel from view", usage = "[name]", anyFlags = true, min = 1, max = -1)
    @CommandPermissions("chat.channel.leave")
    public static void leave(CommandContext args, User user) throws CommandException {
      String channel = args.getString(0);
      if (channel.startsWith("#")) {
        channel = channel.substring(1);
      }
      if (ChatMessenger.leaveChannel(user.getName(), channel)) {
        user.sendMessage("&eYou have left &6#" + channel);
      } else {
        user.sendMessage("&eSorry we could not find a channel name &6#" + channel + " &ethat you are active in");
      }
    }

    @Command(aliases = { "global", "g" }, desc = "Sets your active channel to global chat", usage = "", anyFlags = true, min = 0, max = 0)
    @CommandPermissions("chat.channel.join")
    public static void global(CommandContext args, User user) throws CommandException {
      setActiveChannel("global", user);
    }

    @Command(aliases = { "staff" }, desc = "Sets your active channel to staff chat", usage = "", anyFlags = true, min = 0, max = 0)
    @CommandPermissions("chat.channel.join")
    public static void staff(CommandContext args, User user) throws CommandException {
      setActiveChannel("staff", user);
    }

    @Command(aliases = { "server", "s" }, desc = "Sets your active channel to server chat", usage = "", anyFlags = true, min = 0, max = 0)
    @CommandPermissions("chat.channel.join")
    public static void server(CommandContext args, User user) throws CommandException {
      setActiveChannel("server", user);
    }

    private static void setActiveChannel(String channel, User user) {
      if (channel.startsWith("#")) {
        channel = channel.substring(1);
      }
      if (ChatMessenger.setActiveChannel(user.getName(), channel)) {
        user.sendMessage("&aYour active channel has been set to &6#" + channel.toLowerCase());
      } else {
        user.sendMessage("&eSorry we could not find a chat channel named &6#" + channel.toLowerCase());
        user.sendMessage("&eValid channels are: &6/ch global&e, &6/ch server" + (user.getPrimaryRank().isStaff() ? "&e, &6/ch staff" : ""));
      }
    }
  }

  @Command(aliases = { "tell", "message", "msg", "whisper", "w" }, desc = "Sends another user a message", usage = "[user] [message...]", anyFlags = true, min = 2, max = -1)
  @CommandPermissions("chat.tell")
  public static void tellUser(CommandContext args, User user) throws CommandException {
    if (args.getString(0).equalsIgnoreCase(user.getName())) {
      user.sendMessage("&eWhy would you need to send yourself a message");
      return;
    }
    if (!isUserOnline(user, args.getString(0))) {
      user.sendMessage("&6" + args.getString(0) + " &eis not currently online");
      return;
    }
    try {
      User user2 = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      ChatMessenger.sendTell(user.getName(), user2.getName(), args.getJoinedStrings(1));
    } catch (Exception e) {
      throw new CommandException(e.getMessage());
    }
  }

  public static boolean isUserOnline(User user, String userToCheck) {
    try {
      for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getOnlineAny()) {
        if (u.getName().equalsIgnoreCase(userToCheck) && u.getActiveServer() != null) { return true; }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  @Command(aliases = { "reply", "r" }, desc = "Reply to a user", usage = "[message...]", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("chat.reply")
  public static void reply(CommandContext args, User user) throws CommandException {
    if (ChatMessenger.getReplyee(user.getName()) == null) { throw new CommandException("You have no one to reply to"); }
    String username = ChatMessenger.getReplyee(user.getName());
    ForsakenGlobal.getInstance().getServer().dispatchCommand(user.getPlayer(), "tell " + username + " " + args.getJoinedStrings(0));
  }

  @IRCMessageReceiver()
  public static void ircMessage(APIManager _manager, String channel, String sender, String receiver, String message) {
    if (message.contains("|tell=")) {
      ChatMessenger.sendTell(message.substring(0, message.indexOf("|")), message.substring(message.indexOf("|") + 1, message.indexOf("|tell=")), message.substring(message.indexOf("|tell=") + 6));
    } else if (message.indexOf("|chat=") > 0) {
      try {
        User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(message.substring(0, message.indexOf("|")));
        if (user.getActiveServer() == null || !user.getActiveServer().equalsIgnoreCase(sender.replaceAll("-server", ""))) {
          user.setActiveServer(sender.replaceAll("-server", ""), false);
        }
        ChatMessenger.chat(message.substring(message.indexOf("|") + 1, message.indexOf("|chat=")), message.substring(0, message.indexOf("|")), message.substring(message.indexOf("|chat=") + 6), true);
      } catch (Exception e) {
        e.printStackTrace();
      }

    }
  }

}
