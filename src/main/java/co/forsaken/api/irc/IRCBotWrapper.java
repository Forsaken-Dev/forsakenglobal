/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.irc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pircbotx.PircBotX;
import org.pircbotx.UtilSSLSocketFactory;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.manager.MessageManager;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.chat.ChatMessenger;

@SuppressWarnings("rawtypes")
public class IRCBotWrapper {

  private ForsakenGlobal                 _plugin;
  private User                           _user;
  private final PircBotX                 _ircBot;
  private List<String>                   _connectedChannels = new ArrayList<String>();
  private Map<String, ArrayList<String>> _defaultMessages   = new HashMap<String, ArrayList<String>>();
  private IRCBotListener                 _listener;
  public boolean                         _connected         = false;

  public IRCBotWrapper(ForsakenGlobal plugin)
  {
    _plugin = plugin;
    _user = null;
    _listener = new IRCBotListener(_plugin, this);
    _ircBot = new PircBotX();
    _ircBot.setLogin(plugin.getServer().getMotd() + "-server");
    _ircBot.setName(plugin.getServer().getMotd() + "-server");
    _ircBot.setAutoNickChange(false);
    _ircBot.setFinger("What are you doing? Stop it!");
    _ircBot.setVersion("v" + _plugin.getDescription().getVersion());
    _ircBot.getListenerManager().addListener(_listener);
    asyncConnect();
  }

  public IRCBotWrapper(ForsakenGlobal plugin, co.forsaken.api.factories.obj.User user)
  {
    _plugin = plugin;
    _user = user;
    _listener = new IRCBotListener(_plugin, this);
    _ircBot = new PircBotX();
    _ircBot.setLogin(_user.getName());
    _ircBot.setName(_user.getName());
    _ircBot.setAutoNickChange(false);
    _ircBot.setFinger("What are you doing? Stop it!");
    _ircBot.setVersion("v" + _plugin.getDescription().getVersion());
    _ircBot.getListenerManager().addListener(_listener);
    asyncConnect();
  }

  public void asyncConnect()
  {
    _plugin.getServer().getScheduler().runTaskAsynchronously(_plugin, new Runnable()
    {
      public void run()
      {
        if ((_user == null) || ((_user != null) && (_user.getPlayer() != null) && (_user.getPlayer().isOnline()))) {
          connect();
        }
      }
    });
  }

  public void asyncQuit()
  {
    _plugin.getServer().getScheduler().runTaskAsynchronously(_plugin, new Runnable()
    {
      public void run()
      {
        quit();
      }
    });
  }

  public void asyncJoinChannel()
  {
    _plugin.getServer().getScheduler().runTaskLaterAsynchronously(_plugin, new Runnable()
    {
      public void run()
      {
        joinChannel("global-connections", "");
        if (_user != null)
        {
          try
          {
            ChatMessenger.initUser(_user.getName());
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
          sendChannelMessage("#global-connections", "join-" +
              _user.getActiveServer());
        }
        else
        {
          _ircBot.sendRawLine("oper " + _ircBot.getNick() + " pinkfatgoat");
          _ircBot.op(_ircBot.getChannel("#global-connections"), _ircBot.getUserBot());
          sendChannelMessage("#global-connections", "loadUsers");
        }
      }
    }, 100L);
  }

  public boolean isConnected()
  {
    return _connected;
  }

  public void connect()
  {
    connect(1, 10);
  }

  public void connect(int current, int maxTries) {

    if (current > maxTries) {
      _plugin.getAPI().getMessageManager().log(MessageManager.Level.WARNING, "Exceeded number of reconnect attempts. Failed to connect to IRC");
      if (_user != null) {
        _user.sendMessage("&eThere has been an issue with our chat system, if the problem persists please relog to correct the error");
      }
      return;
    }
    if (current > 1) {
      try {
        Thread.sleep(5000L);
      } catch (InterruptedException ex) {
      }
    }

    if (_connected) {
      _plugin.getAPI().getMessageManager().log(Level.INFO, "Attempted to connect to IRC while already connected.");
      _plugin.getAPI().getMessageManager().log(Level.INFO, "To force reconnecting, reload the plugin.");
    } else {
      try {
        _plugin
            .getAPI()
            .getMessageManager()
            .log(
                Level.INFO,
                "Connecting to \"" + _plugin.getAPI().getConfigManager().getString("irc.server.ip") + ":" + _plugin.getAPI().getConfigManager().getInt("irc.server.port") + "\" as \"" + _ircBot.getName() + "\" [SSL: "
                    + _plugin.getAPI().getConfigManager().getBoolean("irc.server.use_ssl") + "] [Attempt " + current + "/" + maxTries + "]");

        if (_plugin.getAPI().getConfigManager().getBoolean("irc.server.use_ssl")) {
          UtilSSLSocketFactory socketFactory = new UtilSSLSocketFactory();
          socketFactory.disableDiffieHellman();
          _ircBot.connect(_plugin.getAPI().getConfigManager().getString("irc.server.ip"), _plugin.getAPI().getConfigManager().getInt("irc.server.port"), _plugin.getAPI().getConfigManager().getString("irc.server.password"), socketFactory);
        } else {
          _ircBot.connect(_plugin.getAPI().getConfigManager().getString("irc.server.ip"), _plugin.getAPI().getConfigManager().getInt("irc.server.port"), _plugin.getAPI().getConfigManager().getString("irc.server.password"));
        }
        return;
      } catch (Exception ex) {
        _plugin.getAPI().getMessageManager().log(Level.WARNING, "Problem connecting to " + _plugin.getAPI().getConfigManager().getString("irc.server.ip") + " => " + " as " + _ircBot.getName() + " [Error: " + ex.getMessage() + "]");
      }
    }
    if (!_connected) {
      connect(current + 1, maxTries);
    }
  }

  public PircBotX getBot()
  {
    return _ircBot;
  }

  public String getNick()
  {
    return _ircBot.getUserBot().getNick();
  }

  public void quit()
  {
    _ircBot.quitServer("Disconnecting");
  }

  public void leaveChannel(String channel)
  {
    if (channel.charAt(0) != '#') {
      channel = "#" + channel;
    }
    _ircBot.partChannel(_ircBot.getChannel(channel));
    if (_connectedChannels.contains(channel)) {
      _connectedChannels.add(channel);
    }
  }

  public List<String> getConnectedChannels()
  {
    return _connectedChannels;
  }

  public void joinChannel(String channel, String password)
  {
    if (isConnected()) {
      if (channel.charAt(0) != '#') {
        channel = "#" + channel;
      }
      final String channelName = channel;
      if (password.isEmpty()) {
        _ircBot.joinChannel(channelName);
      } else {
        _ircBot.joinChannel(channelName, password);
      }
      if (!_connectedChannels.contains(channelName)) {
        _connectedChannels.add(channelName);
      }
      _plugin.getServer().getScheduler().runTaskLaterAsynchronously(_plugin, new Runnable()
      {
        public void run()
        {
          if (_defaultMessages.containsKey(channelName))
          {
            for (String message : _defaultMessages.get(channelName)) {
              sendRawMessage(channelName, message);
            }
            _defaultMessages.remove(channelName);
          }
        }
      }, 20L);
    }
  }

  public void sendMessage(String message)
  {
    sendRawMessage(_plugin.getAPI().getConfigManager().getString("irc.channel.name"), message);
  }

  public void sendChannelMessage(String channelName, String message)
  {
    sendRawMessage(channelName, message);
  }

  private void sendRawMessage(String channel, final String message)
  {
    if (isConnected()) {
      if (channel.charAt(0) != '#') {
        channel = "#" + channel;
      }
      final String channelName = channel;
      _plugin.getServer().getScheduler().runTaskAsynchronously(_plugin, new Runnable()
      {
        public void run()
        {
          if (!_connectedChannels.contains(channelName)) {
            joinChannel(channelName, "");
          }
          _ircBot.sendMessage(channelName, message);
        }
      });
    }
  }

  public IRCBotListener getListener()
  {
    return _listener;
  }
}