/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.irc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.DisconnectEvent;
import org.pircbotx.hooks.events.MessageEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

/**
 * IRC Listener to designate the messages out
 * 
 * @author ForsakenNetwork-Dev
 */
@SuppressWarnings("rawtypes")
public class IRCBotListener extends ListenerAdapter {

  private final ForsakenGlobal  _plugin;
  private IRCBotWrapper         _wrapper;
  protected static List<Method> _handlers = new ArrayList<Method>();

  public IRCBotListener(ForsakenGlobal plugin, IRCBotWrapper wrapper) {
    _plugin = plugin;
    _wrapper = wrapper;
  }

  private void invokeMethod(Method method, Object[] methodArgs) {
    try {
      method.invoke(null, methodArgs);
    } catch (IllegalArgumentException e) {
      _plugin.getAPI().getMessageManager().log(Level.SEVERE, "Failed to execute message handler");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      _plugin.getAPI().getMessageManager().log(Level.SEVERE, "Failed to execute message handler");
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      _plugin.getAPI().getMessageManager().log(Level.SEVERE, "Failed to execute message handler");
      e.printStackTrace();
    }
  }

  @Override
  public void onConnect(ConnectEvent event) {
//    _plugin.getAPI().getMessageManager().log(Level.INFO, "Connected.");
    _wrapper._connected = true;
    _wrapper.asyncJoinChannel();
  }

  @Override
  public void onDisconnect(DisconnectEvent e) {
//    _plugin.getAPI().getMessageManager().log(Level.INFO, "Disconnected.");
    _wrapper.asyncQuit();
  }

  @Override
  public void onMessage(MessageEvent e) {
    Object[] methodArgs = { _plugin.getAPI(), e.getChannel().getName().replaceFirst("#", ""), e.getUser().getNick(), _wrapper.getBot().getNick(), e.getMessage() };
    for (Method m : _handlers) {
      invokeMethod(m, methodArgs);
    }
  }

  public static List<IRCMessageReceiver> register(Class<?> cls) throws Exception {
    List<IRCMessageReceiver> registered = new ArrayList<IRCMessageReceiver>();
    for (Method method : cls.getMethods()) {
      if (!method.isAnnotationPresent(IRCMessageReceiver.class)) {
        continue;
      }

      Modifier.isStatic(method.getModifiers());
      IRCMessageReceiver cmd = method.getAnnotation(IRCMessageReceiver.class);
      _handlers.add(method);
      registered.add(cmd);
    }

    return registered;
  }

}
