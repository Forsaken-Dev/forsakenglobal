/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories;

import java.sql.SQLDataException;

import org.bukkit.World;
import org.bukkit.WorldCreator;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Server;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;

/**
 * Server factory to load all server into the server
 * 
 * @author ForsakenNetwork-Dev
 */
public class ServerFactory extends DatabaseFactory<Server> {

  public ServerFactory(ForsakenGlobal plugin) {
    super(plugin, "servers");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE IF NOT EXISTS `"
                + _tableName
                + "` ( `id` int(16) NOT NULL AUTO_INCREMENT, `friendly_name` varchar(128) NOT NULL, `name` varchar(64) NOT NULL, `friendly_ip` varchar(64) NOT NULL, `ip` varchar(64) NOT NULL, `port` int(5) NOT NULL, `whitelist` int(1) NOT NULL DEFAULT '0', PRIMARY KEY (`id`), UNIQUE KEY `name` (`name`) ) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
  }

  /**
   * Get a server from the name
   * 
   * @param name
   * @return
   * @throws Exception
   */
  public Server get(String name) throws Exception {
    for (Server s : getAll()) {
      if (s.getName().equalsIgnoreCase(name)) { return s; }
    }
    String sql = "SELECT * FROM " + _tableName + " WHERE `name` = ?;";
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql, name);
    if ((query != null) && query.hasRows()) {
      try {
        Server s = parseFromQuery(query, 0);
        _databaseObjects.put(s.getId() + "", s);
        return s;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    throw new Exception("The server " + name + " could not be found");
  }

  /**
   * Get the current server object
   * 
   * @return
   * @throws Exception
   */
  public Server getCurrent() throws Exception {
    return get(_plugin.getServer().getMotd().toLowerCase());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void initObject() {
    super.initObject();
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT * FROM " + _tableName);
    if ((query != null) && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          Server s = parseFromQuery(query, i);
          _databaseObjects.put(s.getName().toLowerCase(), s);
        } catch (SQLDataException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    _plugin.getAPI().getMessageManager().log(Level.INFO, "ServerFactory has loaded " + _databaseObjects.size() + " servers successfully");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Server parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("A server could not be parsed from the database correctly");
    return new Server(_plugin, query.getInteger(row, "id"), query.getString(row, "friendly_name"), query.getString(row, "name"), query.getString(row,
        "friendly_ip"), query.getString(row, "ip"), query.getInteger(row, "port"), query.getInteger(row, "whitelist") == 1, query.getString(row, "version"), query.getString(row, "text_colour"), query.getInteger(row, "block_colour"));
  }

  public World getWorld(String name) {
    if (name.equalsIgnoreCase("0") && !ForsakenGlobal.isMystcraftEnabled()) {
      name = "world";
    }
    if (_plugin.getServer().getWorld(name) == null) {
      return _plugin.getServer().createWorld(new WorldCreator(name));
    } else {
      return _plugin.getServer().getWorld(name);
    }
  }
}
