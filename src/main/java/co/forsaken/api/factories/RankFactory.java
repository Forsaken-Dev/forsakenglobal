/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories;

import java.sql.SQLDataException;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Rank;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;

/**
 * Rank Factory to keep track of all server ranks
 * 
 * @author ForsakenNetwork-Dev
 */
public class RankFactory extends DatabaseFactory<Rank> {

  public RankFactory(ForsakenGlobal plugin) {
    super(plugin, "ranks");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE IF NOT EXISTS `"
                + _tableName
                + "` ( `id` int(16) NOT NULL AUTO_INCREMENT, `name` varchar(64) NOT NULL, `prefix` varchar(32) NOT NULL, `suffix` varchar(32) NOT NULL, `hex_colour` varchar(6) NOT NULL, `staff` int(1) NOT NULL DEFAULT '0', `order` int(1) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `name` (`name`) ) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "INSERT IGNORE INTO `"
                + _tableName
                + "` (`id`, `name`, `prefix`, `suffix`, `hex_colour`, `staff`, `order`) VALUES (1, 'None', '', '', '000000', 0, 0), (2, 'Member', '', '', '000000', 0, 0), (3, 'Trainee', '&9Helper', '', '5555ff', 1, 1), (4, 'Moderator', '&bMod', '', '55ffff', 1, 2), (5, 'Super Mod', '&3Super Mod', '', '00aaaa', 1, 3), (6, 'Administrator', '&4Admin', '', 'aa0000', 1, 4), (7, 'Developer', '&5Dev', '', 'aa00aa', 1, 5), (8, 'Owner', '&6Owner', '', 'ff5555', 1, 6), (9, 'VIP', '', '&2VIP', '55ff55', 0, 0), (10, 'Donor', '', '&2Donor', '55ff55', 0, 0), (11, 'Sponsor', '', '&aSponsor', '00aaaa', 0, 0),  (11, 'Legendary', '', '&dLegendary', 'ffff00', 0, 0);");
  }

  /**
   * Get the default rank
   * 
   * @return
   */
  public Rank getDefault() {
    try {
      return get(1);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new Rank(_plugin, 1, "Guest", "7", "000000", false, 1, 0, -1D);
  }

  public Rank get(String name) {
    for (Rank r : getAll()) {
      if (r.getName().equalsIgnoreCase(name)) { return r; }
    }
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT * FROM " + _tableName + " WHERE `name` = ?", name);
    if ((query != null) && query.hasRows()) {
      try {
        Rank s = parseFromQuery(query, 0);
        _databaseObjects.put(s.getId() + "", s);
        return s;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return new Rank(_plugin, 1, "Guest", "7", "000000", false, 1, 0, -1D);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void initObject() {
    super.initObject();
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT * FROM " + _tableName + " ORDER BY id DESC;");
    if ((query != null) && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          Rank s = parseFromQuery(query, i);
          _databaseObjects.put(s.getId() + "", s);
        } catch (SQLDataException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    _plugin.getAPI().getMessageManager().log(Level.INFO, "RankFactory has loaded " + _databaseObjects.size() + " ranks successfully");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Rank parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("A rank could not be parsed from the database correctly");
    return new Rank(_plugin, query.getInteger(row, "id"), query.getString(row, "name"), query.getString(row, "mc_colour"), query.getString(row,
        "hex_colour"), query.getInteger(row, "staff") == 1, query.getInteger(row, "xenforo_group_id"), query.getInteger(row, "order"),
        query.getDouble(row, "donation_required"));
  }

}
