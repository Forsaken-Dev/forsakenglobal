/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright Â© 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.XenforoUtils;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;

public class UserFactory extends DatabaseFactory<User> {

  public UserFactory(ForsakenGlobal plugin) {
    super(plugin, "users");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE IF NOT EXISTS `"
                + _tableName
                + "` ( `id` int(16) NOT NULL AUTO_INCREMENT, `name` varchar(16) NOT NULL, `prefix` varchar(32) NOT NULL DEFAULT '', `suffix` varchar(32) NOT NULL DEFAULT '', `primary_rank_id` int(16) NOT NULL DEFAULT '2', `secondary_rank_id` int(16) NOT NULL DEFAULT '1', `receive_log_messages` int(1) NOT NULL DEFAULT '1', `skin_url` varchar(128) NOT NULL DEFAULT '', `cape_url` varchar(128) NOT NULL DEFAULT '', `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',  `credit_amount` int(16) NOT NULL DEFAULT '150', PRIMARY KEY (`id`), UNIQUE KEY `name` (`name`), KEY `primary_rank_id` (`primary_rank_id`,`secondary_rank_id`) ) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "INSERT IGNORE INTO `"
                + _tableName
                + "` (`id`, `name`, `prefix`, `suffix`, `primary_rank_id`, `secondary_rank_id`, `receive_log_messages`, `skin_url`, `cape_url`, `join_date`, `last_login`) VALUES (1, 'Zephyr-Bot', '&6AutoBot', '', 1, 1, 1, 1, '', '', NOW(), NOW()), (2, 'ZephyrusMC', '', '', 8, 1, 2, 1, '', '', NOW(), NOW()), (3, 'WinMac32', '', '', 7, 11, 3, 1, '', '', NOW(), NOW());");
  }

  /**
   * Create and return a user from only the name
   * 
   * @param name
   * @return
   */
  public User createUser(String name) {
    for (int i = 0; i < 5; i++) {
      try {
        return get(name);
      } catch (Exception e) {
      }
      new User(_plugin, name);
    }
    return null;
  }

  /**
   * Gets a user from their name
   */
  public User get(String name) throws Exception {
    name = name.trim();
    if (name.equalsIgnoreCase("console") || name.equalsIgnoreCase("rcon") || name.equalsIgnoreCase("server") || name.equalsIgnoreCase("@")) return get(1);
    if (name.isEmpty() || name.equals("")) throw new Exception("Could not find the user named " + name);
    for (User u : getAll()) {
      if (u.getName().equalsIgnoreCase(name)) { return get(u.getId()); }
    }
    String sql = "SELECT * FROM " + _tableName + " WHERE `name` = ?;";
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql, name);
    if ((query != null) && query.hasRows()) {
      try {
        User s = parseFromQuery(query, 0);
        _databaseObjects.put(s.getId() + "", s);
        return s;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    throw new Exception("Could not find the user named " + name);
  }

  /**
   * Gives a list of all the online users
   * 
   * @return
   * @throws Exception
   */
  public List<User> getAllOnline() throws Exception {
    List<User> users = new ArrayList<User>();
    String current = _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName();
    for (Player p : _plugin.getServer().getOnlinePlayers()) {
      User user = get(p.getName());
      if (user.getActiveServer() == null || (user.getActiveServer() != null && !user.getActiveServer().equals(current))) {
        get(user.getId()).setActiveServer(current, false);
        user.setActiveServer(current, false);
      }
      users.add(user);
    }
    return users;
  }

  public List<User> getOnlineOn(String name) throws Exception {
    List<User> users = new ArrayList<User>();
    for (User user : getAll()) {
      if ((user.getActiveServer() != null && !user.getActiveServer().isEmpty() && user.getActiveServer().equalsIgnoreCase(name))) {
        users.add(user);
      }
    }
    return users;
  }

  public List<User> getOnlineAny() throws Exception {
    List<User> users = new ArrayList<User>();
    for (User user : getAll()) {
      if ((user.getPlayer() != null && user.getPlayer().isOnline()) || (user.getActiveServer() != null && !user.getActiveServer().isEmpty())) {
        users.add(user);
      }
    }
    return users;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void initObject() {
    super.initObject();
    if (_plugin.getServer().getOnlinePlayers().length > 0) {
      String sql = "SELECT * FROM " + _tableName + " WHERE ";
      for (int i = 0; i < _plugin.getServer().getOnlinePlayers().length; i++) {
        if (i > 0) {
          sql += " OR ";
        }
        sql += "`name` = '" + _plugin.getServer().getOnlinePlayers()[i].getName() + "'";
      }
      DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql);
      if ((query != null) && query.hasRows()) {
        for (int i = 0; i < query.rowCount(); i++) {
          try {
            User s = parseFromQuery(query, i);
            _databaseObjects.put(s.getId() + "", s);
          } catch (SQLDataException e) {
            e.printStackTrace();
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
    _plugin.getAPI().getMessageManager().log(Level.INFO, "UserFactory has loaded " + _databaseObjects.size() + " users successfully");
  }

  @Override
  public User reload(int userId) throws Exception {
    String activeServer = get(userId).getActiveServer();
    super.reload(userId);
    User user = get(userId);
    user.setActiveServer(activeServer, false);
    return user;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("A user could not be parsed from the database correctly");
    User u = new User(_plugin, query.getInteger(row, "id"), query.getString(row, "name"),
        query.getInteger(row, "receive_log_messages") == 1, query.getString(row, "skin_url"), query.getString(row, "cape_url"), query.getDate(row,
            "join_date"), query.getDate(row, "last_login"), query.getInteger(row, "credit_amount"), query.getInteger(row, "vote_tokens"));
    u.initIgnores();
    u.initPunishments();
    return u;
  }
}
