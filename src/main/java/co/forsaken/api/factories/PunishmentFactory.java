/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Punishment;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;

public class PunishmentFactory extends DatabaseFactory<Punishment> {

  public static enum PunishmentTypes {
    MUTE, KICK, TEMP_BAN, BAN;

    public String getFriendlyName() {
      String name = name().replaceAll("_", " ");
      if (name().contains("BAN")) {
        name += "NED";
      } else {
        name += "ED";
      }
      return name;
    }
  }

  public PunishmentFactory(ForsakenGlobal plugin) {
    super(plugin, "user_punishments");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE IF NOT EXISTS `"
                + _tableName
                + "` ( `id` int(16) NOT NULL AUTO_INCREMENT, `user_id` int(16) NOT NULL, `staff_id` int(16) NOT NULL, `action_type` varchar(64) NOT NULL, `reason` varchar(512) NOT NULL, `expiry` timestamp NULL DEFAULT NULL, `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `active` int(1) NOT NULL DEFAULT '1', PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
  }

  /**
   * Get all punishments from a list of ids
   * 
   * @param punishmentIds
   * @return
   */
  public List<Punishment> getAll(List<Integer> punishmentIds) {
    List<Punishment> punishments = new ArrayList<Punishment>();
    for (int id : punishmentIds) {
      try {
        punishments.add(get(id));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return punishments;
  }

  public List<Integer> getForUser(int userId) {
    List<Integer> punishments = new ArrayList<Integer>();
    String sql = "SELECT * FROM " + _tableName + " WHERE `user_id` = ?;";
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql, userId);
    if ((query != null) && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          Punishment s = parseFromQuery(query, i);
          _databaseObjects.put(s.getId() + "", s);
          punishments.add(s.getId());
        } catch (SQLDataException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return punishments;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Punishment parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("A punishment could not be parsed from the database correctly");
    return new Punishment(_plugin, query.getInteger(row, "id"), query.getInteger(row, "user_id"), query.getInteger(row, "staff_id"),
        PunishmentTypes.valueOf(query.getString(row, "action_type").toUpperCase()), query.getString(row, "reason"), query.getDate(row, "expiry"),
        query.getDate(row, "date"), query.getInteger(row, "active") == 1);
  }
}
