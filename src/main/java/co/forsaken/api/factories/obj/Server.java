/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories.obj;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.SQLDataException;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.ServerUpdateTask;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.server.lobby.ServerMenu;

/**
 * Server reference for each server we have
 * 
 * @author ForsakenNetwork-Dev
 */
public class Server extends DatabaseObject<Server> {

  private String  _friendlyName;
  private String  _name;
  private String  _friendlyIp;
  private String  _ip;
  private int     _port;
  private boolean _whitelist;
  private String  _welcomeMessage;
  private String  _version;
  private String  _textColour;
  private int     _blockColour;

  /**
   * @param plugin
   * @param id
   * @param friendlyName
   * @param name
   * @param friendlyIp
   * @param ip
   * @param port
   * @param whitelist
   */
  public Server(ForsakenGlobal plugin, int id, String friendlyName, String name, String friendlyIp, String ip, int port, boolean whitelist, String version, String textColour, int blockColour) {
    super(plugin, id);
    _friendlyName = friendlyName;
    _name = name;
    _friendlyIp = friendlyIp;
    _ip = ip;
    _port = port;
    _whitelist = whitelist;
    _version = version;
    _textColour = textColour;
    _blockColour = blockColour;
    loadWelcomeMessage();
    if (ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("lobby") && !_name.equalsIgnoreCase("lobby")) {
      loadServerUpdateTask();
    }
  }

  private ServerUpdateTask _task;

  private void loadServerUpdateTask() {
    if (_task != null) {
      _task.cancel();
    }
    _task = new ServerUpdateTask(_plugin, _id);
  }

  private boolean _online = false;

  public boolean isOnline() {
    return _online;
  }

  public String getVersion() {
    return _version;
  }

  public void setActiveInfo(boolean online, float tps, int playerCount, int maxPlayers) {
    _online = online;
    _tps = tps;
    _playerCount = playerCount;
    _maxPlayers = maxPlayers;
    _plugin.getAPI().getItemMenuManager().updateMenu(ServerMenu.class);
  }

  private float _tps = 20;

  public float getTPS() {
    return _tps;
  }

  private int _playerCount = 0;

  public int getPlayerCount() {
    return _playerCount;
  }

  private int _maxPlayers = 128;

  public int getMaxPlayers() {
    return _maxPlayers;
  }

  public boolean pingServer() {
    try {
      SocketAddress addr = new InetSocketAddress(_ip, _port);
      Socket s = new Socket();
      s.connect(addr);
      s.close();
      return true;
    } catch (IOException e) {
      return false;
    }
  }

  public String getTextColour() {
    return _textColour;
  }

  public int getBlockColour() {
    return _blockColour;
  }

  private void loadWelcomeMessage() {
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT * FROM forsaken_server_welcome_messages WHERE server_id = ?", this.getId());
    if (query != null && query.hasRows()) {
      try {
        _welcomeMessage = query.getString(0, "message");
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    } else {
      _welcomeMessage = "";
    }
  }

  public String getWelcomeMessage() {
    return _welcomeMessage;
  }

  @Override
  public void delete() {
  }

  /**
   * Get the user friendly ip for the server
   * 
   * @return
   */
  public String getFriendlyIp() {
    return _friendlyIp;
  }

  /**
   * get the user friendly name for the server
   * 
   * @return
   */
  public String getFriendlyName() {
    return _friendlyName;
  }

  /**
   * Get the actual IP of the server
   * 
   * @return
   */
  public String getIp() {
    return _ip;
  }

  /**
   * Get a backend name for the server
   * 
   * @return
   */
  public String getName() {
    return _name;
  }

  /**
   * Get the actual server port
   * 
   * @return
   */
  public int getPort() {
    return _port;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
  }

  /**
   * Check if the server is open to the public
   * 
   * @return
   */
  public boolean isWhitelisted() {
    return _whitelist;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
  }
}
