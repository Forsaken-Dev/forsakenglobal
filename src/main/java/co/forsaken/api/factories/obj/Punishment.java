/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories.obj;

import java.sql.SQLDataException;
import java.util.Date;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.PunishmentFactory.PunishmentTypes;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.XenforoUtils;
import co.forsaken.api.utils.XenforoUtils.GroupType;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.api.utils.db.DatabaseResults;

/**
 * User Punishments
 * 
 * @author ForsakenNetwork-Dev
 */
public class Punishment extends DatabaseObject<Punishment> {

  private int             _userId;
  private int             _staffId;
  private PunishmentTypes _type;
  private String          _reason;
  private Date            _expiry;
  private Date            _date;
  private boolean         _active;

  /**
   * @param plugin
   * @param id
   * @param userId
   * @param staffId
   * @param type
   * @param reason
   * @param expiry
   * @param date
   * @param active
   */
  public Punishment(ForsakenGlobal plugin, int id, int userId, int staffId, PunishmentTypes type, String reason, Date expiry, Date date, boolean active) {
    super(plugin, id);
    _userId = userId;
    _staffId = staffId;
    _type = type;
    _reason = reason;
    _expiry = expiry;
    _date = date;
    _active = active;
  }

  /**
   * Used for creating punishments
   * 
   * @param plugin
   * @param userId
   * @param staffId
   * @param type
   * @param reason
   * @param expiry
   */
  public Punishment(ForsakenGlobal plugin, int userId, int staffId, PunishmentTypes type, String reason, Date expiry) {
    super(plugin);
    _userId = userId;
    _staffId = staffId;
    _type = type;
    _reason = reason;
    _expiry = expiry;
    _date = new Date();
    insert();
  }

  @Override
  public void delete() {
  }

  /**
   * Get the time the punishment happened
   * 
   * @return
   */
  public Date getDate() {
    return _date;
  }

  /**
   * Get the expiry date
   * 
   * @return
   */
  public Date getExpiry() {
    return _expiry;
  }

  /**
   * Get the reason for the punishment
   * 
   * @return
   */
  public String getReason() {
    return _reason;
  }

  /**
   * Get the user who applied the punishment
   * 
   * @return
   */
  public User getStaff() throws Exception {
    return _plugin.getAPI().getFactoryManager().getUserFactory().get(getStaffId());
  }

  /**
   * Get the id of the staff member
   * 
   * @return
   */
  public int getStaffId() {
    return _staffId;
  }

  /**
   * Get the type of the punishment
   * 
   * @return
   */
  public PunishmentTypes getType() {
    return _type;
  }

  /**
   * Get the user who committed the "felony"
   * 
   * @return
   */
  public User getUser() throws Exception {
    return _plugin.getAPI().getFactoryManager().getUserFactory().get(getUserId());
  }

  /**
   * Get the id of the user who matches the session
   * 
   * @return
   */
  public int getUserId() {
    return _userId;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "INSERT INTO `" + _plugin.getAPI().getFactoryManager().getPunishmentFactory().getTableName()
                + "` (`user_id`, `staff_id`, `action_type`, `reason`, `expiry`, `date`, `active`) VALUES (?, ?, ?, ?, ?, ?, '1');", getUserId(),
            getStaffId(), getType().name(), getReason(), getExpiry(), getDate());
    refresh();
  }

  /**
   * Get if the punishment is still active
   * 
   * @return
   */
  public boolean isActive() {
    return _active;
  }

  public void setActive(boolean state) {
    _active = state;
    update();
    if (getType() == PunishmentTypes.BAN && !state) {
      try {
        XenforoUtils.changeUserGroup(getUser().getName(), 2, GroupType.PRIMARY);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {
    DatabaseResults query = _plugin
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT * FROM `" + _plugin.getAPI().getFactoryManager().getPunishmentFactory().getTableName()
                + "` WHERE `user_id` = ? AND `staff_id` = ? AND `action_type` = ? AND `date` = ?;", getUserId(), getStaffId(), getType().name(),
            getDate());
    try {
      Punishment user = _plugin.getAPI().getFactoryManager().getPunishmentFactory().parseFromQuery(query, 0);
      _id = user.getId();
      _userId = user.getUserId();
      _staffId = user.getStaffId();
      _type = user.getType();
      _reason = user.getReason();
      _expiry = user.getExpiry();
      _date = user.getDate();
      _active = user.isActive();
    } catch (SQLDataException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    String msg = "";
    if ((getType() == PunishmentTypes.BAN) || (getType() == PunishmentTypes.TEMP_BAN)) {
      msg = "&cYou have been &4"
          + getType().getFriendlyName()
          + (getExpiry() != null ? " &cuntil &4"
              + MessageUtils.getTimeFormater().format(getExpiry()) : "") + "\n\n";
      try {
        msg += "\n&6" + getStaff().getFormattedName() + ": &e" + getReason() + "\n";
      } catch (Exception e) {
        msg += "\n&6Reason: &e" + getReason() + "\n";
      }
      if (getType() == PunishmentTypes.BAN) {
        msg += "\n&cAppeal at: &4http://forum.forsaken.co"
            + "/appeal";
      }
    } else {
      msg = "&cYou have been &4"
          + getType().getFriendlyName()
          + (getExpiry() != null ? " &cuntil &4"
              + MessageUtils.getTimeFormater().format(getExpiry()) : "");
      try {
        msg += " &eby &6" + getStaff().getFormattedName();
      } catch (Exception e) {
        e.printStackTrace();
      }
      msg += " &efor &6" + getReason();
    }
    return msg;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "UPDATE `"
                + _plugin.getAPI().getFactoryManager().getPunishmentFactory().getTableName()
                + "` SET `id` = ?, `user_id` = ?, `staff_id` = ?, `action_type` = ?, `reason` = ?, `expiry` = ?, `date` = ?, `active` = ? WHERE `id` = ?;",
            getId(), getUserId(), getStaffId(), getType().name(), getReason(), getExpiry(), getDate(), (isActive() ? 1 : 0), getId());
  }
}
