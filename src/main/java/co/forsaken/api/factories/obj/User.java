/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories.obj;

import java.sql.SQLDataException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import co.forsaken.ForsakenGlobal;
import co.forsaken.api.ReloadUserTask;
import co.forsaken.api.factories.PunishmentFactory.PunishmentTypes;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.WorldUtils;
import co.forsaken.api.utils.XenforoUtils;
import co.forsaken.api.utils.XenforoUtils.GroupType;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.chat.ChatMessenger;

/**
 * User
 * 
 * @author ForsakenNetwork-Dev
 */
public class User extends DatabaseObject<User> {

  public static enum SessionTypes {
    LOGIN, LOGOUT;
  }

  private String        _name;
  private boolean       _receiveLogMessages = true;
  private String        _skinURL            = "";
  private String        _capeURL            = "";
  private Date          _joinDate           = new Date();
  private Date          _lastLogin          = new Date();
  private int           _creditAmount       = 0;

  private String        _serverLocation     = null;
  private List<Integer> _punishments        = null;
  private long          _lastDamageTime     = 0;
  private List<Integer> _ignoreList         = null;
  private int           _voteTokens         = 0;

  /**
   * @param _plugin
   * @param id
   * @param name
   * @param prefix
   * @param suffix
   * @param primaryRankId
   * @param secondaryRankId
   * @param receiveLogMessages
   * @param skinURL
   * @param capeURL
   * @param joinDate
   * @param lastLogin
   */
  public User(ForsakenGlobal _plugin, int id, String name, boolean receiveLogMessages,
      String skinURL, String capeURL, Date joinDate, Date lastLogin, int creditAmount, int voteTokens) {
    super(_plugin, id);
    _name = name;
    _receiveLogMessages = receiveLogMessages;
    _skinURL = skinURL;
    _capeURL = capeURL;
    _joinDate = joinDate;
    _lastLogin = lastLogin;
    _creditAmount = creditAmount;
    _voteTokens = voteTokens;

    _ignoreList = new ArrayList<Integer>();
    _punishments = new ArrayList<Integer>();
  }

  public int getVoteTokens() {
    return _voteTokens;
  }

  public void addVoteTokens(int amount) {
    _voteTokens += amount;
    update();
  }

  /**
   * Used to create a new user, refreshes all data after the user is created
   * 
   * @param plugin
   * @param name
   */
  public User(ForsakenGlobal plugin, String name) {
    super(plugin);
    _name = name;
    _creditAmount = 100;
    insert();
  }

  /**
   * Add a session from this user and update last login if its a login session
   * 
   * @param ip
   * @param type
   * @throws Exception
   */
  public void addSession(String ip, SessionTypes type) throws Exception {
    if (type == SessionTypes.LOGIN) {
      _plugin
          .getAPI()
          .getDatabase()
          .write("INSERT INTO forsaken_user_sessions (user_id, ip, server_id, login_time, logout_time) VALUES(?, ?, ?, NOW(), NOW());", getId(), ip,
              _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
      _lastLogin = new Date();
      update();
    } else {
      _plugin.getAPI().getDatabase()
          .write("UPDATE forsaken_user_sessions SET logout_time = NOW() WHERE user_id = ? ORDER BY id DESC LIMIT 1;", getId());
    }
  }

  public boolean canUseColours() {
    if (getPlayer() != null) { return getPlayer().hasPermission("forsaken.chat.colours"); }
    return false;
  }

  @Override
  public void delete() {
    _plugin.getAPI().getDatabase()
        .write("DELETE FROM `" + _plugin.getAPI().getFactoryManager().getUserFactory().getTableName() + "` WHERE `name` = ?;", getName());
  }

  /**
   * Get the server the user is currently on
   * 
   * @return
   */
  public String getActiveServer() {
    return _serverLocation;
  }

  /**
   * Get the custom cape url
   * 
   * @return
   */
  public String getCapeURL() {
    return _capeURL;
  }

  public void setCape() {
    _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "cofh cape clear " + getName() + " " + getName());
    if (getCapeURL() != null && !getCapeURL().isEmpty()) {
      _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "cofh cape set " + getName() + " " + getCapeURL());
    }
  }

  public void setCapeURL(String url) {
    _capeURL = url;
    setCape();
    update();
  }

  public void setSkin() {
    _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "cofh skin clear " + getName() + " " + getName());
    if (getSkinURL() != null && !getSkinURL().isEmpty()) {
      _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "cofh skin set " + getName() + " " + getSkinURL());
    }
  }

  public void setSkinURL(String url) {
    _skinURL = url;
    setSkin();
    update();
  }

  /**
   * Checks if the user is currently banned
   * 
   * @return
   */
  public Punishment getCurrentPunishment() {
    Punishment current = null;
    for (Punishment p : getPunishments()) {
      if (!p.isActive()) {
        continue;
      }
      if (current == null) {
        current = p;
      }
      if (current.getId() < p.getId()) {
        if ((p.getType() == PunishmentTypes.BAN)
            || ((p.getExpiry() != null) && ((current.getExpiry() == null) || (current.getExpiry().before(p.getExpiry()))))) {
          current = p;
          if (p.getType() == PunishmentTypes.BAN) {
            break;
          }
        }
      }
    }
    if ((current != null) && ((current.getExpiry() == null) || current.getExpiry().after(new Date()))) return current;
    else return null;
  }

  /**
   * Get a user friendly name for this user with prefixes and suffixes
   * 
   * @return
   */
  public String getFormattedName() {
    String name = "";
    String tagBase = "&%s[%s&r&%s]&r";
    if (!getPrefix().isEmpty()) {
      name += String.format(tagBase, getPrimaryRank().getMCColour(), getPrefix(), getPrimaryRank().getMCColour()) + " ";
    } else if (getPrimaryRankId() > 2) {
      name += String.format(tagBase, getPrimaryRank().getMCColour(), getPrimaryRank().getName(), getPrimaryRank().getMCColour()) + " ";
    }
    name += "&" + (getPrimaryRankId() > 1 ? "F" : "7") + getName();
    if (!getSuffix().isEmpty()) {
      name += " " + String.format(tagBase, getSecondaryRank().getMCColour(), getSuffix(), getSecondaryRank().getMCColour());
    } else if (getSecondaryRankId() > 8) {
      name += " " + String.format(tagBase, getSecondaryRank().getMCColour(), getSecondaryRank().getName(), getSecondaryRank().getMCColour());
    }
    return MessageUtils.format(name, true);
  }

  public Rank getPrimaryRank() {
    String name = "Guest";
    for (PermissionGroup g : PermissionsEx.getUser(getName()).getGroups()) {
      if (g.getRankLadder().equalsIgnoreCase("primary")) {
        name = g.getName();
      }
    }
    return ForsakenGlobal.getInstance().getAPI().getFactoryManager().getRankFactory().get(name);
  }

  public int getPrimaryRankId() {
    return getPrimaryRank().getId();
  }

  public Rank getSecondaryRank() {
    String name = "Guest";
    for (PermissionGroup g : PermissionsEx.getUser(getName()).getGroups()) {
      if (g.getRankLadder().equalsIgnoreCase("secondary")) {
        name = g.getName();
      }
    }
    return ForsakenGlobal.getInstance().getAPI().getFactoryManager().getRankFactory().get(name);
  }

  public int getSecondaryRankId() {
    if (getSecondaryRank() == null) { return 1; }
    return getSecondaryRank().getId();
  }

  public String getPrefix() {
    String prefix = "";
    for (PermissionGroup pg : ((PermissionUser) PermissionsEx.getUser(getName())).getGroups()) {
      if (!pg.getPrefix().isEmpty()) {
        prefix += pg.getPrefix();
      }
    }
    if (PermissionsEx.getUser(getName()).getPrefix().isEmpty()) {
      return MessageUtils.stripFormatting(prefix);
    } else {
      return MessageUtils.stripFormatting(PermissionsEx.getUser(getName()).getPrefix());
    }
  }

  public String getSuffix() {
    String suffix = "";
    for (PermissionGroup pg : ((PermissionUser) PermissionsEx.getUser(getName())).getGroups()) {
      if (!pg.getPrefix().isEmpty()) {
        suffix += pg.getSuffix();
      }
    }
    if (PermissionsEx.getUser(getName()).getSuffix().isEmpty()) {
      return MessageUtils.stripFormatting(suffix);
    } else {
      return MessageUtils.stripFormatting(PermissionsEx.getUser(getName()).getSuffix());
    }
  }

  /**
   * Get the date the user first joined
   * 
   * @return
   */
  public Date getJoinDate() {
    return _joinDate;
  }

  /**
   * Get the date the user last logged in
   * 
   * @return
   */
  public Date getLastLogin() {
    return _lastLogin;
  }

  /**
   * Get the actual name
   * 
   * @return
   */
  public String getName() {
    return _name;
  }

  /**
   * Get the Bukkit Player
   * 
   * @return
   */
  public Player getPlayer() {
    return _plugin.getServer().getPlayer(getName());
  }

  /**
   * Gets all user punishments
   * 
   * @return
   */
  public List<Punishment> getPunishments() {
    return _plugin.getAPI().getFactoryManager().getPunishmentFactory().getAll(_punishments);
  }

  /**
   * Gets the amount of credits
   * 
   * @return
   */
  public int getCreditAmount() {
    return _creditAmount;
  }

  public void addTransaction(int amount, String reason, int... receiverId) {
    String sql = "INSERT INTO `forsaken_user_transactions` (sender_id, receiver_id, amount, reason) VALUES(?, ?, ?, ?);";
    _plugin.getAPI().getDatabase().write(sql, getId(), (receiverId.length >= 1 ? receiverId[0] : 0), amount, reason);
  }

  public void forceRemoveCredits(int amount, String reason) {
    removeCredits(amount);
    addTransaction(amount, reason, 1);
  }

  public void forceAddCredits(int amount, String reason) {
    addCredits(amount);
    addTransaction(amount, reason, 1);
  }

  public void sendCredits(int amount, User receiver, String reason) {
    removeCredits(amount);
    receiver.addCredits(amount);
    addTransaction(amount, reason, receiver.getId());
  }

  public int getAmountDepositedToday() {
    String sql = "SELECT SUM(amount) as amount FROM `forsaken_user_transactions` WHERE sender_id = ? AND reason = 'bank_deposit' AND YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) AND DAY(date) = DAY(NOW());";
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql, getId());
    if (query != null && query.hasRows() && query.rawResults.get(0).get(0) != null) { return Integer.parseInt(query.rawResults.get(0).get(0) + ""); }
    return 0;
  }

  protected void addCredits(int amount) {
    if (amount > 0) {
      _creditAmount += amount;
      update();
    }
  }

  protected void removeCredits(int amount) {
    if (amount > 0) {
      _creditAmount -= amount;
      update();
    }
  }

  public void ignore(int userId) {
    _plugin.getAPI().getDatabase().write("INSERT INTO forsaken_user_ignores (user_id, ignored_id) VALUES (?, ?);", getId(), userId);
    refresh();
  }

  public void unignore(int userId) {
    _plugin.getAPI().getDatabase().write("DELETE FROM forsaken_user_ignores WHERE user_id = ? AND ignored_id = ?;", getId(), userId);
    refresh();
  }

  public boolean ignores(int userId) {
    return _ignoreList.contains(userId);
  }

  public List<Integer> getIgnoreList() {
    return _ignoreList;
  }

  /**
   * Get the custom url for the users skin
   * 
   * @return
   */
  public String getSkinURL() {
    return _skinURL;
  }

  public void initIgnores() {
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT * FROM forsaken_user_ignores WHERE user_id = ?", getId());
    if (query != null && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          _ignoreList.add(query.getInteger(i, "ignored_id"));
        } catch (SQLDataException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Loads user punishments
   */
  public boolean initPunishments() {
    _punishments.addAll(_plugin.getAPI().getFactoryManager().getPunishmentFactory().getForUser(getId()));
    Punishment p = getCurrentPunishment();
    return getPlayer() != null && getPlayer().isOnline() && p != null && p.getType() != PunishmentTypes.MUTE && p.getType() != PunishmentTypes.KICK
        && !(p.getType() == PunishmentTypes.TEMP_BAN && new Date().getTime() <= p.getExpiry().getTime());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "INSERT INTO `" + _plugin.getAPI().getFactoryManager().getUserFactory().getTableName()
                + "` (`name`, `join_date`, `last_login`, `credit_amount`) VALUES (?, NOW(), NOW(), ?);", getName(), getCreditAmount());
    refresh();
  }

  /**
   * Check if the user should receive extra log messages
   * 
   * @return
   */
  public boolean shouldReceiveLogMessages() {
    return _receiveLogMessages;
  }

  public void setReceiveLogMessages() {
    _receiveLogMessages = !_receiveLogMessages;
    update();
  }

  public void punish(int staffId, PunishmentTypes type, String reason, Date expiry) {
    Punishment p = new Punishment(_plugin, getId(), staffId, type, reason, expiry);
    _punishments.add(p.getId());
    sendMessage(MessageUtils.format(p.toString(), true));
    if ((getPlayer() != null)
        && ((p.getType() == PunishmentTypes.BAN) || (p.getType() == PunishmentTypes.KICK) || (p.getType() == PunishmentTypes.TEMP_BAN))) {
      getPlayer().kickPlayer(MessageUtils.format(p.toString(), true));
    }
    if (p.getType() == PunishmentTypes.BAN) {
      XenforoUtils.changeUserGroup(getName(), 9, GroupType.PRIMARY);
    }
    refresh();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {
    new ReloadUserTask(_plugin, getId());
  }

  public void setDefaultCape() {

  }

  /**
   * Sends a message to the user
   * 
   * @param message
   */
  public void sendMessage(String message) {
    if (!message.startsWith("&")) {
      message = "&7" + message;
    }
    if ((getPlayer() != null) && !message.trim().isEmpty()) {
      _plugin.getAPI().getMessageManager().sendMessage(getPlayer(), message.trim(), false);
    }
  }

  public void setActiveServer(String server, boolean update) {
    if (getId() == 1) { return; }
    if (update) {
      new LoginCheckTask(_serverLocation, this, 30L);
    }
    _serverLocation = server;
  }

  private long           _lastConnect = -1;
  private LoginCheckTask _task        = null;

  public class LoginCheckTask implements Runnable {

    private String _oldServer = null;
    private User   _user      = null;

    public LoginCheckTask(String oldServer, User user, long delay) {
      _oldServer = oldServer;
      _user = user;
      _user._task = this;
      ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), this, delay);
    }

    public void run() {
      if (System.currentTimeMillis() - _user._lastConnect >= 1500 || _user._lastConnect == -1) {
        boolean output = _user._serverLocation == ForsakenGlobal.getInstance().getServer().getMotd();
        if (_user._serverLocation == null) {
          _user.sendDisconnectMessage(output, _oldServer);
        } else if (_user._serverLocation != null && _oldServer == null) {
          _user.sendConnectMessage(output, _user._serverLocation);
        } else {
          _user.sendReconnectMessage(output, _user._serverLocation);
        }
        _user._lastConnect = System.currentTimeMillis();
      }
    }
  }

  public void connect(String from) {
    if (getId() == 1) { return; }

    _serverLocation = from;
    ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", getName() + "|join=" + from);
    try {
      ChatMessenger.initUser(getName());
    } catch (Exception e) {
      e.printStackTrace();
    }
    new LoginCheckTask(null, this, 20L);
  }

  public void disconnect() {
    if (getId() == 1) { return; }

    final String loc = _serverLocation;
    ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), new Runnable() {
      public void run() {
        ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", getName() + "|quit=" + loc);
      }
    }, 25L);
    new LoginCheckTask(_serverLocation, this, 20L);
    _serverLocation = null;
  }

  public void sendConnectMessage(boolean output, String server) {
    if (getId() == 1) { return; }

    String msg = "";
    try {
      msg = "&8[&a&l+&r&8] &7"
          + getFormattedName() + " &7joined the " + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(server).getFriendlyName() + " Server";
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (output) {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(msg, false, true);
    } else {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendDebugMessage(msg);
    }
  }

  public void sendReconnectMessage(boolean output, String server) {
    if (getId() == 1) { return; }

    String msg = "";
    try {
      msg = "&8[&e&l<&r&8] &7"
          + getFormattedName() + " &7switched to the " + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(server).getFriendlyName() + " Server";
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (output) {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(msg, false, true);
    } else {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendDebugMessage(msg);
    }
  }

  public void sendDisconnectMessage(boolean output, String server) {
    if (getId() == 1) { return; }

    String msg = "";
    try {
      msg = "&8[&c&l-&r&8] &7"
          + getFormattedName() + " &7left the " + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(server).getFriendlyName() + " Server";
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (output) {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage(msg, false, true);
    } else {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendDebugMessage(msg);
    }
  }

  /**
   * Teleports a player to a location
   * 
   * @param location
   * @return
   */
  public boolean teleport(Location location) {
    return teleport(location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
  }

  /**
   * Teleports to a player
   * 
   * @param player
   * @return
   */
  public boolean teleport(Player player) {
    if (player.isOnline()) return teleport(player.getLocation());
    return false;
  }

  /**
   * Set the last damage time to the current time
   */
  public void updateLastDamageTime() {
    _lastDamageTime = System.currentTimeMillis();
  }

  /**
   * Reset the last damage time to zero
   */
  public void resetLastDamageTime() {
    _lastDamageTime = 0;
  }

  public long getLastDamageTime() {
    return _lastDamageTime;
  }

  /**
   * Teleports to a location
   * 
   * @param world
   * @param x
   * @param y
   * @param z
   * @return
   */
  public boolean teleport(String worldName, int x, int y, int z) {
    if (getPlayer() != null && getPlayer().isOnline()) {
      long timeSince = System.currentTimeMillis() - _lastDamageTime;
      int delay = 5;
      if (timeSince <= (delay * 1000)) {
        // double modifier = 1D - (double) ((double) (System.currentTimeMillis()
        // - _lastDamageTime) / (double)
        // (_plugin.getAPI().getConfigManager().getInt("api.user.teleport_damage_delay")
        // * 1000));
        // double damage = getPlayer().getMaxHealth() * modifier;
        // if (damage > 0) {
        // getPlayer().damage(damage);
        // sendMessage(MessageUtils.THEME_BASE + "You received " +
        // MessageUtils.THEME_VALUE + Math.round((modifier * 100)) + "%" +
        // MessageUtils.THEME_BASE +
        // " damage for teleporting away from combat");
        // }
        int timeLeft = (int) (delay - Math.floor(timeSince / 1000));
        sendMessage("&eYou must wait another " + timeLeft + " seconds before you can teleport.");
        return false;
      }
      if (ForsakenGlobal.isMystcraftEnabled()) {
        _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(),
            "tpx " + getName() + " " + WorldUtils.getWorldId(worldName) + " " + x + " " + y + " " + z);
      } else {
        getPlayer()
            .teleport(new Location(_plugin.getAPI().getFactoryManager().getServerFactory().getWorld(worldName), x, y, z), TeleportCause.PLUGIN);
      }
      return true;
    }
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "UPDATE `" + _plugin.getAPI().getFactoryManager().getUserFactory().getTableName()
                + "` SET `id` = ?, `name` = ?, "
                + " `receive_log_messages` = ?, `skin_url` = ?, `cape_url` = ?, `join_date` = ?, "
                + "`last_login` = ?, `credit_amount` = ?, `vote_tokens` = ? WHERE `id` = ?;", getId(),
            getName(), shouldReceiveLogMessages(), getSkinURL(), getCapeURL(),
            getJoinDate(), getLastLogin(), getCreditAmount(), getVoteTokens(), getId());
    refresh();
  }

  public void addUpgrade(int modifierId, int rankId) {
    _plugin.getAPI().getDatabase()
        .write("INSERT INTO `forsaken_user_upgrades` (`upgradee_id`, `modifier_id`, `rank_id`) VALUES (?, ?, ?)", getId(), modifierId, rankId);
  }

  public double getTotalDonationAmount() {
    double donations = 0;
    DatabaseResults query = _plugin.getAPI().getDatabase()
        .readEnhanced("SELECT SUM(amount) as amount FROM `forsaken_user_donations` WHERE `user_id` = ? GROUP BY `user_id`;", getId());
    if (query != null && query.hasRows()) {
      try {
        donations += query.getDouble(0, "amount");
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    }
    return donations;
  }

  public int getTotalVotesThisMonth() {
    int votes = 0;
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced("SELECT COUNT(*) as count FROM forsaken_user_votes WHERE user_id = ? AND (YEAR(`date`) = YEAR(NOW( )) AND MONTH(`date`) = MONTH(NOW( )))", getId());
    if (query != null && query.hasRows()) {
      votes += Integer.parseInt(query.rawResults.get(0).get(0) + "");
    }
    return votes;
  }

  public int getOnlineSeconds() {
    DatabaseResults query = _plugin
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT SUM(UNIX_TIMESTAMP(zus.logout_time)) - SUM(UNIX_TIMESTAMP(zus.login_time)) as onlineTime, zu.name as username FROM forsaken_user_sessions zus, forsaken_users zu WHERE zu.id = zus.user_id AND zu.id = ? GROUP BY zus.user_id",
            getId());
    if (query != null && query.hasRows()) { return Integer.parseInt(query.rawResults.get(0).get(0) + ""); }
    return 0;
  }

  public String getFormattedOnlineTime() throws Exception {
    double hours = Double.parseDouble("" + getOnlineSeconds()) / Math.pow(60D, 2D);
    return (new DecimalFormat("#.#")).format(hours) + " hour" + (hours == 1 ? "" : "s");
  }

  public boolean isBanned() {
    return getCurrentPunishment() != null && (getCurrentPunishment().getType() == PunishmentTypes.BAN || getCurrentPunishment().getType() == PunishmentTypes.TEMP_BAN);
  }

  public ArrayList<String> getStatus() {
    ArrayList<String> out = new ArrayList<String>();

    out.add("&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-[" + (isBanned() ? "&m" : "") + "&" + getPrimaryRank().getMCColour() + getName() + "&r&7]-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-");

    out.add("&7Primary Rank: &" + getPrimaryRank().getMCColour()
        + (getPrefix() != null && !getPrefix().isEmpty() ? getPrefix() : getPrimaryRank().getName()).replace("[", "").replace("]", ""));
    if (getSecondaryRankId() != 1) {
      out.add("&7Donor Rank: &" + getSecondaryRank().getMCColour() + getSecondaryRank().getName());
    }
    double donationAmount = getTotalDonationAmount();
    if (donationAmount > 0) {
      out.add("&7Total Donated: &f$" + getTotalDonationAmount());
    }
    out.add("&7First Online: &f" + (this.getJoinDate() == null ? "Right Now" : MessageUtils.getTimeFormater().format(this.getJoinDate())));
    if ((this.getPlayer() != null && this.getPlayer().isOnline()) || this.getActiveServer() != null) {
      try {
        out.add("&7Online: &f" + _plugin.getAPI().getFactoryManager().getServerFactory().get(this.getActiveServer()).getFriendlyName() + " Server");
      } catch (Exception e) {
        out.add("&7Last Online: &fOnline Now!");
      }
    } else {
      out.add("&7Last Online: &f" + MessageUtils.getTimeFormater().format(this.getLastLogin()));
    }
    try {
      out.add("&7Total Online: &f" + getFormattedOnlineTime());
    } catch (Exception e) {
      out.add("&7Total Online: &f0 hours");

    }
    out.add("&7Balance: &f" + this.getCreditAmount() + " cR");
    out.add("&7Vote Tokens: &f" + this.getVoteTokens());
    out.add("&7Votes This Month: &f" + getTotalVotesThisMonth());
    return out;
  }
}