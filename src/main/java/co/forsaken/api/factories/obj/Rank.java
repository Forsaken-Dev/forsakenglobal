/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.factories.obj;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseObject;

/**
 * The main server permission ranks
 * 
 * @author ForsakenNetwork-Dev
 */
public class Rank extends DatabaseObject<Rank> {

  private String  _name;
  private String  _mcColour;
  private String  _hexColour;
  private boolean _staff;
  private int     _xenforoGroupId;
  private int     _order;
  private double  _donationRequired;

  /**
   * @param plugin
   * @param id
   * @param name
   * @param prefix
   * @param suffix
   * @param hexColour
   * @param staff
   * @param order
   */
  public Rank(ForsakenGlobal plugin, int id, String name, String mcColour, String hexColour, boolean staff, int xenforoGroupId, int order,
      double donationRequired) {
    super(plugin, id);
    _name = name;
    _mcColour = mcColour;
    _hexColour = hexColour;
    _staff = staff;
    _xenforoGroupId = xenforoGroupId;
    _order = order;
    _donationRequired = donationRequired;
  }

  @Override
  public void delete() {
  }

  /**
   * Get the mc colour representation for the rank
   * 
   * @return
   */
  public String getMCColour() {
    return _mcColour;
  }

  /**
   * Get the hex colour representation for the rank
   * 
   * @return
   */
  public String getHexColour() {
    return _hexColour;
  }

  /**
   * Get the full name of the rank
   * 
   * @return
   */
  public String getName() {
    return _name;
  }

  /**
   * Get the order id the rank will be displayed in
   * 
   * @return
   */
  public int getOrder() {
    return _order;
  }

  public int getXenforoGroupId() {
    return _xenforoGroupId;
  }

  public double getDonationRequired() {
    return _donationRequired;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
  }

  /**
   * Check if the rank is a staff rank or not
   * 
   * @return
   */
  public boolean isStaff() {
    return _staff;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
  }

}
