/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.manager;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.obj.BaseItemMenu;
import co.forsaken.sessions.obj.SessionPlayer;

/**
 * Managing class to handle item menus
 * 
 * @author ForsakenNetwork-Dev
 */
public class ItemMenuManager extends BaseListener {
  private static Map<String, BaseItemMenu>  activeMenus  = new HashMap<String, BaseItemMenu>();
  private static Map<String, SessionPlayer> _cachedItems = new HashMap<String, SessionPlayer>();

  public ItemMenuManager(ForsakenGlobal plugin) {
    super(plugin);
  }

  public boolean hasMenu(String username, Class<? extends BaseItemMenu> cls) {
    if (activeMenus.containsKey(username.toLowerCase())) { return activeMenus.get(username.toLowerCase()).getClass().isInstance(cls); }
    return false;
  }

  /**
   * Adds a menu to be tracked
   * 
   * @param menu
   */
  public void addMenu(final BaseItemMenu menu) {
    if (activeMenus.containsKey(menu.getPlayerName().toLowerCase())) {
      activeMenus.get(menu.getPlayerName().toLowerCase()).closeInv();
      loadInv(activeMenus.get(menu.getPlayerName().toLowerCase()));
    }
    ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(_plugin, new Runnable() {
      public void run() {
        activeMenus.put(menu.getPlayerName().toLowerCase(), menu);
        saveInv(menu);
        menu.openInv();
      }
    }, 20L);
  }

  public void updateMenu(Class<? extends BaseItemMenu> cls) {
    for (BaseItemMenu menu : activeMenus.values()) {
      if (menu.getClass().isInstance(cls)) {
        menu.update();
      }
    }
  }

  public static void closeAny(String name) {
    if (activeMenus.containsKey(name.toLowerCase())) {
      activeMenus.get(name.toLowerCase()).closeInv();
      loadInv(activeMenus.get(name.toLowerCase()));
      activeMenus.remove(name.toLowerCase());
    }
  }

  private static void saveInv(BaseItemMenu menu) {
    _cachedItems.put(menu.getPlayerName().toLowerCase(), new SessionPlayer(menu.getUser()));
    menu.getUser().getPlayer().getInventory().clear();
    menu.getUser().getPlayer().updateInventory();
  }

  private static void loadInv(BaseItemMenu menu) {
    menu.getUser().getPlayer().getInventory().clear();
    menu.getUser().getPlayer().updateInventory();
    _cachedItems.get(menu.getPlayerName().toLowerCase()).apply(menu.getUser());
    _cachedItems.remove(menu.getPlayerName().toLowerCase());
  }

  /**
   * Checks when the inventories are closed and validates if its a menu
   * 
   * @param e
   */
  @EventHandler(priority = EventPriority.NORMAL)
  public void onInventoryClose(InventoryCloseEvent e) {
    Player p = (Player) e.getPlayer();
    if (activeMenus.containsKey(p.getName().toLowerCase())) {
      BaseItemMenu menu = activeMenus.get(p.getName().toLowerCase());
      if (menu.isMenu(e.getInventory(), p)) {
        menu.closeInv(e.getInventory());
        activeMenus.remove(p.getName().toLowerCase());
        loadInv(menu);
      }
    }
  }

  private Map<String, Long> _lastClick = new HashMap<String, Long>();

  @EventHandler(priority = EventPriority.NORMAL)
  public void onItemDrop(PlayerDropItemEvent e) {
    if (activeMenus.containsKey(e.getPlayer().getName().toLowerCase()) && !e.isCancelled() && !e.getPlayer().isOp()) {
      e.setCancelled(true);
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onItemPickup(PlayerPickupItemEvent e) {
    if (activeMenus.containsKey(e.getPlayer().getName().toLowerCase()) && !e.isCancelled() && !e.getPlayer().isOp()) {
      e.setCancelled(true);
    }
  }

  /**
   * Tracks menu clicks
   * 
   * @param e
   */
  @SuppressWarnings("deprecation")
  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
  public void onInventoryClick(InventoryClickEvent e) {
    Player p = (Player) e.getWhoClicked();
    if (_lastClick.containsKey(p.getName().toLowerCase()) && (System.currentTimeMillis() - _lastClick.get(p.getName().toLowerCase()) <= 250)) {
      e.setCancelled(true);
      return;
    }
    if (activeMenus.containsKey(p.getName().toLowerCase())) {
      BaseItemMenu menu = activeMenus.get(p.getName().toLowerCase());
      if (menu.isMenu(e.getInventory(), p)) {
        boolean shouldCancel = false;
        switch (e.getAction()) {
          case CLONE_STACK:
          case COLLECT_TO_CURSOR:
          case DROP_ALL_CURSOR:
          case DROP_ALL_SLOT:
          case DROP_ONE_CURSOR:
          case DROP_ONE_SLOT:
            shouldCancel = true;
          default:
            break;
        }
        boolean clickSlot = false;
        if (!shouldCancel) {
          clickSlot = menu.clickSlot(e.getRawSlot(), e.getCurrentItem(), e.getCursor(), e.isShiftClick(), e.isRightClick(), e.isLeftClick());
          _lastClick.put(p.getName().toLowerCase(), System.currentTimeMillis());
        }
        if (!clickSlot) {
          e.setCancelled(true);
          e.setResult(Result.DENY);

          p.updateInventory();
        } else if (menu.shouldStripItemOnClick()) {
          menu.initItems(e.getInventory());
          p.updateInventory();
        }
      }
    }
  }
}
