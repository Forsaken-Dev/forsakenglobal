/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.manager;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import co.forsaken.ForsakenGlobal;

/**
 * Handles the plugins configuration
 * 
 * @author ForsakenNetwork-Dev
 */
public class ConfigManager {
  private final ForsakenGlobal _plugin;
  private FileConfiguration    config     = null;
  private File                 saveFile   = null;
  private final String         pluginName;
  private boolean              hasUpdated = false;

  /**
   * Loads the default plugin config
   * 
   * @param plugin
   */
  public ConfigManager(ForsakenGlobal plugin) {
    _plugin = plugin;
    this.pluginName = plugin.getDescription().getName();
    Logger.getLogger("Minecraft").info("[" + pluginName + "] Loading Config...");
    this.config = plugin.getConfig();
    this.saveFile = new File(plugin.getDataFolder() + "/config.yml");
  }

  /**
   * Loads a custom config file name
   * 
   * @param plugin
   * @param fileName
   */
  public ConfigManager(ForsakenGlobal plugin, String fileName) {
    _plugin = plugin;
    this.pluginName = plugin.getDescription().getName();
    Logger.getLogger("Minecraft").info("[" + pluginName + "] Loading Config...");
    this.config = plugin.getConfig();
    this.saveFile = new File(plugin.getDataFolder() + fileName);
  }

  /**
   * Add a default config value
   * 
   * @param path
   * @param value
   */
  public void addDefault(String path, Object value) {
    if (!config.contains(path)) {
      if (value instanceof String) {
        config.set(path, value);
      } else {
        config.set(path, value);
      }
      hasUpdated = true;
    }
  }

  /**
   * Get a boolean from the config
   * 
   * @param path
   * @return
   */
  public boolean getBoolean(String path) {
    if (config.contains(path)) return config.getBoolean(path);
    return false;
  }

  /**
   * Get a boolean list from the config
   * 
   * @param path
   * @return
   */
  public List<Boolean> getBooleanList(String path) {
    if (!config.contains(path)) return config.getBooleanList(path);
    return null;
  }

  /**
   * Get a double from the config
   * 
   * @param path
   * @return
   */
  public double getDouble(String path) {
    if (config.contains(path)) return config.getDouble(path);
    return 0;
  }

  /**
   * Get an integer from the config
   * 
   * @param path
   * @return
   */
  public int getInt(String path) {
    if (config.contains(path)) return config.getInt(path);
    return 0;
  }

  /**
   * Get an itemstack from the config
   * 
   * @param path
   * @return
   */
  public ItemStack getItemStack(String path) {
    if (config.contains(path)) return config.getItemStack(path);
    return null;
  }

  /**
   * Get a long from the config
   * 
   * @param path
   * @return
   */
  public long getLong(String path) {
    if (config.contains(path)) return config.getLong(path);
    return 0;
  }

  /**
   * Get a string from the config
   * 
   * @param path
   * @return
   */
  public String getString(String path) {
    if (config.contains(path)) return config.getString(path);
    return null;
  }

  /**
   * Get a string list from the config
   * 
   * @param path
   * @return
   */
  public List<String> getStringList(String path) {
    if (config.contains(path)) return config.getStringList(path);
    return null;
  }

  /**
   * Reload the configs contents
   */
  public void reload() {
    _plugin.reloadConfig();
    this.config = _plugin.getConfig();
    this.saveConfig();
  }

  /**
   * If the config has been updated save it to a file
   */
  public void saveConfig() {
    if (hasUpdated) {
      Logger.getLogger("Minecraft").info("[" + pluginName + "] Saving Config...");
      try {
        config.save(saveFile);
      } catch (IOException e) {
        e.printStackTrace();
      }
      hasUpdated = false;
    }
  }

  /**
   * Hard set a value in the config
   * 
   * @param node
   * @param value
   */
  public void set(String node, Object value) {
    config.set(node, value);
    this.saveConfig();
  }
}
