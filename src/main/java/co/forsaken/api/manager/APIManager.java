/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.manager;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseConnection;

/**
 * API manager to handle managers for the plugins
 * 
 * @author ForsakenNetwork-Dev
 */
public class APIManager {
  protected final ForsakenGlobal _plugin;
  protected MessageManager       _messageManager;
  protected ConfigManager        _configManager;
  protected ItemMenuManager      _menuManager;

  public APIManager(ForsakenGlobal plugin) {
    _plugin = plugin;
  }

  public ItemMenuManager getItemMenuManager() {
    if (_menuManager == null) {
      _menuManager = new ItemMenuManager(_plugin);
    }
    return _menuManager;
  }

  /**
   * Gets the configuration manager
   * 
   * @return
   */
  public ConfigManager getConfigManager() {
    if (_configManager == null) {
      _configManager = new ConfigManager(_plugin);
    }
    return _configManager;
  }

  /**
   * Gets a single instance of the database class (only one connection =
   * ++performance & less network jumble)
   * 
   * @return
   */
  public DatabaseConnection getDatabase() {
    return ForsakenGlobal.getInstance().getDatabaseConnection();
  }

  /**
   * Gets a single instance of the factory manager (single instance =
   * ++performance as the data is only cached once)
   * 
   * @return
   */
  public FactoryManager getFactoryManager() {
    return ForsakenGlobal.getInstance().getFactoryManager();
  }

  /**
   * Gets the message manager
   * 
   * @return
   */
  public MessageManager getMessageManager() {
    if (_messageManager == null) {
      _messageManager = new MessageManager(_plugin);
    }
    return _messageManager;
  }

}