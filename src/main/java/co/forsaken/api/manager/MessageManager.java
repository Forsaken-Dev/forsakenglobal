/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.manager;

import java.util.logging.Logger;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

/**
 * Main message management to unify messages
 * 
 * @author ForsakenNetwork-Dev
 */
public class MessageManager {
  /**
   * Level of the logs
   */
  public static enum Level {
    INFO, WARNING, SEVERE;
  }

  private final ForsakenGlobal _plugin;
  private static String        HEADER_TEXT = "&l&c!&r";
  private static String        HEADER      = "&8[" + MessageManager.HEADER_TEXT + "&8]&7";
  private static Logger        log         = Logger.getLogger("Minecraft");

  public MessageManager(ForsakenGlobal plugin) {
    _plugin = plugin;
  }

  /**
   * Log a message to console, we may want to do something more for severe
   * exceptions
   * 
   * @param severity
   * @param msg
   */
  public void log(Level severity, String msg) {
    String message = MessageUtils.format("[" + MessageManager.HEADER_TEXT + "] " + msg, false);
    switch (severity) {
      default:
      case INFO:
        MessageManager.log.info(message);
        break;
      case WARNING:
        MessageManager.log.warning(message);
        try {
          _plugin
              .getAPI()
              .getDatabase()
              .write("INSERT INTO forsaken_error_log (message, server_id) VALUES(?, ?);", msg,
                  _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
        } catch (Exception e) {
          _plugin.getAPI().getDatabase().write("INSERT INTO forsaken_error_log (message, server_id) VALUES(?, ?);", msg, 0);
        }
        // sendDebugMessage(message, new int[] { 6, 7, 8 });
        break;
      case SEVERE:
        MessageManager.log.severe(message);
        try {
          _plugin
              .getAPI()
              .getDatabase()
              .write("INSERT INTO forsaken_error_log (message, server_id) VALUES(?, ?);", msg,
                  _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
        } catch (Exception e) {
          _plugin.getAPI().getDatabase().write("INSERT INTO forsaken_error_log (message, server_id) VALUES(?, ?);", msg, 0);
        }
        sendDebugMessage(message, new int[] { 6, 7, 8 });
        break;
    }
  }

  public void sendDebugMessage(String msg, int[]... ranksToSendTo) {
    try {
      for (User u : _plugin.getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
        if ((ranksToSendTo.length >= 1 && hasRank(u.getPrimaryRankId(), ranksToSendTo[0]) && u.shouldReceiveLogMessages())
            || (ranksToSendTo.length == 0 && u.shouldReceiveLogMessages())) {
          u.sendMessage(msg);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public boolean hasRank(int rankId, int[] ranks) {
    for (int i : ranks) {
      if (i == rankId) { return true; }
    }
    return false;
  }

  /**
   * Send a message to the whole server
   * 
   * @param msg
   */
  public void sendGlobalMessage(String msg, boolean format) {
    sendGlobalMessage(msg, true, format);
  }

  /**
   * Send a message to the whole server
   * 
   * @param msg
   */
  public void sendGlobalMessage(String msg, boolean header, boolean format) {
    for (Player p : _plugin.getServer().getOnlinePlayers()) {
      sendMessage(p, msg, header, format);
    }
  }

  /**
   * Send a message to a single user with the header
   * 
   * @param sender
   * @param msg
   */
  public void sendMessage(CommandSender sender, String msg) {
    sendMessage(sender, msg, true);
  }

  /**
   * Send a message to a single user
   * 
   * @param sender
   * @param msg
   * @param header
   */
  public void sendMessage(CommandSender sender, String msg, boolean header) {
    sendMessage(sender, msg, header, true);
  }

  /**
   * Send a message to a single user
   * 
   * @param sender
   * @param msg
   * @param header
   * @param formatting
   */
  public void sendMessage(CommandSender sender, String msg, boolean header, boolean formatting) {
    MessageUtils.send(sender, MessageUtils.format((header ? MessageManager.HEADER + " " : ""), true) + MessageUtils.format(msg, formatting));
  }

  /**
   * Send a message to a single user with a header and no formatting
   * 
   * @param sender
   * @param msg
   */
  public void sendMessageNoFormat(CommandSender sender, String msg) {
    sendMessage(sender, msg, true, false);
  }
}
