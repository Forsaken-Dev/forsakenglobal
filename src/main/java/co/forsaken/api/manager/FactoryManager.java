/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.manager;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.PunishmentFactory;
import co.forsaken.api.factories.RankFactory;
import co.forsaken.api.factories.ServerFactory;
import co.forsaken.api.factories.UserFactory;

/**
 * Manages all cached factories
 * 
 * @author ForsakenNetwork-Dev
 */
public class FactoryManager {
  private final ServerFactory     _serverFactory;
  private final RankFactory       _rankFactory;
  private final UserFactory       _userFactory;
  private final PunishmentFactory _punishmentFactory;

  /**
   * Loads all factories
   * 
   * @param plugin
   */
  public FactoryManager(ForsakenGlobal plugin) {
    _serverFactory = new ServerFactory(plugin);
    _rankFactory = new RankFactory(plugin);
    _userFactory = new UserFactory(plugin);
    _punishmentFactory = new PunishmentFactory(plugin);
    ForsakenGlobal._loadedProperly = true;
  }

  /**
   * Get the punishment factory
   * 
   * @return
   */
  public PunishmentFactory getPunishmentFactory() {
    return _punishmentFactory;
  }

  /**
   * Get the rank factory
   * 
   * @return
   */
  public RankFactory getRankFactory() {
    return _rankFactory;
  }

  /**
   * Get the server factory
   * 
   * @return
   */
  public ServerFactory getServerFactory() {
    return _serverFactory;
  }

  /**
   * Get the user factory
   * 
   * @return
   */
  public UserFactory getUserFactory() {
    return _userFactory;
  }
}
