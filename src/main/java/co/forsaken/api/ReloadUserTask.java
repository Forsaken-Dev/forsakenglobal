/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

public class ReloadUserTask implements Runnable {

  private ForsakenGlobal _ForsakenGlobal;
  private int            _userIdToReload = -1;
  private boolean        _returnMsg      = true;

  public ReloadUserTask(ForsakenGlobal ForsakenGlobal, int userIdToReload) {
    _ForsakenGlobal = ForsakenGlobal;
    _userIdToReload = userIdToReload;
    _ForsakenGlobal.getServer().getScheduler().runTaskLater(_ForsakenGlobal, this, 1);
  }

  public ReloadUserTask(ForsakenGlobal ForsakenGlobal, int userIdToReload, boolean returnMsg) {
    this(ForsakenGlobal, userIdToReload);
    _returnMsg = returnMsg;
  }

  @Override
  public void run() {
    try {
      _ForsakenGlobal.getAPI().getFactoryManager().getUserFactory().reload(_userIdToReload);
      if (_returnMsg) {
        ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", ForsakenGlobal._instance.getAPI().getFactoryManager().getUserFactory().get(_userIdToReload).getName() + "|reload");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    _ForsakenGlobal.getAPI().getMessageManager().log(Level.INFO, "Reloaded cache for user " + _userIdToReload);

  }
}
