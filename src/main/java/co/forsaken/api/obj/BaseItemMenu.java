/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.obj;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.ItemUtils;
import co.forsaken.api.utils.SerializationUtils;

public abstract class BaseItemMenu {

  protected ForsakenGlobal          _plugin;
  protected static final int        MAX_SIZE            = 6 * 9;
  protected String                  menuName            = "";
  protected String                  playerName;
  protected Map<Integer, ItemStack> menuItems           = new HashMap<Integer, ItemStack>();
  protected List<ItemStack>         itemsBefore         = new ArrayList<ItemStack>();
  protected int                     menuSize            = -1;
  protected boolean                 stripOnClickSuccess = false;

  public BaseItemMenu(ForsakenGlobal plugin, String menuName, String playerName) {
    _plugin = plugin;
    this.menuName = menuName;
    this.playerName = playerName;
    this.loadItems();
  }

  public BaseItemMenu(ForsakenGlobal plugin, String menuName, String playerName, int menuSize) {
    this(plugin, menuName, playerName);
    this.menuSize = menuSize;
  }

  public BaseItemMenu(ForsakenGlobal plugin, String menuName, String playerName, int menuSize, boolean stripOnClick) {
    this(plugin, menuName, playerName, menuSize);
    this.stripOnClickSuccess = stripOnClick;
  }

  /**
   * Load all the items within the item menu
   */
  protected abstract void loadItems();

  /**
   * Get the player whos item menu this is
   * 
   * @return
   */
  public String getPlayerName() {
    return playerName;
  }

  /**
   * Shortcut to get the user
   * 
   * @return
   */
  public User getUser() {
    User user = null;
    try {
      user = _plugin.getAPI().getFactoryManager().getUserFactory().get(playerName);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return user;
  }

  public void update() {
    menuItems.clear();
    itemsBefore.clear();
    loadItems();
  }

  /**
   * Add an item to the menu
   * 
   * @param item
   * @return
   */
  protected boolean addItem(ItemStack item) {
    if (menuItems.size() > getMaxMenuSize()) { return false; }
    if (item == null || (item != null && item.getAmount() <= 0)) { return true; }
    int maxAmount = item.getMaxStackSize();
    int currentAmount = item.getAmount();
    List<ItemStack> itemsToAdd = new ArrayList<ItemStack>();

    while (currentAmount > maxAmount) {
      ItemStack tmp = item.clone();
      tmp.setAmount(maxAmount);
      itemsToAdd.add(tmp);
      currentAmount -= maxAmount;
    }
    item.setAmount(currentAmount);
    itemsToAdd.add(item);
    if (menuItems.size() + itemsToAdd.size() >= getMaxMenuSize()) {
      return false;
    } else {
      itemToAdd: for (ItemStack itemToAdd : itemsToAdd) {
        menuItems: for (Integer i : menuItems.keySet()) {
          if (menuItems.get(i).isSimilar(itemToAdd)) {
            if (menuItems.get(i).getAmount() + itemToAdd.getAmount() >= itemToAdd.getMaxStackSize()) {
              menuItems.get(i).setAmount(itemToAdd.getMaxStackSize());
              if (menuItems.get(i).getAmount() + itemToAdd.getAmount() > itemToAdd.getMaxStackSize()) {
                itemToAdd.setAmount(itemToAdd.getAmount() + menuItems.get(i).getAmount() - itemToAdd.getMaxStackSize());
                break menuItems;
              }
              continue itemToAdd;
            } else {
              menuItems.get(i).setAmount(menuItems.get(i).getAmount() + itemToAdd.getAmount());
              continue itemToAdd;
            }
          }
        }
        menuItems.put(menuItems.size(), itemToAdd);
      }
      return true;
    }
  }

  /**
   * Add an item at a specific slot
   * 
   * @param startSlot
   * @param item
   * @return
   */
  protected boolean addItem(int startSlot, ItemStack item) {
    if (menuItems.size() > getMaxMenuSize()) { return false; }
    if (item == null || (item != null && item.getAmount() <= 0)) { return true; }
    int maxAmount = item.getMaxStackSize();
    int currentAmount = item.getAmount();
    List<ItemStack> itemsToAdd = new ArrayList<ItemStack>();

    while (currentAmount > maxAmount) {
      ItemStack tmp = item.clone();
      tmp.setAmount(maxAmount);
      itemsToAdd.add(tmp);
      currentAmount -= maxAmount;
    }
    item.setAmount(currentAmount);
    itemsToAdd.add(item);
    if (menuItems.size() + itemsToAdd.size() >= getMaxMenuSize()) {
      return false;
    } else {
      int index = 0;
      for (ItemStack itemToAdd : itemsToAdd) {
        menuItems.put(startSlot + index, itemToAdd);
        index++;
      }
      return true;
    }
  }

  /**
   * Get the name of the menu
   * 
   * @return
   */
  public String getName() {
    return menuName.length() > 32 ? menuName.substring(0, 32) : menuName;
  }

  /**
   * Checks if an inventory is the valid menu
   * 
   * @param inv
   * @param player
   * @return
   */
  public boolean isMenu(Inventory inv, Player player) {
    return inv.getName().equalsIgnoreCase(getName()) && player.getName().equalsIgnoreCase(playerName);
  }

  public boolean shouldStripItemOnClick() {
    return this.stripOnClickSuccess;
  }

  /**
   * Get the max size of the menu
   * 
   * @return
   */
  protected int getMaxMenuSize() {
    return MAX_SIZE;
  }

  /**
   * Get the actual size of this menu
   * 
   * @return
   */
  protected int getMenuSize() {
    int menuSize = menuItems.size();
    for (Integer i : menuItems.keySet()) {
      if (i > menuSize) {
        menuSize = i;
      }
    }
    if (this.menuSize != -1 && this.menuSize > menuSize) {
      menuSize = this.menuSize;
    }
    if (((double) menuSize / 9D) <= 6D) {
      return (int) Math.ceil((double) menuSize / 9D) * 9;
    } else {
      return MAX_SIZE;
    }
  }

  /**
   * Gets how many slots in the menu are in use
   * 
   * @return
   */
  protected int getActiveSlotsUsed() {
    return menuItems.size();
  }

  /**
   * Checks if the menu has space
   * 
   * @return
   */
  protected boolean hasSpace() {
    return this.getActiveSlotsUsed() < this.getMaxMenuSize();
  }

  /**
   * Shortcut to check if two items are the same
   * 
   * @param item1
   * @param item2
   * @return
   */
  protected boolean itemEquals(ItemStack item1, ItemStack item2) {
    return (item1 != null && item2 != null && item1.getType().name().equals(item2.getType().name()) && item1.getDurability() == item2.getDurability())
        || (item1 == null && item2 == null);
  }

  /**
   * Open the item menu
   */
  public void openInv() {
    _plugin.getServer().getScheduler().runTaskLater(_plugin, new Runnable() {
      public void run() {
        User user = getUser();
        if (user != null && user.getPlayer() != null && user.getPlayer().isOnline()) {
          Inventory inv = Bukkit.createInventory(user.getPlayer(), getMenuSize(), getName());
          initItems(inv);
          user.getPlayer().openInventory(inv);
        }
      }
    }, 10);
  }

  public Map<Integer, ItemStack> getMenuItems() {
    return menuItems;
  }

  public void initItems(Inventory inv) {
    for (int i = 0; i < inv.getSize(); i++) {
      if (!menuItems.containsKey(i)) {
        inv.clear(i);
      }
    }
    for (Integer i : menuItems.keySet()) {
      inv.setItem(i, menuItems.get(i));
      itemsBefore.add(menuItems.get(i).clone());
    }
  }

  /**
   * Force close the item menu
   */
  public void closeInv() {
    _plugin.getServer().getScheduler().runTaskLater(_plugin, new Runnable() {
      public void run() {
        User user = getUser();
        if (user != null && user.getPlayer() != null && user.getPlayer().isOnline()) {
          user.getPlayer().closeInventory();
        }
      }
    }, 1);
  }

  /**
   * Called when a player clicks on the specified slot
   * 
   * @param slot
   * @param itemInSlot
   * @param itemInHand
   * @param isShiftClick
   * @return
   */
  public abstract boolean clickSlot(int slot, ItemStack itemInSlot, ItemStack itemInHand, boolean isShiftClick, boolean isRightClick,
      boolean isLeftClick);

  /**
   * Called when the inventory is closed, spits out any extra items and tracks
   * the items still in the inventory
   * 
   * @param inv
   */
  @SuppressWarnings("deprecation")
  public void closeInv(Inventory inv) {
    Map<String, Integer> thrownItems = new HashMap<String, Integer>();
    Map<String, Integer> originalItems = new HashMap<String, Integer>();
    Map<String, Integer> missingItems = new HashMap<String, Integer>();
    Map<String, Integer> newItems = new HashMap<String, Integer>();
    Map<String, ItemStack> items = new HashMap<String, ItemStack>();
    for (ItemStack i : Arrays.asList(inv.getContents())) {
      if (i == null) {
        continue;
      }
      String itemName = i.getType().name() + ":" + i.getDurability() + ":" + ItemUtils.getItemEnchantments(i);
      try {
        itemName = SerializationUtils.serialize(i);
      } catch (IOException e) {
      }

      int amount = i.getAmount();
      if (newItems.containsKey(itemName)) {
        amount += newItems.get(itemName);
      }
      newItems.put(itemName, amount);
      if (items.containsKey(itemName)) {
        items.get(itemName).setAmount(amount + items.get(itemName).getAmount());
      } else {
        items.put(itemName, i);
      }
    }

    for (ItemStack i : itemsBefore) {
      if (i == null) {
        continue;
      }
      String itemName = i.getType().name() + ":" + i.getDurability() + ":" + ItemUtils.getItemEnchantments(i);
      try {
        itemName = SerializationUtils.serialize(i);
      } catch (IOException e) {
      }

      int amount = i.getAmount();
      if (originalItems.containsKey(itemName)) {
        amount += originalItems.get(itemName);
      }
      originalItems.put(itemName, amount);
      missingItems.put(itemName, amount);
      if (items.containsKey(itemName)) {
        items.get(itemName).setAmount(amount + items.get(itemName).getAmount());
      } else {
        items.put(itemName, i);
      }
    }

    newItems: for (String newItem : newItems.keySet()) {
      int newAmount = newItems.get(newItem);
      for (String originalItem : originalItems.keySet()) {
        int originalAmount = originalItems.get(originalItem);
        if (newItem.equals(originalItem)) {
          if (originalAmount == newAmount) {
            missingItems.remove(originalItem);
          } else if (originalAmount < newAmount) {
            int amountThrown = newAmount - originalAmount;
            thrownItems.put(originalItem, amountThrown);
            missingItems.remove(originalItem);
          } else if (originalAmount > newAmount) {
            int amountMissing = originalAmount - newAmount;
            missingItems.put(originalItem, amountMissing);
          }
          originalItems.remove(originalItem);
          continue newItems;
        }
      }
      thrownItems.put(newItem, newAmount);
    }

    for (String itemName : originalItems.keySet()) {
      int amount = originalItems.get(itemName);
      missingItems.put(itemName, amount);
    }

    if (getUser() != null) {
      for (String itemName : missingItems.keySet()) {
        ItemStack item = items.get(itemName).clone();
        item.setAmount(missingItems.get(itemName));
        itemMissing(item);
      }
      if (getUser().getPlayer().isOnline()) {
        for (String itemName : thrownItems.keySet()) {
          ItemStack item = items.get(itemName).clone();
          item.setAmount(thrownItems.get(itemName));
          extraItem(item);
        }
      }
    }
  }

  /**
   * Called when an item is determined to be missing from the menu
   * 
   * @param item
   */
  protected abstract void itemMissing(ItemStack item);

  /**
   * Default case to deal with extra items put into the menu, dropping them on
   * the ground for now
   * 
   * @param item
   */
  protected void extraItem(ItemStack item) {
    int amount = item.getAmount();
    while (amount > 0) {
      if (amount >= item.getMaxStackSize()) {
        item.setAmount(item.getMaxStackSize());
        getUser().getPlayer().getWorld().dropItem(getUser().getPlayer().getLocation(), item.clone());
        amount -= item.getMaxStackSize();
        continue;
      }
      item.setAmount(amount);
      getUser().getPlayer().getWorld().dropItem(getUser().getPlayer().getLocation(), item.clone());
      break;
    }
  }
}
