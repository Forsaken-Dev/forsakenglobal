/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import org.bukkit.command.CommandSender;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.irc.IRCBotListener;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandsManager;

public class Registrar {

  private ForsakenGlobal                 _plugin;

  /**
   * sk89q command manager with reference to all loaded commands
   */
  private CommandsManager<CommandSender> _commands;

  /**
   * sk89q command registration used to register all command classes
   */
  private CommandsManagerRegistration    _commandsManagerRegistrar;

  public Registrar(ForsakenGlobal plugin) {
    _plugin = plugin;
    _commands = new CommandsManager<CommandSender>() {
      @Override
      public boolean hasPermission(CommandSender sender, String perm) {
        return sender.hasPermission(perm);
      }
    };
    _commandsManagerRegistrar = new CommandsManagerRegistration(plugin, _commands);
  }

  public CommandsManager<CommandSender> getCommands() {
    return _commands;
  }

  public void register(Class<?> cls) {
    _commandsManagerRegistrar.register(cls);
    try {
      IRCBotListener.register(cls);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
