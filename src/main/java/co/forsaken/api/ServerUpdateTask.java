/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Server;
import co.forsaken.server.lobby.LobbyRegistrar;

public class ServerUpdateTask implements Runnable {

  private ForsakenGlobal _ForsakenGlobal;
  private int            _serverId;
  private int            _taskId = -1;

  public ServerUpdateTask(ForsakenGlobal ForsakenGlobal, int serverId) {
    _ForsakenGlobal = ForsakenGlobal;
    _serverId = serverId;
    _taskId = _ForsakenGlobal.getServer().getScheduler().runTaskTimer(_ForsakenGlobal, this, 20L, 5 * 20L).getTaskId();
  }

  @Override
  public void run() {
    try {
      Server s = _ForsakenGlobal.getAPI().getFactoryManager().getServerFactory().get(_serverId);
      boolean ping = s.pingServer();
      if (ping) {
        s.setActiveInfo(true, 20.0F, _ForsakenGlobal.getAPI().getFactoryManager().getUserFactory().getOnlineOn(s.getName()).size(), 100);
      } else {
        s.setActiveInfo(false, 0.0F, 0, 0);
      }
      if (ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("lobby")) {
        LobbyRegistrar.updateServer(s);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void cancel() {
    if (_taskId > 0) {
      _ForsakenGlobal.getServer().getScheduler().cancelTask(_taskId);
    }
  }
}
