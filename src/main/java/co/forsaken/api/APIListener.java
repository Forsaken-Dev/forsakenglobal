/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import net.minecraft.server.v1_6_R3.MinecraftServer;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.XenforoUtils;

public class APIListener extends BaseListener {

  public APIListener(ForsakenGlobal plugin) {
    super(plugin);
  }

  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
  public void onCommandPreprocess(ServerCommandEvent event) {
    permissionsCmd(event.getCommand());
    _plugin.getAPI().getMessageManager()
        .sendDebugMessage(event.getSender().getName() + " ran  \"" + event.getCommand() + "\"", new int[] { 6, 7, 8 });
    if (event.getCommand().split(" ")[0].equalsIgnoreCase("say")) {
      event.setCommand(event.getCommand().replace("say", "salert"));
    }
  }

  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
  public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
    if ((event.getMessage().split(" ")[0].equalsIgnoreCase("/plugins") || event.getMessage().split(" ")[0].equalsIgnoreCase("/pl")
        || event.getMessage().split(" ")[0].equalsIgnoreCase("/mods") || event.getMessage().split(" ")[0].equalsIgnoreCase("/me") || (event
        .getMessage().split(" ")[0].equalsIgnoreCase("/cofh") && (event.getMessage().split(" ")[1].equalsIgnoreCase("killall") || event.getMessage()
        .split(" ")[1].equalsIgnoreCase("tps"))))
        && !event.getPlayer().isOp()) {
      event.setCancelled(true);
      ForsakenGlobal.getInstance().getAPI().getMessageManager()
          .sendMessage(event.getPlayer(), "&cYou do not have permissions to perform this command", true, true);
      return;
    }
    if (event.getMessage().split(" ")[0].equalsIgnoreCase("/say")) {
      event.setMessage(event.getMessage().replace("say", "salert"));
      return;
    }
    if (event.getMessage().split(" ")[0].equalsIgnoreCase("/help") || event.getMessage().split(" ")[0].equalsIgnoreCase("/modreq") || event.getMessage().split(" ")[0].equalsIgnoreCase("/ticket") || event.getMessage().split(" ")[0].equalsIgnoreCase("/pe")) {
      String msg = "&8[&l&c!&r&8] &cWebsite&7: &aNews &6@ &bhttp://www.forsaken.co &7(&eClick Link&7)";
      msg += "&/&8[&l&c!&r&8] &cVote Site&7: &aVote &6@ &bhttp://www.forsaken.co/vote &7(&eClick Link&7)";
      msg += "&/&8[&l&c!&r&8] &cWebstore&7: &aDonate &6@ &bhttp://www.forsaken.co/donate &7(&eClick Link&7)";
      msg += "&/&8[&l&c!&r&8] &cHelp&7: &aSupport &6@ &bhttp://www.forsaken.co/support &7(&eClick Link&7)";
      msg += "&/&8[&l&c!&r&8] &cTeamspeak 3&7: &aVoice &6@ &bts.forsaken.co";
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendMessage(event.getPlayer(), msg, false, true);
      event.setCancelled(true);
      return;
    }
    if (event.getMessage().split(" ")[0].equalsIgnoreCase("/tps") || event.getMessage().split(" ")[0].equalsIgnoreCase("/ping")) {
      double tps = (MinecraftServer.currentTPS > 20D ? 20D : MinecraftServer.currentTPS);
      char color = 'a';
      if (tps >= 18D) {
        color = 'a';
      } else if (tps >= 10.5) {
        color = 'e';
      } else {
        color = 'c';
      }
      User user = null;
      try {
        user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (user == null) { return; }
      user.sendMessage("&8[&l&c!&r&8] &eThe server is currently at &" + color + new DecimalFormat("#.##").format(tps) + " TPS");
      event.setCancelled(true);
      return;
    }
    permissionsCmd(event.getMessage());
    _plugin.getAPI().getMessageManager()
        .sendDebugMessage(event.getPlayer().getName() + " ran  \"" + event.getMessage() + "\"", new int[] { 6, 7, 8 });
  }

  public void permissionsCmd(String cmd) {
    if (cmd.startsWith("/")) {
      cmd = cmd.replaceFirst("/", "");
    }
    if (cmd.toLowerCase().contains("pex promote") || cmd.toLowerCase().contains("pex demote") || cmd.toLowerCase().contains("group add") || cmd.toLowerCase().contains("group set") || cmd.toLowerCase().contains("group remove")) {
      XenforoUtils.sync(cmd.split(" ")[2]);
      try {
        new ReloadUserTask(_plugin, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(cmd.split(" ")[2]).getId());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerDeath(PlayerDeathEvent event) {
    String color = "&7";
    String msg = "";
    msgLoop: for (String s : event.getDeathMessage().split(" ")) {
      if (!msg.isEmpty()) {
        msg += " ";
      } else {
        msg += color;
      }
      for (Player p : _plugin.getServer().getOnlinePlayers()) {
        if (s.equalsIgnoreCase(p.getName())) {
          if (event.getEntity().getName().equalsIgnoreCase(s)) {
            msg += "&a";
          } else {
            msg += "&c";
          }
          msg += s + color;
          continue msgLoop;
        }
      }
      msg += s;
    }
    if (msg == null || msg == "" || msg.isEmpty() || MessageUtils.cleanMsgEnding(MessageUtils.format(msg, false)).isEmpty()) {
      event.setDeathMessage(null);
    } else {
      event.setDeathMessage(MessageUtils.format("&8[&4&l⚔&r&8] " + msg, true));
    }
    if (event.getEntity().getKiller() != null && event.getEntity().getKiller() instanceof Player) {
      try {
        _plugin
            .getAPI()
            .getDatabase()
            .write("INSERT INTO forsaken_user_deaths (user_id, killer_id, weapon, cause) VALUES (?, ?, ?, '');",
                _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity()).getName()).getId(),
                _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity().getKiller()).getName()).getId(),
                ((Player) event.getEntity().getKiller()).getItemInHand().getType().name());
      } catch (Exception e) {
      }
    } else if (event.getEntity().getLastDamageCause() != null && event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent
        && ((EntityDamageByEntityEvent) event.getEntity().getLastDamageCause()).getDamager() instanceof Projectile) {
      Projectile damager = (Projectile) ((EntityDamageByEntityEvent) event.getEntity().getLastDamageCause()).getDamager();
      if (damager.getShooter() instanceof Player) {
        try {
          _plugin
              .getAPI()
              .getDatabase()
              .write(
                  "INSERT INTO forsaken_user_deaths (user_id, killer_id, weapon, cause) VALUES (?, ?, ?, ?);",
                  _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity()).getName()).getId(),
                  _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) damager.getShooter()).getName()).getId(),
                  ((Player) event.getEntity().getKiller()).getItemInHand().getType().name(),
                  (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent ? ((EntityDamageByEntityEvent) event.getEntity()
                      .getLastDamageCause()).getDamager().getType().name() : event.getEntity().getLastDamageCause().getCause().name()));
        } catch (Exception e) {
        }
      } else {
        try {
          _plugin
              .getAPI()
              .getDatabase()
              .write(
                  "INSERT INTO forsaken_user_deaths (user_id, killer_id, weapon, cause) VALUES (?, ?, ?, ?);",
                  _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity()).getName()).getId(),
                  1,
                  (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent ? ((EntityDamageByEntityEvent) event.getEntity()
                      .getLastDamageCause()).getDamager().getType().name() : event.getEntity().getLastDamageCause().getCause().name()),
                  damager.getShooter().getType().name());
        } catch (Exception e) {
        }
      }
    } else {
      try {
        _plugin
            .getAPI()
            .getDatabase()
            .write(
                "INSERT INTO forsaken_user_deaths (user_id, killer_id, weapon, cause) VALUES (?, ?, '', ?);",
                _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity()).getName()).getId(),
                1,
                (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent ? ((EntityDamageByEntityEvent) event.getEntity()
                    .getLastDamageCause()).getDamager().getType().name() : event.getEntity().getLastDamageCause().getCause().name()));
      } catch (Exception e) {
      }
    }
  }

  HashMap<String, Long> timestamp = new HashMap<String, Long>();

  @EventHandler(priority = EventPriority.NORMAL)
  public void onBlockPlaceEvent(BlockPlaceEvent evt) {
    Player player = evt.getPlayer();
    if (player.isOp()) { return; }
    if (evt.getBlockPlaced() != null && evt.getBlockPlaced().getTypeId() == 2548) {
      if (timestamp.get(player.getName().toLowerCase()) == null || (timestamp.get(player.getName().toLowerCase()) != null && timestamp.get(player.getName().toLowerCase()) < (System.currentTimeMillis() - 8000))) {
        player.playSound(player.getLocation(), Sound.ANVIL_LAND, 1, 0);
        MessageUtils.send(player, "&8[&l&c!&r&8] &cLarge OpenBlock Tanks use a ton of resources, please keep them small!");
        timestamp.put(player.getName().toLowerCase(), System.currentTimeMillis());
      }

      Set<Block> known = new LinkedHashSet<Block>();
      LinkedList<Block> toProcess = new LinkedList<Block>();
      toProcess.add(evt.getBlockPlaced());

      while (!toProcess.isEmpty()) {
        Block current = toProcess.pop();

        for (BlockFace f : BlockFace.values()) {
          Block next = current.getRelative(f);
          if (next != null && next.getTypeId() == 2548 && known.add(next)) {
            toProcess.offer(next);
          }

          if (known.size() > 8) {
            MessageUtils.send(player, "&8[&l&c!&r&8] Do you wanna kill the server? Sorry this structure is getting too large");
            player.playSound(player.getLocation(), Sound.GHAST_SCREAM, 1, 0);
            evt.setCancelled(true);
            return;
          }
        }
      }
    }
  }
}
