/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.tasks;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.managers.TeleportManager;

/**
 * @author vanZeben
 */
public class DelayedCommandTask implements Runnable {
  private ForsakenGlobal   _plugin;
  public int               _taskId      = -1;
  private static final int TOTAL_TIME   = 3;
  private int              _numSeconds  = TOTAL_TIME;
  private User             _user;
  private String           _commandToExec;
  private String           _startMessage;
  private String           _finalMessage;
  private boolean          _fromConsole = true;
  public boolean           isCompleted  = false;
  private boolean          _override    = false;

  public DelayedCommandTask(ForsakenGlobal plugin, User user, String commandToExec, String startMessage, String finalMessage, boolean fromConsole) {
    _plugin = plugin;
    _user = user;
    _commandToExec = commandToExec;
    _startMessage = startMessage;
    _finalMessage = finalMessage;
    _fromConsole = fromConsole;
    _taskId = plugin.getServer().getScheduler().runTaskTimer(plugin, this, 0, 20).getTaskId();
  }

  public DelayedCommandTask(ForsakenGlobal plugin, User user, String commandToExec, String startMessage, String finalMessage, boolean fromConsole,
      boolean override) {
    _plugin = plugin;
    _user = user;
    _commandToExec = commandToExec;
    _startMessage = startMessage;
    _finalMessage = finalMessage;
    _fromConsole = fromConsole;
    _override = override;
    if (override) {
      _numSeconds = 0;
    }
    _taskId = plugin.getServer().getScheduler().runTaskTimer(plugin, this, 0, 20).getTaskId();

  }

  public void run() {
    if (_user.getPlayer() == null) {
      _plugin.getServer().getScheduler().cancelTask(_taskId);
      isCompleted = true;
      return;
    }
    if (_numSeconds == 0) {
      if (_commandToExec.startsWith("tp")) {
        String[] location = _commandToExec.replace("tp ", "").split(" ");
        if (location.length >= 4) {
          _user.teleport(location[0], Integer.parseInt(location[1]), Integer.parseInt(location[2]), Integer.parseInt(location[3]));
        }
      } else {
        if (_fromConsole) {
          _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), _commandToExec);
        } else {
          if (_user.getPlayer() != null && _user.getPlayer().isOnline()) {
            _plugin.getServer().dispatchCommand(_user.getPlayer(), _commandToExec);
          }
        }
      }
      if (_finalMessage != null) {
        _user.sendMessage(_finalMessage);
      }
      addEffect(_user.getPlayer(), TOTAL_TIME, true);
    } else if (_numSeconds == TOTAL_TIME) {
      if (_startMessage != null) {
        _user.sendMessage(_startMessage);
      }
      addEffect(_user.getPlayer(), ((TOTAL_TIME * 2) + 1), false);
    }
    if (_numSeconds > 0) {
      _user.sendMessage("&l&c" + _numSeconds);
    }
    _numSeconds -= 1;
    if ((_override) || (_numSeconds <= (0 - TOTAL_TIME) && _taskId != -1)) {
      isCompleted = true;
      _plugin.getServer().getScheduler().cancelTask(_taskId);
    }
  }

  private void addEffect(Player player, int seconds, boolean blindness) {
    if (player == null || (player != null && !player.isOnline())) { return; }
    player.removePotionEffect(PotionEffectType.CONFUSION);
    player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, seconds * 20, 5));

    if (blindness) {
      player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, seconds * 20, 5));
      player.removePotionEffect(PotionEffectType.BLINDNESS);
    }
  }
}
