/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.DisallowedItemException;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.InvalidFilenameException;
import com.sk89q.worldedit.InvalidItemException;
import com.sk89q.worldedit.LocalConfiguration;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.LocalWorld;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.UnknownItemException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldVector;
import com.sk89q.worldedit.bags.BlockBag;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.blocks.BlockType;
import com.sk89q.worldedit.blocks.ClothColor;
import com.sk89q.worldedit.blocks.NoteBlock;
import com.sk89q.worldedit.blocks.SignBlock;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.patterns.Pattern;
import com.sk89q.worldedit.patterns.SingleBlockPattern;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.regions.RegionSelector;
import com.sk89q.worldedit.schematic.SchematicFormat;

public class WorldEditUtils {
  private static WorldEditPlugin _wePlugin;

  public static BaseBlock getBlock(String arg, LocalWorld world) throws UnknownItemException, DisallowedItemException {
    BlockType blockType;
    arg = arg.replace("_", " ");
    arg = arg.replace(";", "|");
    String[] blockAndExtraData = arg.split("\\|");
    String[] typeAndData = blockAndExtraData[0].split(":", 2);
    String testID = typeAndData[0];
    int blockId = -1;

    int data = -1;

    // Attempt to parse the item ID or otherwise resolve an item/block
    // name to its numeric ID
    try {
      blockId = Integer.parseInt(testID);
      blockType = BlockType.fromID(blockId);
    } catch (NumberFormatException e) {
      blockType = BlockType.lookup(testID);
      if (blockType == null) {

      }
    }

    if ((blockId == -1) && (blockType == null)) {
      // Maybe it's a cloth
      ClothColor col = ClothColor.lookup(testID);

      if (col != null) {
        blockType = BlockType.CLOTH;
        data = col.getID();
      } else {
        throw new UnknownItemException(arg);
      }
    }

    // Read block ID
    if (blockId == -1) {
      blockId = blockType.getID();
    }

    if (!world.isValidBlockType(blockId)) { throw new UnknownItemException(arg); }

    if (data == -1) { // Block data not yet detected
      // Parse the block data (optional)
      try {
        data = typeAndData.length > 1 ? Integer.parseInt(typeAndData[1]) : 0;
        if ((data > 15) || ((data < 0) && !(data == -1))) {
          data = 0;
        }
      } catch (NumberFormatException e) {
        if (blockType != null) {
          switch (blockType) {
            case CLOTH:
              ClothColor col = ClothColor.lookup(typeAndData[1]);

              if (col != null) {
                data = col.getID();
              } else {
                throw new InvalidItemException(arg, "Unknown cloth color '" + typeAndData[1] + "'");
              }
              break;

            case STEP:
            case DOUBLE_STEP:
              BlockType dataType = BlockType.lookup(typeAndData[1]);

              if (dataType != null) {
                switch (dataType) {
                  case STONE:
                    data = 0;
                    break;

                  case SANDSTONE:
                    data = 1;
                    break;

                  case WOOD:
                    data = 2;
                    break;

                  case COBBLESTONE:
                    data = 3;
                    break;
                  case BRICK:
                    data = 4;
                    break;
                  case STONE_BRICK:
                    data = 5;

                  default:
                    throw new InvalidItemException(arg, "Invalid step type '" + typeAndData[1] + "'");
                }
              } else {
                throw new InvalidItemException(arg, "Unknown step type '" + typeAndData[1] + "'");
              }
              break;

            default:
              throw new InvalidItemException(arg, "Unknown data value '" + typeAndData[1] + "'");
          }
        } else {
          throw new InvalidItemException(arg, "Unknown data value '" + typeAndData[1] + "'");
        }
      }
    }

    // Check if the item is allowed
    if (blockType != null) {
      switch (blockType) {
        case SIGN_POST:
        case WALL_SIGN:
          // Allow special sign text syntax
          String[] text = new String[4];
          text[0] = blockAndExtraData.length > 1 ? blockAndExtraData[1] : "";
          text[1] = blockAndExtraData.length > 2 ? blockAndExtraData[2] : "";
          text[2] = blockAndExtraData.length > 3 ? blockAndExtraData[3] : "";
          text[3] = blockAndExtraData.length > 4 ? blockAndExtraData[4] : "";
          return new SignBlock(blockType.getID(), data, text);

        case MOB_SPAWNER:

        case NOTE_BLOCK:
          if (blockAndExtraData.length > 1) {
            byte note = Byte.parseByte(blockAndExtraData[1]);
            if ((note < 0) || (note > 24)) {
              throw new InvalidItemException(arg, "Out of range note value: '" + blockAndExtraData[1] + "'");
            } else {
              return new NoteBlock(data, note);
            }
          } else {
            return new NoteBlock(data, (byte) 0);
          }

        default:
          return new BaseBlock(blockId, data);
      }
    } else {
      return new BaseBlock(blockId, data);
    }
  }

  /**
   * Gets a cuboid from two locations passed
   * 
   * @param worldName
   *          World to get cuboid from
   * @param placementPosition
   *          Where the "player" is standing when they get the cuboid
   * @param maxPoint
   *          Top point of the cuboid
   * @param minPoint
   *          Bottom point of the cuboid
   * @return CuboidClipboard The cuboid object for other use
   * @throws IncompleteRegionException
   *           Thrown if the points to not match up
   */
  public static CuboidClipboard getCuboidFromLocation(String worldName, Vector pos, Vector max, Vector min)
      throws IncompleteRegionException {

    LocalSession session = getLocalSession();
    RegionSelector selector = session.getRegionSelector(getLocalWorld(worldName));
    selector.selectPrimary(toVector(min));
    selector.selectSecondary(toVector(max));
    Region region = selector.getRegion();
    EditSession editSession = new EditSession(getLocalWorld(worldName), -1);

    CuboidClipboard clipboard = new CuboidClipboard(max.subtract(min).add(Vector.ONE), min, min.subtract(pos));
    if (region instanceof CuboidRegion) {
      clipboard.copy(editSession);
    } else {
      clipboard.copy(editSession, region);
    }
    return clipboard;
  }

  /**
   * Gets the configuation of WorldEdit
   * 
   * @return LocalConfiguration Configuration
   */
  private static LocalConfiguration getLocalConfig() {
    return getWorldEdit().getWorldEdit().getConfiguration();
  }

  /**
   * Gets a new session
   * 
   * @return LocalSession Session used for other tasks
   */
  private static LocalSession getLocalSession() {
    return new LocalSession(getLocalConfig());
  }

  /**
   * Transfers a worldname into a LocalWorld
   * 
   * @param worldName
   *          Name of world to convert
   * @return LocalWorld LocalWorld object from Worldedit
   */
  private static LocalWorld getLocalWorld(String worldName) {
    return new BukkitWorld(ForsakenGlobal.getInstance().getServer().getWorld(worldName));
  }

  public static void pasteSchematicAtLocation(String worldName, String schematicName, Location loc) {
    try {
      pasteSchematicAtVector(worldName, schematicName, toVector(loc));
    } catch (EmptyClipboardException e) {
      e.printStackTrace();
    } catch (DataException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Pastes a pre-existing Schematic at any coordinates
   * 
   * @param worldName
   *          World to paste in
   * @param schematicName
   *          Schematic to paste
   * @param loc
   *          Location to paste the Schematic
   * @throws DataException
   *           Thrown if the data doesnt match
   * @throws IOException
   *           Thrown if the schematic cannot be reached
   * @throws EmptyClipboardException
   *           Thrown if the schematic is empty
   */
  public static void pasteSchematicAtVector(String worldName, String schematicName, Vector loc) throws DataException, IOException,
      EmptyClipboardException {

    // variables
    LocalSession session = getLocalSession();
    LocalWorld localWorld = getLocalWorld(worldName);
    Vector pos = WorldVector.toBlockPoint(localWorld, loc.getX(), loc.getY(), loc.getZ());

    // load
    String filename = schematicName + ".schematic";
    File dir = getWorldEdit().getWorldEdit().getWorkingDirectoryFile(getLocalConfig().saveDir);
    File f = new File(dir, filename);
    session.setClipboard(CuboidClipboard.loadSchematic(f));

    BlockBag blockBag = null;
    // paste
    EditSession editSession = new EditSession(localWorld, session.getBlockChangeLimit(), blockBag);
    editSession.setFastMode(session.hasFastMode());
    editSession.setMask(session.getMask());

    try {
      session.getClipboard().paste(editSession, pos, false);
    } catch (MaxChangedBlocksException e) {
      e.printStackTrace();
    }
  }

  public static boolean regenArea(Location minLocation, Location maxLocation) {
    LocalSession session = getLocalSession();
    RegionSelector selector = session.getRegionSelector(getLocalWorld(minLocation.getWorld().getName()));
    selector.selectPrimary(toVector(minLocation));
    selector.selectSecondary(toVector(maxLocation));
    EditSession editSession = new EditSession(getLocalWorld(minLocation.getWorld().getName()), -1);
    Region region = null;
    try {
      region = selector.getRegion();
      getLocalWorld(minLocation.getWorld().getName()).regenerate(region, editSession);
    } catch (IncompleteRegionException e) {
      e.printStackTrace();
    }
    return true;
  }

  /**
   * Saves a cuboid to memory
   * 
   * @param schematicName
   *          Name of schematic to save
   * @param schematic
   *          Cuboid of the area wanted to save
   * @throws InvalidFilenameException
   *           Thrown if the file already exists
   * @throws EmptyClipboardException
   *           Thrown if no cuboid is passed
   * @throws IOException
   *           Thrown if the file location cannot be reached
   * @throws DataException
   *           Thrown if the save fails
   */
  public static void saveSchematic(String schematicName, CuboidClipboard schematic) throws InvalidFilenameException,
      EmptyClipboardException, IOException, DataException {

    String filename = schematicName + ".schematic";
    if (!filename.matches("^[A-Za-z0-9_\\- \\./\\\\'\\$@~!%\\^\\*\\(\\)\\[\\]\\+\\{\\},\\?]+\\.[A-Za-z0-9]+$")) { throw new InvalidFilenameException(
        filename, "Invalid characters or extension missing"); }
    File dir = getWorldEdit().getWorldEdit().getWorkingDirectoryFile(getLocalConfig().saveDir);
    File f = new File(dir, filename);
    if (!dir.exists()) {
      if (!dir.mkdir()) {
        ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.WARNING, "Error on Saving schematic. The storage folder could not be created.");
      }
    }
    File parent = f.getParentFile();
    if ((parent != null) && !parent.exists()) {
      parent.mkdirs();
    }

    SchematicFormat format = SchematicFormat.getFormat("mce");
    format.save(schematic, f);
  }

  public static void saveSchematicFromCoords(String schematicName, String worldName, Location savePoint, Location minPoint, Location maxPoint) {
    try {
      saveSchematic(
          schematicName,
          getCuboidFromLocation(worldName, toVector(savePoint), toVector(maxPoint), toVector(minPoint)));
    } catch (InvalidFilenameException e) {
      e.printStackTrace();
    } catch (EmptyClipboardException e) {
      e.printStackTrace();
    } catch (IncompleteRegionException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (DataException e) {
      e.printStackTrace();
    }
  }

  public static void saveSchematicFromCoords(String schematicName, String worldName, Location minPoint, Location maxPoint) {
    try {
      saveSchematic(
          schematicName,
          getCuboidFromLocation(worldName, toVector(minPoint), toVector(maxPoint), toVector(minPoint)));
    } catch (InvalidFilenameException e) {
      e.printStackTrace();
    } catch (EmptyClipboardException e) {
      e.printStackTrace();
    } catch (IncompleteRegionException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (DataException e) {
      e.printStackTrace();
    }
  }

  public static int setAreaWithBlock(String world, Location minPoint, Location maxPoint, String blockId) {
    LocalSession session = getLocalSession();
    RegionSelector selector = session.getRegionSelector(getLocalWorld(world));
    selector.selectPrimary(toVector(minPoint));
    selector.selectSecondary(toVector(maxPoint));
    EditSession editSession = new EditSession(getLocalWorld(world), -1);
    Pattern pattern = null;
    try {
      pattern = new SingleBlockPattern(getBlock(blockId, getLocalWorld(world)));
    } catch (UnknownItemException e) {
      e.printStackTrace();
    } catch (DisallowedItemException e) {
      e.printStackTrace();
    }
    int affected = -1;

    if (pattern instanceof SingleBlockPattern) {
      try {
        affected = editSession.setBlocks(selector.getRegion(), ((SingleBlockPattern) pattern).getBlock());
      } catch (MaxChangedBlocksException e) {
        e.printStackTrace();
      } catch (IncompleteRegionException e) {
        e.printStackTrace();
      }
    } else {
      try {
        affected = editSession.setBlocks(selector.getRegion(), pattern);
      } catch (MaxChangedBlocksException e) {
        e.printStackTrace();
      } catch (IncompleteRegionException e) {
        e.printStackTrace();
      }

    }
    return affected;
  }

  public static int replaceAreaWithBlock(String world, Location minPoint, Location maxPoint, String blockId, String replacedId) {
    LocalSession session = getLocalSession();
    RegionSelector selector = session.getRegionSelector(getLocalWorld(world));
    selector.selectPrimary(toVector(minPoint));
    selector.selectSecondary(toVector(maxPoint));
    EditSession editSession = new EditSession(getLocalWorld(world), -1);
    Pattern to = null;
    Set<BaseBlock> from = new HashSet<BaseBlock>();
    try {
      to = new SingleBlockPattern(getBlock(blockId, getLocalWorld(world)));
      String[] items = replacedId.split(",");
      for (String id : items) {
        from.add(getBlock(id, getLocalWorld(world)));
      }
    } catch (UnknownItemException e) {
      e.printStackTrace();
    } catch (DisallowedItemException e) {
      e.printStackTrace();
    }
    int affected = -1;

    if (to instanceof SingleBlockPattern) {
      try {
        affected = editSession.replaceBlocks(selector.getRegion(), from, ((SingleBlockPattern) to).getBlock());
      } catch (MaxChangedBlocksException e) {
        e.printStackTrace();
      } catch (IncompleteRegionException e) {
        e.printStackTrace();
      }
    } else {
      try {
        affected = editSession.replaceBlocks(selector.getRegion(), from, to);
      } catch (MaxChangedBlocksException e) {
        e.printStackTrace();
      } catch (IncompleteRegionException e) {
        e.printStackTrace();
      }

    }
    return affected;
  }

  public static Location getMinLocation(Player player) {
    Selection selection = getWorldEdit().getSelection(player);
    return selection.getMinimumPoint();
  }

  public static Location getMaxLocation(Player player) {
    Selection selection = getWorldEdit().getSelection(player);
    return selection.getMaximumPoint();
  }

  public static WorldEditPlugin getWorldEdit() {
    if (_wePlugin == null) {
      _wePlugin = (WorldEditPlugin) ForsakenGlobal.getInstance().getServer().getPluginManager().getPlugin("WorldEdit");
    }
    return _wePlugin;
  }

  /**
   * Converts a object to a location
   * 
   * @param world
   *          World of the object
   * @param object
   *          Object to convert
   * @return Location
   */
  public static Location toLocation(com.sk89q.worldedit.Location location) {
    return new Location(ForsakenGlobal.getInstance().getServer().getWorld(location.getWorld().getName()), location.getPosition().getX(), location.getPosition().getY(), location.getPosition().getZ(), location.getYaw(), location.getPitch());
  }

  /**
   * Converts a player to a location
   * 
   * @param player
   * @return
   */
  public static Location toLocation(Player player) {
    Location loc = new Location(player.getWorld(), (int) player.getLocation().getX(), -128, (int) player.getLocation().getZ());
    return loc;
  }

  /**
   * Converts a vector to a location
   * 
   * @param world
   *          World of the vector
   * @param vector
   *          Vector to convert
   * @return Location
   */
  private static Location toLocation(World world, Vector vector) {
    return BukkitUtil.toLocation(world, vector);
  }

  /**
   * Used to convert a set of coords to a vector object
   * 
   * @param x
   * @param y
   * @param z
   * @return Vector Vector object
   */
  private static Vector toVector(double x, double y, double z) {
    return new Vector(x, y, z);
  }

  /**
   * Used to convert a location to a vector object
   * 
   * @param loc
   *          Location to convert
   * @return Vector Vector object
   */
  public static Vector toVector(Location loc) {
    return new Vector(loc.getX(), loc.getY(), loc.getZ());
  }

  /**
   * Attempts to convert an object into a vector
   * 
   * @param object
   *          Object to convert
   * @return Vector Vector object
   */
  private static Vector toVector(Object object) {
    return (Vector) object;
  }
}
