/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

import com.avaje.ebeaninternal.server.lib.sql.DataSourceException;

/**
 * MySQL Database connection
 * 
 * @author ForsakenNetwork-Dev
 */
public class DatabaseConnection {
  private final ForsakenGlobal _plugin;
  private Connection           _conn;
  private boolean              _showQueries = true;
  private final String         _connectionUrl;
  private String               _databaseName;
  private final String         _username;
  private final String         _password;

  /**
   * Initialize the drivers and database and connect to it
   * 
   * @param plugin
   * @param connectionUrl
   * @param databaseName
   * @param username
   * @param password
   */
  public DatabaseConnection(ForsakenGlobal plugin, String connectionUrl, String databaseName, String username, String password) {
    _plugin = plugin;
    _connectionUrl = connectionUrl;
    _databaseName = databaseName;
    _username = username;
    _password = password;
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
    } catch (Exception ex) {
      _plugin.getAPI().getMessageManager().log(Level.SEVERE, "Failed to initialize JDBC driver");
    }

    ensureConnection();
  }

  /**
   * Connect to mysql with the default parameters
   * 
   * @throws Exception
   */
  private void connect() throws Exception {
    try {
      _conn = DriverManager.getConnection(getConnectionString());
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataSourceException("Failed to connect to MySQL");
    }
  }

  /**
   * Connect to mysql with a custom database
   * 
   * @param database
   * @throws Exception
   */
  public void connect(String database) throws Exception {
    _databaseName = database;
    this.connect();
  }

  /**
   * Display the sql error in a nice manner
   * 
   * @param ex
   */
  private void dumpSqlException(SQLException ex) {
    System.out.println("SQLException: " + ex.getMessage());
    System.out.println("SQLState: " + ex.getSQLState());
    System.out.println("VendorError: " + ex.getErrorCode());
    ex.printStackTrace();
  }

  /**
   * Validate that we still have a connection to the server, if not create it
   */
  private void ensureConnection() {
    try {
      if ((_conn == null) || !_conn.isValid(5)) {
        try {
          this.connect();
        } catch (Exception e) {
          if (e.getMessage() != null) {
            _plugin.getAPI().getMessageManager().log(Level.SEVERE, e.getMessage());
          } else {
            e.printStackTrace();
          }
        }
      }
    } catch (SQLException ex) {
      this.dumpSqlException(ex);
    }
  }

  /**
   * Get the MySQL Connection
   * 
   * @return
   */
  public Connection getConn() {
    return _conn;
  }

  /**
   * Get a string representation of the mysql server with all auth
   * 
   * @return
   */
  private String getConnectionString() {
    return "jdbc:mysql://" + _connectionUrl + "/" + _databaseName + "?user=" + _username + "&password=" + _password;
  }

  /**
   * Escape an SQL statement to avoid injections
   * 
   * @param sql
   * @param params
   * @return
   * @throws SQLException
   */
  private PreparedStatement prepareSqlStatement(String sql, Object[] params) throws SQLException {
    PreparedStatement stmt = _conn.prepareStatement(sql);

    int counter = 1;

    for (Object param : params) {
      if (param instanceof Integer) {
        stmt.setInt(counter++, (Integer) param);
      } else if (param instanceof Short) {
        stmt.setShort(counter++, (Short) param);
      } else if (param instanceof Long) {
        stmt.setLong(counter++, (Long) param);
      } else if (param instanceof Double) {
        stmt.setDouble(counter++, (Double) param);
      } else if (param instanceof String) {
        stmt.setString(counter++, (String) param);
      } else if (param == null) {
        stmt.setNull(counter++, Types.NULL);
      } else if (param instanceof Object) {
        stmt.setObject(counter++, param);
      } else {
        System.out.printf("Database -> Unsupported data type %s", param.getClass().getSimpleName());
      }
    }
    if (this.shouldShowQueries()) {
      _plugin.getAPI().getMessageManager().log(Level.INFO, stmt.toString());
    }

    return stmt;
  }

  /**
   * Read data from MySQL into a nice object for us to parse
   * 
   * @param sql
   * @param params
   * @return
   */
  public DatabaseResults readEnhanced(String sql, Object... params) {
    this.ensureConnection();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    DatabaseResults results = null;

    try {
      stmt = this.prepareSqlStatement(sql, params);
      rs = stmt.executeQuery();
      if (rs != null) {
        ResultSetMetaData meta = rs.getMetaData();
        results = new DatabaseResults(meta);
        while (rs.next()) {
          results.addRow(rs);
        }
      }
    } catch (SQLException ex) {
      this.dumpSqlException(ex);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
        }
        rs = null;
      }
      if (stmt != null) {
        try {
          stmt.close();
        } catch (SQLException sqlEx) {
        }
        stmt = null;
      }
      if ((results == null) || !results.hasRows() || (results.rawResults == null)) return null;
    }
    return results;
  }

  /**
   * Read the raw data from MySQL
   * 
   * @param sql
   * @param params
   * @return
   */
  public HashMap<Integer, ArrayList<Object>> readRaw(String sql, Object... params) {
    this.ensureConnection();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    HashMap<Integer, ArrayList<Object>> Rows = new HashMap<Integer, ArrayList<Object>>();

    try {
      stmt = this.prepareSqlStatement(sql, params);
      if (stmt.executeQuery() != null) {
        stmt.executeQuery();
        rs = stmt.getResultSet();
        while (rs.next()) {
          ArrayList<Object> Col = new ArrayList<Object>();
          for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            Col.add(rs.getString(i));
          }
          Rows.put(rs.getRow(), Col);
        }
      }
    } catch (SQLException ex) {
      this.dumpSqlException(ex);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
        }
        rs = null;
      }
      if (stmt != null) {
        try {
          stmt.close();
        } catch (SQLException sqlEx) {
        }
        stmt = null;
      }
      if (Rows.isEmpty() || (Rows == null) || (Rows.get(1) == null)) return null;
    }
    return Rows;
  }

  /**
   * Set if the server should be displaying the queries sent
   * 
   * @param showQueries
   */
  public void setShowQueries(boolean showQueries) {
    _showQueries = showQueries;
  }

  /**
   * Whether or not the server is showing all queries sent
   * 
   * @return
   */
  public boolean shouldShowQueries() {
    return _showQueries;
  }

  /**
   * Prettify a table name
   * 
   * @param nameOfTable
   * @return
   */
  public String tableName(String nameOfTable) {
    return (String.format("`%s`.`%s`", _databaseName, nameOfTable));
  }

  /**
   * Write data to mysql
   * 
   * @param sql
   * @param params
   * @return
   */
  public boolean write(String sql, Object... params) {
    try {
      this.ensureConnection();
      PreparedStatement stmt = this.prepareSqlStatement(sql, params);
      stmt.executeUpdate();
    } catch (SQLException ex) {
      this.dumpSqlException(ex);
      return false;
    }
    return true;
  }

  /**
   * Write data to mysql and throw away any exceptions generated
   * 
   * @param sql
   * @param params
   * @return
   */
  public boolean writeNoError(String sql, Object... params) {
    try {
      this.ensureConnection();
      PreparedStatement stmt = this.prepareSqlStatement(sql, params);
      stmt.executeUpdate();
      return true;
    } catch (SQLException ex) {
      return false;
    }
  }

}
