/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils.db;

import co.forsaken.ForsakenGlobal;

/**
 * Essentially a row pertaining to a table
 * 
 * @author ForsakenNetwork-Dev
 * @param <T>
 */
public abstract class DatabaseObject<T> {

  protected ForsakenGlobal _plugin;
  protected int            _id;

  public DatabaseObject(ForsakenGlobal plugin, int id) {
    _plugin = plugin;
    _id = id;
  }

  public DatabaseObject(ForsakenGlobal plugin) {
    _plugin = plugin;
  }

  /**
   * Get the database id
   * 
   * @return
   */
  public int getId() {
    return _id;
  }

  /**
   * Delete the row
   */
  public abstract void delete();

  /**
   * Insert the row into the table
   */
  public abstract void insert();

  /**
   * Refresh the row
   */
  public abstract void refresh();

  /**
   * push the contents of the row to the database
   */
  public abstract void update();
}
