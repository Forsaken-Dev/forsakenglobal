/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils.db;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

/**
 * Essentially a database table and the cache of its contents
 * 
 * @author ForsakenNetwork-Dev
 */
public abstract class DatabaseFactory<T extends DatabaseObject<T>> {
  protected final ForsakenGlobal _plugin;
  protected Map<String, T>       _databaseObjects = new HashMap<String, T>();
  protected String               _tableName;

  public DatabaseFactory(ForsakenGlobal plugin, String tableName) {
    _plugin = plugin;
    _tableName = ForsakenGlobal.getInstance().getAPI().getConfigManager().getString("mysql.table.prefix") + tableName;
    initObject();
  }

  /**
   * Create the table and insert default rows if necessary
   * 
   * @param arguments
   */
  public abstract void createTable();

  /**
   * Check if the table already exists in the database
   * 
   * @return
   * @throws SQLException
   */
  public boolean doesTableExist() throws SQLException {
    DatabaseMetaData metadata = _plugin.getAPI().getDatabase().getConn().getMetaData();
    ResultSet resultSet;
    resultSet = metadata.getTables(null, null, _tableName, null);
    return resultSet.next();
  }

  public T reload(int id) throws Exception {
    remove(id);
    return get(id);
  }

  /**
   * Get a cached object if it exists, otherwise attempt to load and return it
   * 
   * @param key
   * @return
   * @throws Exception
   */
  public T get(int id) throws Exception {
    for (T obj : getAll()) {
      if (obj.getId() == id) return obj;
    }

    String sql = "SELECT * FROM " + _tableName + " WHERE `id` = ?;";
    DatabaseResults query = _plugin.getAPI().getDatabase().readEnhanced(sql, id);
    if ((query != null) && query.hasRows()) {
      try {
        T s = parseFromQuery(query, 0);
        _databaseObjects.put(s.getId() + "", s);
        return s;
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    }
    throw new Exception("Invalid ID: " + id);
  }

  /**
   * Get a list of cached objects, otherwise load them
   * 
   * @param key
   * @return
   * @throws Exception
   */
  public List<T> get(List<Integer> ids) throws Exception {
    List<T> objects = new ArrayList<T>();
    for (Integer id : ids) {
      objects.add(get(id));
    }
    return objects;
  }

  /**
   * Get all values cached
   * 
   * @return
   */
  @SuppressWarnings("unchecked")
  public List<T> getAll() {
    ArrayList<T> objs = new ArrayList<T>();
    for (Object obj : _databaseObjects.values()) {
      if (obj instanceof DatabaseObject<?>) {
        objs.add((T) obj);
      }
    }
    return objs;
  }

  /**
   * Get the table name
   * 
   * @return
   */
  public String getTableName() {
    return _tableName;
  }

  /**
   * Initialize the object, creating any tables if its needed
   */
  protected void initObject() {
    try {
      if (!doesTableExist()) {
        _plugin.getAPI().getMessageManager().log(Level.INFO, _tableName + " was not found. Creating it now from the schema");
        createTable();
      }
    } catch (SQLException e) {
      e.printStackTrace();
      createTable();
    }
  }

  public void remove(int id) {
    if (_databaseObjects.containsKey(id + "")) {
      _databaseObjects.remove(id + "");
    }
  }

  /**
   * Parse a DatabseObject from a query given
   * 
   * @param query
   * @param row
   * @return
   * @throws SQLDataException
   * @throws Exception
   */
  public abstract T parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception;

  /**
   * Reload the cache
   */
  public void reload() {
    _databaseObjects.clear();
    initObject();
  }
}
