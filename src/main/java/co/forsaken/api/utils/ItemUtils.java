/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtils {

  public static ItemStack getItemName(ItemStack item, String name, ArrayList<String> lore) {
    if (item == null) { return item; }
    try {
      ItemMeta meta = item.getItemMeta();
      if (lore == null) {
        lore = new ArrayList<String>();
      }
      for (int i = 0; i < lore.size(); i++) {
        lore.set(i, MessageUtils.format(lore.get(i), true));
      }
      meta.setLore(lore);
      if (name != null) {
        meta.setDisplayName(name);
      }
      item.setItemMeta(meta);
    } catch (ArrayIndexOutOfBoundsException e) {
      e.printStackTrace();
      return item;
    } catch (Exception e) {
      e.printStackTrace();
      return item;
    }
    return item;
  }

  public static void giveItemToPlayer(Player player, List<ItemStack> items, boolean replaceArmour) {
    List<ItemStack> tmpList = new ArrayList<ItemStack>();
    for (ItemStack item : items) {
      if (item.getTypeId() == 1311 && replaceArmour) {
        if (player.getInventory().getHelmet() != null && player.getInventory().getHelmet().getTypeId() != 0) {
          tmpList.add(player.getInventory().getHelmet().clone());
        }
        player.getInventory().setHelmet(item);
      } else {
        tmpList.add(item);
      }
    }
    for (ItemStack item : tmpList) {
      HashMap<Integer, ItemStack> failedItems = player.getInventory().addItem(item);
      for (ItemStack i : failedItems.values()) {
        player.getWorld().dropItemNaturally(player.getLocation(), i);
      }
    }
  }

  public static void giveItemToPlayer(Player player, List<ItemStack> items) {
    for (ItemStack item : items) {
      HashMap<Integer, ItemStack> failedItems = player.getInventory().addItem(item);
      for (ItemStack i : failedItems.values()) {
        player.getWorld().dropItemNaturally(player.getLocation(), i);
      }
    }
  }

  public static String getItemEnchantments(ItemStack item) {
    Map<Enchantment, Integer> enchantments = item.getEnchantments();
    String enchant = "";
    if (!enchantments.isEmpty()) {
      int i = 0;
      for (Enchantment e : enchantments.keySet()) {
        if (i > 0) {
          enchant += ";";
        }
        enchant += e.getName() + ":" + enchantments.get(e);
        i++;
      }
    }
    return enchant;
  }

}
