/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.io.File;

import org.bukkit.World;

import co.forsaken.ForsakenGlobal;

import com.google.common.base.Preconditions;

/**
 * @author ForsakenNetwork-Dev
 */
public class WorldUtils {

  /**
   * Gets a world id from the friendly name
   * 
   * @param world
   * @return
   */
  public static int getWorldId(String name) {
    if (name.equalsIgnoreCase("world")) return 0;
    else return Integer.parseInt(name.replaceAll("DIM_MYST", "").replaceAll("DIM", ""));
  }

  /**
   * Gets a worlds name from the id
   * 
   * @param dimId
   * @return
   */
  public static String getWorldName(int dimId) {
    switch (dimId) {
      case -17:
        return "DIM-17";
      case -1:
        return "DIM-1";
      case 0:
        return "world";
      case 1:
        return "DIM1";
      case 7:
        return "DIM7";
      case 20:
        return "DIM20";
      default:
        return "DIM_MYST" + dimId;
    }
  }

  public static String getPlayerFilePath(String playerName) {
    Preconditions.checkNotNull("playerName", "playerName cannot be NULL");
    World world = ForsakenGlobal.getInstance().getServer().getWorld("world");
    File worldDirectory = world.getWorldFolder();

    if (!worldDirectory.exists()) throw new IllegalArgumentException(
        String.format("Cannot find world %s: Directory %s doesn't exist.", world.getName(), worldDirectory));
    return new File(new File(worldDirectory, "players"), playerName + ".dat").getAbsolutePath();
  }

  public static String getPlayerFilePath(World world, String playerName) {
    Preconditions.checkNotNull("world", "world cannot be NULL");
    Preconditions.checkNotNull("playerName", "playerName cannot be NULL");
    File worldDirectory = world.getWorldFolder();

    if (!worldDirectory.exists()) throw new IllegalArgumentException(
        String.format("Cannot find world %s: Directory %s doesn't exist.", world.getName(), worldDirectory));
    return new File(new File(worldDirectory, "players"), playerName + ".dat").getAbsolutePath();
  }

}
