/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.lang.reflect.Method;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * Extra firework effects to blow up fireworks at locations
 * 
 * @author ForsakenNetwork-Dev
 */
public class FireworkUtils {

  /**
   * Internal method, used as shorthand to grab our method in a nice friendly
   * manner
   * 
   * @param cl
   * @param method
   * @return Method (or null)
   */
  private static Method getMethod(Class<?> cl, String method) {
    for (Method m : cl.getMethods()) {
      if (m.getName().equals(method)) return m;
    }
    return null;
  }

  // internal references, performance improvements
  private static Method world_getHandle                 = null;
  private static Method nms_world_broadcastEntityEffect = null;

  private static Method firework_getHandle              = null;

  public static FireworkEffect getRandomFirework() {
    Random rand = new Random();
    Type type;
    switch (rand.nextInt(4)) {
      default:
      case 1:
        type = Type.BALL;
      case 2:
        type = Type.BURST;
      case 3:
        type = Type.STAR;
      case 4:
        type = Type.BALL_LARGE;
    }

    return FireworkEffect.builder().with(type).withColor(Color.fromRGB(rand.nextInt(128), rand.nextInt(255), rand.nextInt(128)))
        .withFade(Color.fromRGB(rand.nextInt(200), rand.nextInt(255), rand.nextInt(200))).trail(rand.nextBoolean()).flicker(rand.nextBoolean())
        .build();
  }

  /**
   * Play a pretty firework at the location with the FireworkEffect when called
   * 
   * @param world
   * @param loc
   * @param fe
   * @throws Exception
   */
  public static void playFirework(World world, Location loc, FireworkEffect fe) throws Exception {
    Firework fw = world.spawn(loc, Firework.class);
    Object nms_world = null;
    Object nms_firework = null;
    if (world_getHandle == null) {
      world_getHandle = FireworkUtils.getMethod(world.getClass(), "getHandle");
      firework_getHandle = FireworkUtils.getMethod(fw.getClass(), "getHandle");
    }
    nms_world = world_getHandle.invoke(world, (Object[]) null);
    nms_firework = firework_getHandle.invoke(fw, (Object[]) null);
    if (nms_world_broadcastEntityEffect == null) {
      nms_world_broadcastEntityEffect = FireworkUtils.getMethod(nms_world.getClass(), "broadcastEntityEffect");
    }
    FireworkMeta data = fw.getFireworkMeta();
    data.clearEffects();
    data.setPower(0);
    data.addEffect(fe);
    fw.setFireworkMeta(data);
    if (nms_world_broadcastEntityEffect != null && nms_world != null && nms_firework != null) {
      nms_world_broadcastEntityEffect.invoke(nms_world, new Object[] { nms_firework, (byte) 17 });
    }
    fw.remove();
  }

  /**
   * Play a random firework at the location with the FireworkEffect when called
   * 
   * @param world
   * @param loc
   * @param fe
   * @throws Exception
   */
  public static void playRandomFirework(World world, Location loc) throws Exception {
    playFirework(world, loc, getRandomFirework());
  }

}