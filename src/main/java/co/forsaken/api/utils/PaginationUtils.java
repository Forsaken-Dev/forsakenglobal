/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility for creating message menu pages
 * 
 * @author ForsakenNetwork-Dev
 */
public class PaginationUtils {
  private static final int NUM_ELEMENTS_PER_PAGE = 10;

  /**
   * Get the current page
   * 
   * @param currentSelectedPage
   * @param numPages
   * @return
   */
  public static int getCurrentPage(int currentSelectedPage, int numPages) {
    if (currentSelectedPage < 1) {
      currentSelectedPage = 1;
    } else if (currentSelectedPage > numPages) {
      currentSelectedPage = numPages;
    }
    return currentSelectedPage;
  }

  /**
   * Get how many page are in the message total
   * 
   * @param inputLines
   * @return
   */
  public static int getNumPages(List<String> inputLines) {
    int numPages = 0;
    if ((inputLines.size() % PaginationUtils.NUM_ELEMENTS_PER_PAGE) != 0) {
      for (int i = 0; i < PaginationUtils.NUM_ELEMENTS_PER_PAGE; i++) {
        if (((inputLines.size() + i) % PaginationUtils.NUM_ELEMENTS_PER_PAGE) == 0) {
          numPages = ((inputLines.size() + i) / PaginationUtils.NUM_ELEMENTS_PER_PAGE);
        }
      }
    } else {
      numPages = (inputLines.size() / PaginationUtils.NUM_ELEMENTS_PER_PAGE);
    }
    return numPages;
  }

  /**
   * Create the messages and parse out the curernt page to send
   * 
   * @param inputLines
   * @param currentSelectedPage
   * @return
   */
  public static List<String> paginateInput(List<String> inputLines, int currentSelectedPage) {
    List<String> output = new ArrayList<String>();
    // pagination
    int numPages = PaginationUtils.getNumPages(inputLines);
    currentSelectedPage = PaginationUtils.getCurrentPage(currentSelectedPage, numPages);

    int numStartElementOnCurrentPage = ((currentSelectedPage - 1) * PaginationUtils.NUM_ELEMENTS_PER_PAGE);
    int numMaxElemetsOnCurrentPage = (((currentSelectedPage) * PaginationUtils.NUM_ELEMENTS_PER_PAGE) < inputLines.size() ? ((currentSelectedPage) * PaginationUtils.NUM_ELEMENTS_PER_PAGE)
        : inputLines.size());

    // content
    for (int i = numStartElementOnCurrentPage; i < numMaxElemetsOnCurrentPage; i++) {
      output.add(inputLines.get(i));
    }
    return output;
  }
}
