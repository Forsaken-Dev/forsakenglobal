/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;

public class XenforoUtils {
  public static enum RegistrationType {
    SUCCESS, INVALID_OTHER, ALREADY_EXISTS, EMAIL_IN_USE;
  }

  public static enum GroupType {
    PRIMARY, SECONDARY
  }

  public static void sync(String username) {
    ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.INFO, "Syncing server ranks to forums for " + username);
    ForsakenGlobal
        .getInstance()
        .getAPI()
        .getDatabase()
        .write(
            "UPDATE `forsaken_xenforo`.`xf_user` xfu SET user_group_id = (SELECT xf.user_group_id FROM `forsaken_xenforo`.`xf_user_group` xf, `forsaken_mc`.`permissions_inheritance` pi, `forsaken_mc`.`permissions` p WHERE pi.child = xfu.username AND xf.user_title = pi.parent AND pi.parent = p.name AND p.value = 'primary'), secondary_group_ids = COALESCE((SELECT xf.user_group_id FROM `forsaken_xenforo`.`xf_user_group` xf, `forsaken_mc`.`permissions_inheritance` pi, `forsaken_mc`.`permissions` p WHERE pi.child = xfu.username AND xf.user_title = pi.parent AND pi.parent = p.name AND p.value = 'secondary'), '') WHERE xfu.username = ?",
            username);
  }

  /**
   * Edit a users forum group
   * 
   * @param user
   * @param groupId
   * @return
   */
  public static RegistrationType changeUserGroup(String user, int groupId, GroupType type) {
    try {
      String link = getWebsiteURL() + "api.php?action=edituser&hash="
          + ForsakenGlobal.getInstance().getAPI().getConfigManager().getString("website.api.hash") + "&user=" + user + "&"
          + (type == GroupType.PRIMARY ? "group=" + groupId : "secondary_group=" + groupId);
      URL url = new URL(link);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");

      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

      String inputLine;
      while ((inputLine = in.readLine()) != null) {
        return RegistrationType.INVALID_OTHER;
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
      return RegistrationType.INVALID_OTHER;
    } catch (IOException e) {
      e.printStackTrace();
      return RegistrationType.INVALID_OTHER;
    }
    return RegistrationType.SUCCESS;
  }

  /**
   * Registers a user with Xenforo with a username and email
   * 
   * @param user
   * @param email
   * @return
   */
  public static RegistrationType registerUser(String user, String email) {
    try {
      String link = getWebsiteURL() + "api.php?action=register&hash="
          + ForsakenGlobal.getInstance().getAPI().getConfigManager().getString("website.api.hash") + "&username=" + user + "&password="
          + ((new Random()).nextInt(Integer.MAX_VALUE) + 999999) + "&email=" + email + "&user_state=email_confirm";
      URL url = new URL(link);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");

      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

      String inputLine;
      while ((inputLine = in.readLine()) != null) {
        if (inputLine.contains("\"user_error_id\":40")) return RegistrationType.ALREADY_EXISTS;
        if (inputLine.contains("\"user_error_id\":42")) return RegistrationType.EMAIL_IN_USE;
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
      return RegistrationType.INVALID_OTHER;
    } catch (IOException e) {
      e.printStackTrace();
      return RegistrationType.INVALID_OTHER;
    }
    return RegistrationType.SUCCESS;
  }

  /**
   * Registers a user with Xenforo with a username and email
   * 
   * @param user
   * @param email
   * @return
   */
  public static void voteMessage() {
    try {
      String link = getWebsiteURL() + "dispatchVoteMessage.php?secret=redfatdog";
      URL url = new URL(link);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String getWebsiteURL() {
    String websiteBase = "http://forum.forsaken.co/";
    if (websiteBase.charAt(websiteBase.length() - 1) != '/') {
      websiteBase += '/';
    }
    return websiteBase;
  }
}
