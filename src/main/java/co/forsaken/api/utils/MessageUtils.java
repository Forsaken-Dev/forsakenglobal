/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.ocpsoft.pretty.time.PrettyTime;

/**
 * Utilities for formatting messages
 * 
 * @author ForsakenNetwork-Dev
 */
public class MessageUtils {
  private static final char       deg           = '\u00A7';
  private static String[][]       colourChars   = { { "0", ChatColor.BLACK.toString() }, { "1", ChatColor.DARK_BLUE.toString() },
                                                { "2", ChatColor.DARK_GREEN.toString() }, { "3", ChatColor.DARK_AQUA.toString() }, { "4", ChatColor.DARK_RED.toString() },
                                                { "5", ChatColor.DARK_PURPLE.toString() }, { "6", ChatColor.GOLD.toString() }, { "7", ChatColor.GRAY.toString() },
                                                { "8", ChatColor.DARK_GRAY.toString() }, { "9", ChatColor.BLUE.toString() }, { "a", ChatColor.GREEN.toString() },
                                                { "b", ChatColor.AQUA.toString() }, { "c", ChatColor.RED.toString() }, { "d", ChatColor.LIGHT_PURPLE.toString() },
                                                { "e", ChatColor.YELLOW.toString() }, { "f", ChatColor.WHITE.toString() } };

  private static String[][]       modifierChars = { { "l", ChatColor.BOLD.toString() }, { "o", ChatColor.ITALIC.toString() },
                                                { "k", ChatColor.MAGIC.toString() }, { "m", ChatColor.STRIKETHROUGH.toString() }, { "n", ChatColor.UNDERLINE.toString() },
                                                { "r", ChatColor.RESET.toString() } };

  private static final PrettyTime PRETTY_TIME   = new PrettyTime();

  /**
   * Strip any colour characters at the end of a message
   * 
   * @param msg
   * @return
   */
  public static String cleanMsgEnding(String msg) {
    while (msg.length() > 0) {
      if (msg.endsWith(String.valueOf(MessageUtils.deg)) || msg.endsWith(" ")) {
        msg = msg.substring(0, msg.length() - 1);
      } else if ((msg.length() >= 2) && (msg.charAt(msg.length() - 2) == MessageUtils.deg)) {
        msg = msg.substring(0, msg.length() - 2);
      } else {
        break;
      }
    }
    return msg;
  }

  public static String stripFormatting(String msg) {
    for (String[] s : MessageUtils.modifierChars) {
      if (msg.contains("&" + s[0])) {
        msg = msg.replaceAll("&" + s[0], "");
      }
      if (msg.contains("&" + s[0].toUpperCase())) {
        msg = msg.replaceAll("&" + s[0].toUpperCase(), "");
      }
    }
    return msg;
  }

  /**
   * Strip colours from the end of a message and then trim its contents
   * 
   * @param msg
   * @return
   */
  private static String fix(String msg) {
    msg = MessageUtils.cleanMsgEnding(msg);
    return msg.trim();
  }

  /**
   * Format a String into Bukkit colours
   * 
   * @param msg
   * @param useColour
   * @return
   */
  public static String format(String msg, boolean useColour) {
    String message = "";
    if (useColour) {
      int applicableModifier = 5;
      textLoop: for (int i = 0; i < msg.length(); i++) {
        char currentLetter = msg.charAt(i);
        if (currentLetter == '&') {
          if ((i + 1) < msg.length()) {
            for (String[] s : MessageUtils.colourChars) {
              if (msg.toLowerCase().charAt(i + 1) == s[0].charAt(0)) {
                message += s[1];
                if (applicableModifier < 5) {
                  message += MessageUtils.modifierChars[applicableModifier][1];
                }
                i++;
                continue textLoop;
              }
            }
            for (int ii = 0; ii < MessageUtils.modifierChars.length; ii++) {
              String[] s = MessageUtils.modifierChars[ii];
              if (msg.toLowerCase().charAt(i + 1) == s[0].charAt(0)) {
                applicableModifier = ii;
                message += s[1];
                i++;
                continue textLoop;
              }
            }
          }
        }
        message += currentLetter;
      }
    } else {
      message = msg;
      for (String[] s : MessageUtils.colourChars) {
        if (msg.contains("&" + s[0])) {
          message = message.replaceAll("&" + s[0], "");
        }
        if (msg.contains("&" + s[0].toUpperCase())) {
          message = message.replaceAll("&" + s[0].toUpperCase(), "");
        }
      }
      for (String[] s : MessageUtils.modifierChars) {
        if (msg.contains("&" + s[0])) {
          message = message.replaceAll("&" + s[0], "");
        }
        if (msg.contains("&" + s[0].toUpperCase())) {
          message = message.replaceAll("&" + s[0].toUpperCase(), "");
        }
      }
    }
    return message;
  }

  public static PrettyTime getTimeFormater() {
    return MessageUtils.PRETTY_TIME;
  }

  /**
   * Send a formatted message, and spilt lines if its asked
   * 
   * @param sender
   * @param msg
   */
  public static void send(CommandSender sender, String msg) {
    if (sender == null) return;
    msg = MessageUtils.format(msg, true);
    for (String s : msg.split("&/")) {
      sender.sendMessage(MessageUtils.fix(s));
    }
  }
}
