/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenAPI.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenAPI is licensed under the Forsaken Network License Version 1
 *
 * ForsakenAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenAPI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api.utils;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.inventory.ItemStack;

import com.comphenix.protocol.utility.StreamSerializer;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.io.NbtBinarySerializer;
import com.google.common.io.Closeables;

public class SerializationUtils {
  private static StreamSerializer serializer = new StreamSerializer();

  public static String serialize(ItemStack item) throws IOException {
    return serializer.serializeItemStack(item);
  }

  public static ItemStack deserialize(String s) throws IOException {
    return serializer.deserializeItemStack(s);
  }

  public static String serialize(NbtCompound nbtData) throws IOException {
    DataOutputStream stream = new DataOutputStream(new OutputStream() {
      private StringBuilder string = new StringBuilder();

      @Override
      public void write(int b) throws IOException {
        this.string.append((char) b);
      }

      public String toString() {
        return this.string.toString();
      }
    });
    serializer.serializeCompound(stream, nbtData);
    return stream.toString();
  }

  public static NbtCompound deserializeNbt(String s) throws IOException {
    return serializer.deserializeCompound(new DataInputStream(new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8))));
  }

  public static NbtCompound loadPlayerNbtFromFile(String username) throws IOException {
    File file = new File(WorldUtils.getPlayerFilePath(username));

    FileInputStream stream = null;
    DataInputStream input = null;
    boolean swallow = true;

    try {
      stream = new FileInputStream(file);
      NbtCompound result = NbtBinarySerializer.DEFAULT.deserializeCompound(input = new DataInputStream(new GZIPInputStream(stream)));
      swallow = false;
      return result;
    } finally {
      if (input != null) Closeables.close(input, swallow);
      else if (stream != null) Closeables.close(stream, swallow);
    }
  }

  public static void savePlayerNbtToFile(String username, NbtCompound compound) throws IOException {
    File file = new File(WorldUtils.getPlayerFilePath(username));

    FileOutputStream stream = null;
    DataOutputStream output = null;
    boolean swallow = true;

    try {
      stream = new FileOutputStream(file);
      NbtBinarySerializer.DEFAULT.serialize(compound, output = new DataOutputStream(new GZIPOutputStream(stream)));
      swallow = false;
    } finally {
      if (output != null) Closeables.close(output, swallow);
      else if (stream != null) Closeables.close(stream, swallow);
    }
  }
}