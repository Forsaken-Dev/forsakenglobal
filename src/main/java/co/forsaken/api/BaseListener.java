/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import org.bukkit.event.Listener;

import co.forsaken.ForsakenGlobal;

/**
 * Base Event listener which registers itself upon creation
 * 
 * @author ForsakenNetwork-Dev
 */
public abstract class BaseListener implements Listener {

  protected ForsakenGlobal _plugin;

  /**
   * Registers the listener with Bukkit
   * 
   * @param plugin
   */
  public BaseListener(ForsakenGlobal plugin) {
    _plugin = plugin;
    _plugin.getServer().getPluginManager().registerEvents(this, _plugin);
  }
}
