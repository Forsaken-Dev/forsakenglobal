/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.api;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.irc.IRCMessageReceiver;
import co.forsaken.api.manager.APIManager;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.MessageUtils;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;

public class APIRegistrar {

  @Command(aliases = { "ignore" }, desc = "Ignores a player", usage = "", anyFlags = true, min = 1, max = 1)
  @CommandPermissions(value = { "api.ignore" })
  public static void ignore(CommandContext args, User user) throws CommandException {
    try {
      User u = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      if (!user.ignores(u.getId())) {
        if (u.getPrimaryRank().isStaff()) {
          user.sendMessage("&cYou cannot ignore a staff member");
          return;
        }
        user.ignore(u.getId());
        user.sendMessage("&eYou are now ignoring &6" + u.getName());
      } else {
        user.unignore(u.getId());
        user.sendMessage("&eYou are now unignoring &6" + u.getName());
      }
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "protections", "prot", "protect", "towny", "mytown", "towns", "factions", "f" }, desc = "Shows our protection plugin", usage = "", anyFlags = true, min = 0, max = -1)
  @CommandPermissions(value = { "api.ignore" })
  public static void protections(CommandContext args, User user) throws CommandException {
    if (ForsakenGlobal.getInstance().getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
      user.sendMessage("&eWe use &6GriefPrevention &eto protect your items here!");
      user.sendMessage("&8- &eYou can gain claimblocks by voting/donating AND");
      user.sendMessage("&8-  &e50 claimblocks for every hour played up to 1000 total");
      user.sendMessage("&o&eIf you need some help with how to user GP, check &6http://goo.gl/NNsJJU");
    }
  }

  @Command(aliases = { "echeck" }, desc = "Checks entities in a world", usage = "", anyFlags = true, min = 0, max = -1)
  @CommandPermissions(value = { "api.echeck" })
  public static void echeck(CommandContext args, User user) throws CommandException {
    Map<String, Integer> e = new HashMap<String, Integer>();
    int totalAmount = 0;
    for (World w : ForsakenGlobal.getInstance().getServer().getWorlds()) {
      for (Entity ent : w.getEntities()) {
        if (!e.containsKey(ent.getClass().getName().toLowerCase())) {
          e.put(ent.getClass().getName().toLowerCase(), 1);
        } else {
          e.put(ent.getClass().getName().toLowerCase(), e.get(ent.getClass().getName().toLowerCase()) + 1);
        }
        totalAmount += 1;
      }
    }
    user.sendMessage("&eLoaded Entities &8[&7" + totalAmount + "&8]&e: ");
    for (String s : e.keySet()) {
      user.sendMessage("&8 - &6" + s + " &8[&7" + e.get(s) + "&8]");
    }
  }

  @Command(aliases = { "pcheck" }, desc = "Checks players in a world", usage = "", anyFlags = true, min = 0, max = -1)
  @CommandPermissions(value = { "api.pcheck" })
  public static void icheck(CommandContext args, User user) throws CommandException {
    Map<String, String> e = new HashMap<String, String>();
    int totalAmount = 0;
    for (Player p : ForsakenGlobal.getInstance().getServer().getOnlinePlayers()) {
      e.put(p.getName(), p.getWorld().getName() + ":" + p.getLocation().getBlockX() + "," + p.getLocation().getBlockY() + ","
          + p.getLocation().getBlockZ());
      totalAmount += 1;
    }
    user.sendMessage("&eLoaded Players &8[&7" + totalAmount + "&8]&e: ");
    for (String s : e.keySet()) {
      user.sendMessage("&8 - &6" + s + " &8[&7" + e.get(s) + "&8]");
    }
  }

  @Command(aliases = { "cape" }, desc = "Sets your cape", usage = "[url]", anyFlags = true, min = 1, max = -1)
  @CommandPermissions(value = { "api.cape" })
  public static void cape(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() > 3 && user.getPrimaryRankId() <= 8) {
      user.sendMessage("Sorry you cannot change your cape, you need to represent staff");
      return;
    }
    if (user.getSecondaryRankId() < 11) {
      user.sendMessage("Sorry only Epic+ donors can set custom capes");
      return;
    }
    String capeURL = args.getJoinedStrings(0);
    user.setCapeURL(capeURL);
    user.sendMessage("&aYou cape has been updated, Check it out :)");
  }

  @Command(aliases = { "skin" }, desc = "Sets your skin", usage = "[url]", anyFlags = true, min = 1, max = -1)
  @CommandPermissions(value = { "api.skin" })
  public static void skin(CommandContext args, User user) throws CommandException {
    if (user.getSecondaryRankId() < 12 || user.getPrimaryRankId() < 7) {
      user.sendMessage("Sorry only Legendary donors can set custom skins");
      return;
    }
    String skinURL = args.getJoinedStrings(0);
    user.setSkinURL(skinURL);
    user.sendMessage("&aYou skin has been updated, Check it out :)");
  }

  @Command(aliases = { "sudo" }, desc = "Run a command as a player", usage = "[player] [command]", flags = "", min = 2, max = -1)
  @Console
  @CommandPermissions("api.sudo")
  public static void sudo(CommandContext args, User user) throws CommandException {
    String name = args.getString(0);
    String command = args.getJoinedStrings(1);
    Player player = ForsakenGlobal.getInstance().getServer().getPlayer(name);
    if (player == null || !player.isOnline()) { throw new CommandException(name + " is not online"); }
    ForsakenGlobal.getInstance().getServer().dispatchCommand(player, command);
    user.sendMessage("Forced " + player.getName() + " to run \"" + command + "\"");
  }

  @Command(aliases = { "user" }, desc = "Check a user profile or check your own", usage = "<name>", min = 0, max = 1)
  @CommandPermissions("api.user")
  public static void user(CommandContext args, User user) throws CommandException {
    User check = user;
    if (args.argsLength() > 0) {
      try {
        check = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      } catch (Exception e) {
        user.sendMessage(e.getMessage());
      }
    }
    for (String s : check.getStatus()) {
      user.sendMessage(s);
    }
  }

  @Command(aliases = { "rawmsg" }, desc = "RawMessage commands", usage = "[msg...]", anyFlags = true, min = 1, max = -1)
  @Console
  @CommandPermissions("api.rawmsg")
  public static void rawMessage(CommandContext args, User user) throws CommandException {
    for (Player p : ForsakenGlobal.getInstance().getServer().getOnlinePlayers()) {
      MessageUtils.send(p, args.getJoinedStrings(0).replaceAll("<3", "❤"));
    }
  }

  @Command(aliases = { "reloadAnnouncements" }, desc = "Reloads the announcements", usage = "", min = -1, max = -1)
  @Console
  @CommandPermissions("api.reloadAnnouncements")
  public static void reloadAnnouncements(CommandContext args, User user) throws CommandException {
    ForsakenGlobal.getInstance().getAnnouncementTask().reload();
    if (user != null) {
      user.sendMessage("Announcements Reloaded");
    } else {
      ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.INFO, "Announcements Reloaded");
    }
  }

  @Command(aliases = { "title" }, desc = "Sets a donors title", usage = "[word]", anyFlags = true, min = -1, max = -1)
  @CommandPermissions("forsaken.donor.title")
  public static void title(CommandContext args, User user) throws CommandException {
    if (args.argsLength() > 0) {
      String title = args.getJoinedStrings(0);
      if (title.length() > 9) {
        title = title.substring(0, 9);
      }
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "pex user " + user.getName() + " suffix " + '"' + title + '"');
      user.sendMessage("You have set your title to " + title);
    } else {
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "pex user " + user.getName() + " suffix " + '"' + '"');
      user.sendMessage("You have removed your custom title");
    }

  }

  @Command(aliases = { "debug" }, desc = "Toggles showing of debug information", flags = "", min = 0, max = -1)
  @Console
  @CommandPermissions("api.debug")
  public static void debug(CommandContext args, User user) throws CommandException {
    int userId = user.getId();
    user.setReceiveLogMessages();
    try {
      user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(userId);
    } catch (Exception e) {
      e.printStackTrace();
    }
    user.sendMessage("&aDebug mode is now " + (user.shouldReceiveLogMessages() ? "&aon" : "&coff"));
  }

  @Command(aliases = { "salert" }, desc = "Sends a message to the server", flags = "", min = 0, max = -1)
  @Console
  @CommandPermissions("api.salert")
  public static void salert(CommandContext args, User user) throws CommandException {
    ForsakenGlobal.getInstance().getAPI().getMessageManager()
        .sendGlobalMessage("&8[&l&" + user.getPrimaryRank().getMCColour() + (user.getPrefix().isEmpty() ? user.getPrimaryRank().getName() : user.getPrefix()) + " Alert&r&8] &" + user.getPrimaryRank().getMCColour() + args.getJoinedStrings(0), false, true);
  }

  @IRCMessageReceiver()
  public static void ircMessage(APIManager _manager, String channel, String sender, String receiver, String message) {
    if (receiver.equalsIgnoreCase(ForsakenGlobal.getBaseIRCBot().getNick())) {
      if (channel.equalsIgnoreCase("global-connections")) {
        try {
          if (sender.contains("-server")) {
            if (message.equalsIgnoreCase("loadUsers")) {
              for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
                if (u.shouldReceiveLogMessages() && !(ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("lobby"))) {
                  u.sendMessage("&8[&l&c!&8] &l&6" + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(sender.replace("-server", "")).getFriendlyName() + " &r&ehas started back up!");
                  if (u.getActiveServer() != null) {
                    u.sendMessage("&8-   &7Toggle these messages with /debug");
                  }
                }
                ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", u.getName() + "|on=" + u.getActiveServer());
              }
            } else if (message.contains("|reload")) {
              User sUser = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(message.substring(0, message.indexOf("|reload")));
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().reload(sUser.getId());
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(sUser.getId()).setActiveServer(sender.replaceAll("-server", ""), false);
            } else if (message.indexOf("|on=") > 0) {
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(message.substring(0, message.indexOf("|on="))).setActiveServer(message.substring(message.indexOf("on=") + 3), false);
            } else if (message.indexOf("|join=") > 0) {
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(message.substring(0, message.indexOf("|join="))).setActiveServer(message.substring(message.indexOf("join=") + 5), true);
            } else if (message.indexOf("|quit=") > 0) {
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(message.substring(0, message.indexOf("|quit="))).setActiveServer(null, true);
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
}
