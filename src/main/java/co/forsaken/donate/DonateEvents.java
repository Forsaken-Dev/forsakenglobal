/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.donate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.ItemUtils;

public class DonateEvents extends BaseListener {
  private static Map<String, String> kitSelected = new HashMap<String, String>();

  public DonateEvents(ForsakenGlobal plugin) {
    super(plugin);
  }

  @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
  public void onSignChange(SignChangeEvent e) {
    String[] lines = e.getLines();
    if (lines[0].equalsIgnoreCase("ForsakenShop")) {
      if (!lines[3].equalsIgnoreCase(e.getPlayer().getName()) || lines[3].isEmpty()) {
        int rankId = 1;
        try {
          rankId = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName()).getPrimaryRankId();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
        if (rankId >= 7 && rankId <= 8) {
          e.setLine(3, "ForsakenBot");
        } else {
          e.setLine(3, e.getPlayer().getName());
        }
      }
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getClickedBlock() != null && e.getClickedBlock().getType() == Material.WALL_SIGN && e.getAction() == Action.RIGHT_CLICK_BLOCK) {
      Sign sign = (Sign) e.getClickedBlock().getState();
      if (sign.getLine(0).equalsIgnoreCase("ForsakenShop")) {
        User user = null;
        try {
          user = _plugin.getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName());
        } catch (Exception ex) {
        }
        if (user == null) { return; }
        if (sign.getLine(3).equalsIgnoreCase("ForsakenBot")) {

          if (sign.getLine(1).contains(" Claimblocks")) {
            if (kitSelected.containsKey(user.getName().toLowerCase())
                && kitSelected.get(user.getName().toLowerCase()).equalsIgnoreCase(sign.getLine(1))) {
              if (user.getCreditAmount() < Integer.parseInt(sign.getLine(2).replace(" cR", ""))) {
                user.sendMessage("You do not have " + sign.getLine(2).replace(" cR", "") + " credits to purchase the " + sign.getLine(1) + " kit");
                return;
              }
              user.forceRemoveCredits(Integer.parseInt(sign.getLine(2).replace(" cR", "")), sign.getLine(1) + "_kit_purchase");
              _plugin
                  .getServer()
                  .dispatchCommand(_plugin.getServer().getConsoleSender(),
                      "acb " + user.getName() + " " + sign.getLine(1).replace(" Claimblocks", ""));
              user.sendMessage("&eYou have purchased &6" + sign.getLine(1).replace(" Claimblocks", "") + " claimblocks &efor &a" + sign.getLine(2));
              kitSelected.remove(user.getName().toLowerCase());
            } else {
              kitSelected.put(user.getName().toLowerCase(), sign.getLine(1));
              user.sendMessage("&6The " + sign.getLine(1) + " kit contains: ");
              user.sendMessage("&8-  &e" + sign.getLine(1).replace(" Claimblocks", "") + " claimblocks");
              user.sendMessage("&6Click the sign again to buy the &a" + sign.getLine(1) + " &6kit for &a" + sign.getLine(2));
            }
          } else if (sign.getLine(1).contains(" VoteTokens")) {
            if (kitSelected.containsKey(user.getName().toLowerCase())
                && kitSelected.get(user.getName().toLowerCase()).equalsIgnoreCase(sign.getLine(1))) {
              if (user.getCreditAmount() < Integer.parseInt(sign.getLine(2).replace(" cR", ""))) {
                user.sendMessage("You do not have " + sign.getLine(2).replace(" cR", "") + " credits to purchase the " + sign.getLine(1) + " kit");
                return;
              }
              user.forceRemoveCredits(Integer.parseInt(sign.getLine(2).replace(" cR", "")), sign.getLine(1) + "_kit_purchase");
              user.addVoteTokens(Integer.parseInt(sign.getLine(1).replace(" VoteTokens", "")));
              user.sendMessage("&eYou have purchased &6" + sign.getLine(1).replace(" VoteTokens", "") + " vote tokens &efor &a" + sign.getLine(2));
              kitSelected.remove(user.getName().toLowerCase());
            } else {
              kitSelected.put(user.getName().toLowerCase(), sign.getLine(1));
              user.sendMessage("&6The " + sign.getLine(1) + " kit contains: ");
              user.sendMessage("&8-  &e" + sign.getLine(1).replace(" Claimblocks", "") + " claimblocks");
              user.sendMessage("&6Click the sign again to buy the &a" + sign.getLine(1) + " &6kit for &a" + sign.getLine(2));
            }
          } else {
            Block block = sign.getLocation().getWorld().getBlockAt(sign.getLocation().subtract(0, 1, 0));
            if (block != null && block.getType() == Material.CHEST) {
              Chest chest = (Chest) block.getState();
              if (kitSelected.containsKey(user.getName().toLowerCase())
                  && kitSelected.get(user.getName().toLowerCase()).equalsIgnoreCase(sign.getLine(1))) {
                if (user.getCreditAmount() < Integer.parseInt(sign.getLine(2).replace(" cR", ""))) {
                  user.sendMessage("You do not have " + sign.getLine(2).replace(" cR", "") + " credits to purchase the " + sign.getLine(1) + " kit");
                  return;
                }
                user.forceRemoveCredits(Integer.parseInt(sign.getLine(2).replace(" cR", "")), sign.getLine(1) + "_kit_purchase");
                List<ItemStack> items = new ArrayList<ItemStack>();
                for (ItemStack i : chest.getInventory().getContents()) {
                  if (i != null) {
                    items.add(i.clone());
                  }
                }
                ItemUtils.giveItemToPlayer(user.getPlayer(), items);
                user.getPlayer().updateInventory();
                user.sendMessage("You purchased the " + sign.getLine(1) + " kit for " + sign.getLine(2));
                kitSelected.remove(user.getName().toLowerCase());
              } else {
                kitSelected.put(user.getName().toLowerCase(), sign.getLine(1));
                user.sendMessage("&6The " + sign.getLine(1) + " kit contains: ");
                for (ItemStack i : chest.getInventory().getContents()) {
                  if (i != null) {
                    user.sendMessage("&8-  &e"
                        + i.getAmount()
                        + " x "
                        + (i.getItemMeta().hasDisplayName() ? i.getItemMeta().getDisplayName().trim() : i.getType().name().replace("_", " ")
                            .toLowerCase()) + (!i.getEnchantments().isEmpty() ? " (" + ItemUtils.getItemEnchantments(i) + ")" : ""));
                  }
                }
                user.sendMessage("&6Click the sign again to buy the &a" + sign.getLine(1) + " &6kit for &a" + sign.getLine(2));
              }
            }
          }
        }
      }
    }
  }
}
