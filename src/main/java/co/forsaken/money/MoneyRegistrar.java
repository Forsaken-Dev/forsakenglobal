/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.money;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class MoneyRegistrar {
  @Command(aliases = { "money", "balance", "bal", "credits" }, desc = "Check your balance", min = 0, max = -1)
  @NestedCommand(value = { Child.class }, executeBody = true)
  public static void money(CommandContext args, User user) throws CommandException {
    user.sendMessage("&eYou currently have &6" + user.getCreditAmount() + " credits &ein your bank");
  }

  @Command(aliases = { "pay" }, desc = "Sends credits to a user", usage = "[name] [amount]", min = 2, max = 2)
  @CommandPermissions("money.send")
  public static void pay(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    int creditAmount = args.getInteger(1);
    if (creditAmount <= 0) { throw new CommandException("You cannot send " + creditAmount + " credits"); }
    if (user.getCreditAmount() < creditAmount) { throw new CommandException("You do not have " + creditAmount + " credit"
        + (creditAmount == 1 ? "" : "s") + " to send"); }
    try {
      User userToChange = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      user.sendCredits(creditAmount, userToChange, "user_transaction");
      user.sendMessage("You have sent " + userToChange.getName() + " " + creditAmount + " credits");
      userToChange.sendMessage("You have received " + creditAmount + " credits" + " from " + user.getName());
    } catch (Exception e) {
      user.sendMessage(e.getMessage());
    }
  }

  public static class Child {
    @Command(aliases = { "send", "pay" }, desc = "Sends credits to a user", usage = "[name] [amount]", min = 2, max = 2)
    @CommandPermissions("money.send")
    public static void send(CommandContext args, User user) throws CommandException {
      String username = args.getString(0);
      int creditAmount = args.getInteger(1);
      if (creditAmount <= 0) { throw new CommandException("You cannot send " + creditAmount + " credits"); }
      if (user.getCreditAmount() < creditAmount) { throw new CommandException("You do not have " + creditAmount + " credit"
          + (creditAmount == 1 ? "" : "s") + " to send"); }
      try {
        User userToChange = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
        user.sendCredits(creditAmount, userToChange, "user_transaction");
        user.sendMessage("You have sent " + userToChange.getName() + " " + creditAmount + " credits");
        userToChange.sendMessage("You have received " + creditAmount + " credits" + " from " + user.getName());
      } catch (Exception e) {
        user.sendMessage(e.getMessage());
      }
    }

    @Command(aliases = { "add" }, desc = "Force adds credits to a user", usage = "[name] [amount]", min = 2, max = 2)
    @Console
    @CommandPermissions("money.add")
    public static void add(CommandContext args, User user) throws CommandException {
      String username = args.getString(0);
      int creditAmount = args.getInteger(1);
      if (creditAmount <= 0) { throw new CommandException("You cannot send " + creditAmount + " credits"); }
      try {
        User userToChange = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
        userToChange.forceAddCredits(creditAmount, "admin_force_add_" + user.getId());
        user.sendMessage("You have given " + userToChange.getName() + " " + creditAmount + " credits");
        userToChange.sendMessage("You have received " + creditAmount + " credits" + " from " + user.getName());
      } catch (Exception e) {
        user.sendMessage(e.getMessage());
      }
    }

    @Command(aliases = { "remove", "rm" }, desc = "Force removes credits to a user", usage = "[name] [amount]", min = 2, max = 2)
    @Console
    @CommandPermissions("money.remove")
    public static void remove(CommandContext args, User user) throws CommandException {
      String username = args.getString(0);
      int creditAmount = args.getInteger(1);
      if (creditAmount <= 0) { throw new CommandException("You cannot remove " + creditAmount + " credits"); }
      try {
        User userToChange = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
        userToChange.forceRemoveCredits(creditAmount, "admin_force_remove_" + user.getId());
        user.sendMessage("You have removed " + creditAmount + " credits from " + userToChange.getName());
      } catch (Exception e) {
        user.sendMessage(e.getMessage());
      }
    }
  }
}
