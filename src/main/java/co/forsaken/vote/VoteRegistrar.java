/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote;

import java.io.IOException;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.irc.IRCMessageReceiver;
import co.forsaken.api.manager.APIManager;
import co.forsaken.api.utils.SerializationUtils;
import co.forsaken.vote.factory.ClaimsFactory;
import co.forsaken.vote.factory.VoteFactory;
import co.forsaken.vote.factory.VoteSiteFactory;
import co.forsaken.vote.factory.obj.VoteSite;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteRegistrar extends BaseListener {

  private static int NUM_TOKENS_PER_VOTE = 10;

  public VoteRegistrar(ForsakenGlobal plugin) {
    super(plugin);
    getVoteFactory();
    if (getVoteSiteFactory().getAll().size() > 1) {
      NUM_TOKENS_PER_VOTE = Math.round(NUM_TOKENS_PER_VOTE / getVoteSiteFactory().getAll().size());
    }
    if (NUM_TOKENS_PER_VOTE < 5) {
      NUM_TOKENS_PER_VOTE = 5;
    }
    getClaimsFactory();
  }

  private static VoteFactory     _voteFactory;
  private static VoteSiteFactory _siteFactory;
  private static ClaimsFactory   _claimFactory;

  public static ClaimsFactory getClaimsFactory() {
    if (_claimFactory == null) {
      _claimFactory = new ClaimsFactory(ForsakenGlobal.getInstance());
    }
    return _claimFactory;
  }

  public static VoteFactory getVoteFactory() {
    if (_voteFactory == null) {
      _voteFactory = new VoteFactory(ForsakenGlobal.getInstance());
    }
    return _voteFactory;
  }

  public static VoteSiteFactory getVoteSiteFactory() {
    if (_siteFactory == null) {
      _siteFactory = new VoteSiteFactory(ForsakenGlobal.getInstance());
    }
    return _siteFactory;
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onVotifierEvent(VotifierEvent event) {
    Vote vote = event.getVote();

    if (vote != null) {
      String username = vote.getUsername();
      String service = vote.getServiceName();
      if (service.lastIndexOf(".") != -1) {
        service = service.substring(0, service.lastIndexOf("."));
      }

      if (username.equalsIgnoreCase("Test")) {
        System.out.println(username + " has voted on " + service.trim());
        return;
      }
      VoteSite site = null;
      try {
        site = getVoteSiteFactory().get(service.trim());
      } catch (Exception e) {
        try {
          site = new VoteSite(_plugin, _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId(), service, vote.getAddress());
        } catch (Exception e1) {
        }
      }

      vote(username, true);
      ForsakenGlobal.getBaseIRCBot().sendChannelMessage("#global-connections", username + "|vote");

      User user = null;
      try {
        user = _plugin.getAPI().getFactoryManager().getUserFactory().get(username.trim());
      } catch (Exception e) {
        e.printStackTrace();
      }

      if (site != null && user != null) {
        new co.forsaken.vote.factory.obj.Vote(_plugin, user.getId(), site.getId());
      } else if (site == null && user != null) {
        new co.forsaken.vote.factory.obj.Vote(_plugin, user.getId(), -1);
      }
    }
  }

  public static void vote(String username, boolean credits) {
    User user = null;
    int numVoteCredits = NUM_TOKENS_PER_VOTE;
    double percent = 1D;
    String reward = numVoteCredits + " Voting Tokens";
    try {
      user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username.trim());
      if (user.getPrimaryRankId() == 1) {
        percent = 0.5D;
        user.sendMessage("&cYou only start with &650% &cof the actual voting rewards, since you are not registered... &eRegister with &6/register");
      }
      if (user.getSecondaryRankId() >= 2) {
        double p = ((double) (((user.getSecondaryRankId() - 8) * 25))) / 100D;
        user.sendMessage("&eYou receive an extra &6" + Math.round(p * 100) + "%&e, since you are a &a" + user.getSecondaryRank().getName());
        percent += p;
      }
      if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 25).containsKey(user.getName().toLowerCase())) {
        double p = 0.75D;
        user.sendMessage("&eYou receive an extra &6" + Math.round(p * 100) + "%&e, since voted more than " + (getVoteSiteFactory().getAll().size() * 25) + " times last month!");
        percent += p;
      } else if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 20).containsKey(user.getName().toLowerCase())) {
        double p = 0.5D;
        user.sendMessage("&eYou receive an extra &6" + Math.round(p * 100) + "%&e, since voted more than " + (getVoteSiteFactory().getAll().size() * 20) + " times last month!");
        percent += p;
      } else if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 15).containsKey(user.getName().toLowerCase())) {
        double p = 0.25D;
        user.sendMessage("&eYou receive an extra &6" + Math.round(p * 100) + "%&e, since voted more than " + (getVoteSiteFactory().getAll().size() * 15) + " times last month!");
        percent += p;
      }
      numVoteCredits = (int) ((double) numVoteCredits * percent);
      if (credits) {
        user.addVoteTokens(numVoteCredits);
      }
      reward = numVoteCredits + " Vote Tokens" + (percent > 1D ? " (" + (Math.round(percent * 100) - 100) + "% more)" : "");
      ForsakenGlobal.getInstance().getServer()
          .dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "vote notify " + username + " " + reward);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @IRCMessageReceiver()
  public static void ircMessage(APIManager _manager, String channel, String sender, String receiver, String message) {
    if (receiver.equalsIgnoreCase(ForsakenGlobal.getBaseIRCBot().getNick())) {
      if (channel.equalsIgnoreCase("global-connections")) {
        try {
          if (sender.contains("-server")) {
            if (message.contains("|vote")) {
              vote(message.substring(0, message.indexOf("|vote")), false);
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Command(aliases = { "vote" }, desc = "All voting commands", min = 0, max = -1)
  @NestedCommand(value = { Child.class }, executeBody = true)
  public static void voteBase(CommandContext args, User user) throws CommandException {
    int numVoteCredits = 10;
    double percent = 1D;
    String why = "";
    if (user.getPrimaryRankId() == 1) {
      percent = 0.5D;
      why += "&8-  &7-50% [You are not registered yet]";
    }
    if (user.getSecondaryRankId() >= 2) {
      double p = ((double) (((user.getSecondaryRankId() - 8) * 25))) / 100D;
      percent += p;
      if (!why.isEmpty()) {
        why += "&/";
      }
      why += "&8-  &7+" + (Math.round(p * 100D)) + "% [You are a " + user.getSecondaryRank().getName() + "]";
    }
    if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 25).containsKey(user.getName().toLowerCase())) {
      double p = 0.75D;
      percent += p;
      if (!why.isEmpty()) {
        why += "&/";
      }
      why += "&8-  &7+" + (Math.round(p * 100D)) + "% [You had " + (getVoteSiteFactory().getAll().size() * 25) + "+ votes last month]";

    } else if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 20).containsKey(user.getName().toLowerCase())) {
      double p = 0.5D;
      percent += p;
      if (!why.isEmpty()) {
        why += "&8/";
      }
      why += "&8-  &7+" + (Math.round(p * 100D)) + "% [You had " + (getVoteSiteFactory().getAll().size() * 20) + "+ votes last month]";

    } else if (VoteUtils.getVotersOver(getVoteSiteFactory().getAll().size() * 15).containsKey(user.getName().toLowerCase())) {
      double p = 0.25D;
      percent += p;
      if (!why.isEmpty()) {
        why += "&/";
      }
      why += "&8-  &7+" + (Math.round(p * 100D)) + "% [You had " + (getVoteSiteFactory().getAll().size() * 15) + "+ votes last month]";

    }
    numVoteCredits = ((int) ((double) numVoteCredits * percent));

    String msg = "&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-[  &6Votes  &7]-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-";
    msg += "&/&7Spend vote tokens on rewards with &6/claim";
    msg += "&/&7Vote everyday for &6"
        + numVoteCredits
        + " Vote Tokens &7per vote";
    if (!why.isEmpty()) {
      msg += "&/&7" + why;
    }
    msg += "&/&/&7Vote more than " + (getVoteSiteFactory().getAll().size() * 15) + " times this month and receive bonus tokens!";
    msg += "&/&8-   &7" + (getVoteSiteFactory().getAll().size() * 25) + "+ Votes = +75% next month&/&8-   &7" + (getVoteSiteFactory().getAll().size() * 20) + "+ Votes = +50% next month&/&8-   &7" + (getVoteSiteFactory().getAll().size() * 15)
        + "+ Votes = +25% next month";
    msg += "&/&/&aThe Top 2 voters will receive a $10 and $5 donation credit respectively at the end of the month!";
    msg += "&/&/&7You current have "
        + user.getTotalVotesThisMonth() + " votes this month!";
    user.sendMessage(msg);
    VoteUtils.sendVoteReminder(user, true, true);
  }

  public static class Child {

    @Console
    @Command(aliases = { "notify" }, desc = "Notifies that a user has voted", usage = "[player] [reward...]", min = 2, max = -1)
    @CommandPermissions(value = { "vote.notify" })
    public static void vote(final CommandContext args, User user) throws CommandException {
      User voter = null;
      String reward = args.getJoinedStrings(1);
      try {
        voter = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      } catch (Exception e) {
      }
      if (voter != null) {
        voter.sendMessage("&7You have been given " + reward + ", Thank you for voting &c❤&7!");
        ForsakenGlobal
            .getInstance()
            .getAPI()
            .getMessageManager()
            .sendGlobalMessage("&8[&a&l✔&r&8] &f" + voter.getName() + " &ehas voted with &a/vote &eand received &6" + reward + "&e!", false, true);
        if (voter.getPlayer() != null && voter.getPlayer().isOnline() && !ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("lobby") && !ForsakenGlobal.getInstance().getAPI().getItemMenuManager().hasMenu(voter.getName(), ClaimMenu.class)) {
          voter.sendMessage("&cType /claim to claim a reward");
        }
      } else {
        ForsakenGlobal
            .getInstance()
            .getAPI()
            .getMessageManager()
            .sendGlobalMessage(
                "&8[&a✓&8] &7Someone voted and forgot their name :(", false, true);
      }
    }
  }

  @Console
  @Command(aliases = { "admin-add-vote-tokens", "aavt" }, desc = "Gives a user vote tokens", usage = "[player] [amount]", min = 2, max = 2)
  @CommandPermissions(value = { "vote.add-tokens" })
  public static void vote(final CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    int amount = args.getInteger(1);
    if (amount <= 0) { throw new CommandException("Sorry, you need to give them more than 0 tokens"); }
    try {
      User userToGive = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      userToGive.addVoteTokens(amount);
      user.sendMessage("You gave " + username + " " + amount + " vote tokens");
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "admin-add-claim", "aac" }, desc = "Adds a new claim reward", usage = "[required_amount]", min = 1, max = 1)
  @CommandPermissions(value = { "vote.admin.addclaim" })
  public static void adminAddClaim(CommandContext args, User user) throws CommandException {
    ItemStack item = user.getPlayer().getItemInHand();
    if (item == null) {
      user.sendMessage("The item in your hand is currently non existant. Please hold an item while doing this");
      return;
    }
    try {
      ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .write("INSERT INTO forsaken_claim_items (name, server_id, required_token_amount, item) VALUES(?,?,?,?)", item.getType().name() + ":" + item.getDurability(),
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId()
              , args.getInteger(0), SerializationUtils.serialize(item));
      getClaimsFactory().reload();
    } catch (NumberFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Command(aliases = { "claim", "claims" }, desc = "Opens up your claim chest", usage = "", min = 0, max = -1)
  @CommandPermissions(value = { "vote.claim" })
  public static void claims(CommandContext args, User user) throws CommandException {
    if (!ForsakenGlobal.getInstance().getServer().getMotd().equals("lobby")) {
      ForsakenGlobal.getInstance().getAPI().getItemMenuManager().addMenu(new ClaimMenu(ForsakenGlobal.getInstance(), user.getName()));
    } else {
      user.sendMessage("This command will not work in the lobby");
    }
  }
}
