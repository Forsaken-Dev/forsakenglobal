/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Rank;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.XenforoUtils;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.vote.factory.obj.Vote;
import co.forsaken.vote.factory.obj.VoteSite;

public class VoteUtils {

  public static Map<Integer, List<VoteSite>> getVoteSitesForUsers(List<String> userNames) {
    Map<Integer, List<VoteSite>> voteSites = new HashMap<Integer, List<VoteSite>>();
    if (userNames.size() == 0) { return voteSites; }
    String sql = "";
    try {
      sql = "SELECT vs.id as id, DATE_ADD(uv.date, INTERVAL 1 DAY) as nextVote, uv.user_id as user_id FROM `"
          + VoteRegistrar.getVoteSiteFactory().getTableName() + "` vs, `" + VoteRegistrar.getVoteFactory().getTableName()
          + "` uv, `forsaken_users` zu WHERE uv.vote_site_id = vs.id AND (vs.server_id = "
          + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId() + " OR vs.server_id = 0) AND ( ";
    } catch (Exception e) {
      sql = "SELECT vs.id as id, DATE_ADD(uv.date, INTERVAL 1 DAY) as nextVote, uv.user_id as user_id FROM `"
          + VoteRegistrar.getVoteSiteFactory().getTableName() + "` vs, `" + VoteRegistrar.getVoteFactory().getTableName()
          + "` uv, `forsaken_users` zu WHERE uv.vote_site_id = vs.id AND vs.server_id = 0 AND ( ";
      e.printStackTrace();
    }
    int i = 0;
    for (String name : userNames) {
      if (i > 0) {
        sql += " OR ";
      }
      sql += "(uv.user_id = zu.id AND zu.name = '" + name + "')";
      i++;
    }
    sql += ") GROUP BY uv.user_id;";

    DatabaseResults query = ForsakenGlobal.getInstance().getAPI().getDatabase().readEnhanced(sql);
    VoteSite site = null;
    if (query != null && query.hasRows()) {
      for (i = 0; i < query.rowCount(); i++) {
        try {
          int userId = query.getInteger(i, "user_id");
          if (site == null || (site != null && site.getId() != query.getInteger(i, "id"))) {
            site = VoteRegistrar.getVoteSiteFactory().get(query.getInteger(i, "id"));
          }
          if (!voteSites.containsKey(userId)) {
            voteSites.put(userId, new ArrayList<VoteSite>());
          }
          Date nextVote = query.getDate(i, "nextVote");
          if (new Date().getTime() <= nextVote.getTime()) {
            voteSites.get(userId).add(site);
          }
        } catch (SQLDataException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return voteSites;
  }

  /**
   * Gets all voting sites that the user can vote on
   * 
   * @param userId
   * @return
   */
  public static List<VoteSite> getVoteSitesForUser(int userId) {
    List<VoteSite> voteSites = new ArrayList<VoteSite>();
    DatabaseResults query;
    try {
      query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT vs.id as id FROM `"
                  + VoteRegistrar.getVoteSiteFactory().getTableName()
                  + "` vs, `"
                  + VoteRegistrar.getVoteFactory().getTableName()
                  + "` uv WHERE uv.vote_site_id = vs.id AND uv.user_id = ? AND NOW() >= DATE_ADD(uv.date, INTERVAL 1 DAY) AND (vs.server_id = ? OR vs.server_id = 0);",
              userId, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e1) {
      e1.printStackTrace();
      query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT vs.id as id FROM `" + VoteRegistrar.getVoteSiteFactory().getTableName() + "` vs, `"
                  + VoteRegistrar.getVoteFactory().getTableName()
                  + "` uv WHERE uv.vote_site_id = vs.id AND uv.user_id = ? AND NOW() >= DATE_ADD(uv.date, INTERVAL 1 DAY) AND vs.server_id = ?;",
              userId, 0);
    }
    if (query != null && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          voteSites.add(VoteRegistrar.getVoteSiteFactory().get(query.getInteger(i, "id")));
        } catch (SQLDataException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return voteSites;
  }

  /**
   * Gets todays votes from a user
   * 
   * @param userId
   * @return
   */
  public static List<Vote> getVotesToday(int userId) {
    List<Vote> votes = new ArrayList<Vote>();
    DatabaseResults query;
    try {
      query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT uv.id as id, uv.user_id as user_id, uv.vote_site_id as vote_site_id, uv.date as date FROM `"
                  + VoteRegistrar.getVoteSiteFactory().getTableName()
                  + "` vs, `"
                  + VoteRegistrar.getVoteFactory().getTableName()
                  + "` uv WHERE uv.vote_site_id = vs.id AND uv.user_id = ? AND NOW() < DATE_ADD(uv.date, INTERVAL 1 DAY) AND (vs.server_id = ? OR vs.server_id = 0) ORDER BY uv.id DESC;",
              userId, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e1) {
      e1.printStackTrace();
      query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT uv.id as id, uv.user_id as user_id, uv.vote_site_id as vote_site_id, uv.date as date FROM `"
                  + VoteRegistrar.getVoteSiteFactory().getTableName()
                  + "` vs, `"
                  + VoteRegistrar.getVoteFactory().getTableName()
                  + "` uv WHERE uv.vote_site_id = vs.id AND uv.user_id = ? AND NOW() < DATE_ADD(uv.date, INTERVAL 1 DAY) AND vs.server_id = ? ORDER BY uv.id DESC;",
              userId, 0);
    }
    if (query != null && query.hasRows()) {
      for (int i = 0; i < query.rowCount(); i++) {
        try {
          Vote v = VoteRegistrar.getVoteFactory().parseFromQuery(query, i);
          votes.add(v);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return votes;
  }

  /**
   * Sends a voting reminder for a user
   * 
   * @param user
   * @param nextVote
   * @param voteSites
   */
  public static void sendVoteReminder(User user, boolean showFailed, boolean fromVotesCmd) {
    List<VoteSite> voting = new ArrayList<VoteSite>();
    List<VoteSite> voteSites = VoteRegistrar.getVoteSiteFactory().getAll();
    Vote lastVote = null;
    voteSite: for (int i = 0; i < voteSites.size(); i++) {
      VoteSite vs = voteSites.get(i);
      for (Vote v : getVotesToday(user.getId())) {
        if (lastVote == null || lastVote.getDate().getTime() < v.getDate().getTime()) {
          lastVote = v;
        }
        if (vs.getId() == v.getVoteSiteId()) {
          continue voteSite;
        }
      }
      voting.add(vs);
    }
    String msg = "";
    if (!fromVotesCmd) {
      msg = "&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-[  &6Vote  &7]-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-";
    }
    boolean failed = false;
    if (voting.size() > 0) {
      msg += "&/&7You haven't voted today on " + (voting.size() == 1 ? "this" : "these") + " site"
          + (voting.size() == 1 ? "" : "s");

      for (VoteSite vs : voting) {
        msg += "&/&8-  &a" + vs.getName() + ": &b" + vs.getURL();
      }
      failed = false;
    } else if (showFailed) {
      msg += "You have already voted on all our sites!";
      if (lastVote != null) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastVote.getDate());
        cal.add(Calendar.DATE, 1);
        Date nextTime = cal.getTime();
        msg += " You can vote again in " + MessageUtils.getTimeFormater().format(nextTime);
      }
      failed = true;
    } else {
      failed = true;
    }
    msg += "&/&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-";
    if (!(!showFailed && failed) || showFailed) {
      user.sendMessage(msg);
    }
  }

  private static Map<String, Integer> voters = new HashMap<String, Integer>();

  public static Map<String, Integer> getTopVoters(int amount) {
    if (voters.isEmpty()) {
      DatabaseResults query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT zu.name as name, COUNT( * ) AS count FROM `forsaken_vote_sites` vs, `forsaken_user_votes` uv, `forsaken_users` zu WHERE zu.id = uv.user_id AND (YEAR(`date`) = YEAR(DATE_SUB(NOW( ), INTERVAL 1 MONTH)) AND MONTH(`date`) = MONTH(DATE_SUB(NOW( ), INTERVAL 1 MONTH))) GROUP BY uv.user_id  HAVING count >= (SELECT DISTINCT COUNT(*) as count FROM forsaken_user_votes WHERE (YEAR(`date`) = YEAR(DATE_SUB(NOW( ), INTERVAL 1 MONTH)) AND MONTH(`date`) = MONTH(DATE_SUB(NOW( ), INTERVAL 1 MONTH))) group by user_id ORDER BY count DESC LIMIT ?, 1)  ORDER BY count DESC",
              amount - 1);
      if (query != null && query.hasRows()) {
        int pos = 0;
        int posValue = 0;
        for (int i = 0; i < query.rowCount(); i++) {
          if (posValue != Integer.valueOf(query.rawResults.get(i).get(1) + "")) {
            posValue = Integer.valueOf(query.rawResults.get(i).get(1) + "");
            pos++;
          }
          try {
            voters.put(query.getString(i, "name").toLowerCase(), pos);
          } catch (SQLDataException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return voters;
  }

  private static Map<String, Map<String, Integer>> lastMonthsVoters = new HashMap<String, Map<String, Integer>>();

  public static Map<String, Integer> getVotersOver(int amount) {
    if (!lastMonthsVoters.containsKey(amount + "")) {
      DatabaseResults query = ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .readEnhanced(
              "SELECT zu.name as name, COUNT( * ) AS count FROM `forsaken_vote_sites` vs, `forsaken_user_votes` uv, `forsaken_users` zu WHERE zu.id = uv.user_id AND (YEAR(`date`) = YEAR(DATE_SUB(NOW( ), INTERVAL 1 MONTH)) AND MONTH(`date`) = MONTH(DATE_SUB(NOW( ), INTERVAL 1 MONTH))) GROUP BY uv.user_id  HAVING count >= ? ORDER BY count DESC",
              amount);
      if (query != null && query.hasRows()) {
        int pos = 0;
        int posValue = 0;
        for (int i = 0; i < query.rowCount(); i++) {
          if (posValue != Integer.valueOf(query.rawResults.get(i).get(1) + "")) {
            posValue = Integer.valueOf(query.rawResults.get(i).get(1) + "");
            pos++;
          }
          try {
            if (!lastMonthsVoters.containsKey(amount + "")) {
              lastMonthsVoters.put(amount + "", new HashMap<String, Integer>());
            }
            lastMonthsVoters.get(amount + "").put(query.getString(i, "name").toLowerCase(), pos);
          } catch (SQLDataException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return lastMonthsVoters.get(amount + "");
  }

  public static void upgradeTop(int count) {
    Map<String, Integer> topVoters = getTopVoters(count);
    for (String username : topVoters.keySet()) {
      int pos = topVoters.get(username);
      int donationToAdd = (((count - pos) + 1) * 5);
      try {
        User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
        double donationAmount = user.getTotalDonationAmount();
        ForsakenGlobal.getInstance().getAPI().getDatabase().write("INSERT INTO `forsaken_user_donations` (email, user_id, amount, status, date) VALUES('payments@forsaken.co', ?, ?, 'credit', DATE_SUB(NOW( ), INTERVAL 1 MONTH))", user.getId(), donationToAdd);
        Rank rank = null;
        Rank userOldRank = user.getSecondaryRank();
        for (Rank r : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getRankFactory().getAll()) {
          if (r.getDonationRequired() > 0) {
            if (rank == null || r.getDonationRequired() <= donationAmount + donationToAdd) {
              rank = r;
            }
          }
        }
        if (userOldRank.getId() < rank.getId()) {
          ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "pex user " + user.getName() + " group remove " + userOldRank.getName().toLowerCase());
          ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "pex user " + user.getName() + " group add " + rank.getName().toLowerCase());
        }
        XenforoUtils.voteMessage();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static boolean hasAddedTopVoters() {
    DatabaseResults query = ForsakenGlobal
        .getInstance()
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT * FROM `forsaken_user_donations` WHERE status = 'credit' and (YEAR(`date`) = YEAR(DATE_SUB(NOW( ), INTERVAL 1 MONTH)) AND MONTH(`date`) = MONTH(DATE_SUB(NOW( ), INTERVAL 1 MONTH)))");
    return query != null && query.hasRows();
  }
}
