/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenShops.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenShops is licensed under the Forsaken Network License Version 1
 *
 * ForsakenShops is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenShops is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenShops.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenShops is licensed under the Forsaken Network License Version 1
 *
 * ForsakenShops is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenShops is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.obj.BaseItemMenu;
import co.forsaken.api.utils.ItemUtils;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.vote.factory.obj.Claim;

public class ClaimMenu extends BaseItemMenu {

  public ClaimMenu(ForsakenGlobal plugin, String playerName) {
    super(plugin, "Claim Chest", playerName);
    this.menuSize = ((int) Math.floor(getAllClaims().size() / 9D) + 1) * 9;
    if (menuSize > 36) {
      menuSize = 36;
    }
    menuName = MessageUtils.format("&l&cClaim Chest: &6" + getUser().getVoteTokens() + " Tokens", true);
  }

  public int getCost(ItemStack i) {
    for (Claim c : getAllClaims()) {
      if (itemEquals(c.getItem(), i)) { return c.getRequiredTokens(); }
    }
    return 0;
  }

  private List<Claim> getAllClaims() {
    ArrayList<Claim> claims = new ArrayList<Claim>();
    for (Claim c : VoteRegistrar.getClaimsFactory().getAll()) {
      claims.add(c);
    }
    return claims;
  }

  @Override
  protected void loadItems() {
    ArrayList<String> lore = new ArrayList<String>();
    if (ForsakenGlobal._GPEnabled) {
      lore.add("Claim Reward");
      lore.add("&l&aCost: &r&a10 tokens");
      if (10 > getUser().getVoteTokens()) {
        lore.add("&cYou cannot afford this item");
      }
      addItem(ItemUtils.getItemName(new ItemStack(Material.GOLD_SPADE), "125 Claimblocks", lore));
    }
    for (Claim c : getAllClaims()) {
      lore.clear();
      lore.add("Claim Reward");
      lore.add("&l&aCost: &r&a" + c.getRequiredTokens() + " tokens");
      if (c.getRequiredTokens() > getUser().getVoteTokens()) {
        lore.add("&cYou cannot afford this item");
      }
      ItemStack i = ItemUtils.getItemName(c.getItem().clone(), null, lore);
      addItem(i);
    }
  }

  @Override
  public boolean clickSlot(int slot, ItemStack itemInSlot, ItemStack itemInHand, boolean isShiftClick, boolean isRightClick, boolean isLeftClick) {
    if (itemInSlot == null || itemInSlot.getType() == Material.AIR) { return false; }
    if (itemInSlot.hasItemMeta() && itemInSlot.getItemMeta() != null && itemInSlot.getItemMeta().hasDisplayName() && itemInSlot.getItemMeta().getDisplayName().contains("Claimblocks")) {
      int cost = 10;
      if (getUser().getVoteTokens() >= cost) {
        getUser().addVoteTokens(0 - cost);
        ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "acb " + getUser().getName() + " 125");
        closeInv();
        getUser().sendMessage("You have been given 125 claim blocks");
      } else {
        getUser().sendMessage("&cYou cannot afford to buy this item");
      }
    } else {
      int cost = getCost(itemInSlot);
      if (cost > -1) {
        if (getUser().getVoteTokens() >= cost) {
          getUser().addVoteTokens(0 - cost);
          final ItemStack item = ItemUtils.getItemName(itemInSlot.clone(), null, null);
          closeInv();
          _plugin.getServer().getScheduler().runTaskLater(_plugin, new Runnable() {
            public void run() {
              ArrayList<ItemStack> i = new ArrayList<ItemStack>();
              i.add(item);
              ItemUtils.giveItemToPlayer(getUser().getPlayer(), i);
            }
          }, 20L);
        } else {
          getUser().sendMessage("&cYou cannot afford to buy this item");
        }
      }
    }
    return false;
  }

  public void closeInv() {
    super.closeInv();
    removeClaims(getUser().getPlayer());
  }

  private static List<Integer> hasClaims(Player user) {
    List<Integer> itemsToRemove = new ArrayList<Integer>();
    ItemStack[] items = user.getInventory().getContents();
    for (int j = 0; j < items.length; j++) {
      ItemStack i = items[j];
      if (i == null || !i.hasItemMeta() || (i.hasItemMeta() && !i.getItemMeta().hasLore())) {
        continue;
      }
      if (i.getItemMeta().getLore().get(0).toLowerCase().contains("claim reward")) {
        itemsToRemove.add(j);
      }
    }
    return itemsToRemove;
  }

  public static void removeClaims(Player user) {
    List<Integer> itemsToRemove = hasClaims(user);
    for (Integer i : itemsToRemove) {
      user.getInventory().clear(i);
    }
    user.updateInventory();
  }

  @Override
  protected void extraItem(ItemStack item) {
    super.extraItem(item);
  }

  @Override
  protected void itemMissing(ItemStack item) {
    // removeClaims(getUser().getPlayer());
    getUser().getPlayer().playSound(getUser().getPlayer().getLocation(), Sound.ANVIL_LAND, 1, 0);
    MessageUtils.send(getUser().getPlayer(), "&8[&l&c!&r&8] Nice try there buddy, you cant steal items from menus");
    _plugin.getAPI().getMessageManager().log(Level.WARNING, "Item Missing - " + getUser().getName() + " - " + item.getAmount() + " " + item.getType().name() + ":" + item.getDurability());
  }

}
