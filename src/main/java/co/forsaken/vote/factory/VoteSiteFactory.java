/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote.factory;

import java.sql.SQLDataException;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.vote.factory.obj.VoteSite;

public class VoteSiteFactory extends DatabaseFactory<VoteSite> {

  public VoteSiteFactory(ForsakenGlobal plugin) {
    super(plugin, "vote_sites");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE IF NOT EXISTS `"
                + _tableName
                + "` ( `id` int(16) NOT NULL AUTO_INCREMENT, `server_id` int(16) NOT NULL, `name` varchar(64) NOT NULL, `url` varchar(128) NOT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
  }

  /**
   * Gets a vote site from its name
   */
  public VoteSite get(String name) throws Exception {
    for (VoteSite s : getAll()) {
      if (s.getName().equals(name)) { return s; }
    }
    String sql = "SELECT * FROM " + _tableName + " WHERE `name` = ? AND server_id = ? OR server_id = 0;";
    DatabaseResults query = _plugin.getAPI().getDatabase()
        .readEnhanced(sql, name, _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    if ((query != null) && query.hasRows()) {
      try {
        VoteSite s = parseFromQuery(query, 0);
        _databaseObjects.put(s.getId() + "", s);
        return s;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    throw new Exception("Could not get vote site " + name);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void initObject() {
    super.initObject();

    try {
      DatabaseResults query = _plugin
          .getAPI()
          .getDatabase()
          .readEnhanced("SELECT * FROM `" + _tableName + "` WHERE `server_id` = ? OR server_id = 0;",
              _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
      if (query != null && query.hasRows()) {
        for (int i = 0; i < query.rowCount(); i++) {
          try {
            VoteSite site = parseFromQuery(query, i);
            _databaseObjects.put(site.getId() + "", site);
          } catch (SQLDataException e) {
            e.printStackTrace();
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VoteSite parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    return new VoteSite(_plugin, query.getInteger(row, "id"), query.getInteger(row, "server_id"), query.getString(row, "name"), query.getString(row,
        "url"));
  }

}
