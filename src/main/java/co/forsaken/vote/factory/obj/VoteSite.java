/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote.factory.obj;

import java.sql.SQLDataException;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.vote.VoteRegistrar;

public class VoteSite extends DatabaseObject<VoteSite> {

  private int    _serverId;
  private String _name;
  private String _url;

  public VoteSite(ForsakenGlobal plugin, int id, int serverId, String name, String url) {
    super(plugin, id);
    _serverId = serverId;
    _name = name;
    _url = url;
  }

  public VoteSite(ForsakenGlobal plugin, int serverId, String name, String url) {
    super(plugin);
    _serverId = serverId;
    _name = name;
    _url = url;
    insert();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
    _plugin
        .getAPI()
        .getDatabase()
        .write("INSERT INTO `forsaken_vote_sites` (`server_id`, `name`, `url`) VALUES (?, ?, ?);",
            getServerId(), getName(), getURL());
    refresh();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {
    DatabaseResults query = _plugin
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT * FROM `forsaken_vote_sites` WHERE `name` = ? and `server_id` = ? AND `url` = ?;", getName(), getServerId(), getURL());
    try {
      VoteSite site = VoteRegistrar.getVoteSiteFactory().parseFromQuery(query, 0);
      _id = site.getId();
      _serverId = site.getServerId();
      _name = site.getName();
      _url = site.getURL();
    } catch (SQLDataException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
  }

  /**
   * Gets the server this site is relevant to
   * 
   * @return
   */
  public int getServerId() {
    return _serverId;
  }

  /**
   * Gets the name of the site
   * 
   * @return
   */
  public String getName() {
    return _name;
  }

  /**
   * Gets the url of the site
   * 
   * @return
   */
  public String getURL() {
    return _url;
  }

}
