/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenVote.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenVote is licensed under the Forsaken Network License Version 1
 *
 * ForsakenVote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenVote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.vote.factory.obj;

import java.sql.SQLDataException;
import java.util.Date;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.vote.VoteRegistrar;

public class Vote extends DatabaseObject<Vote> {

  private int  _userId;
  private int  _voteSiteId;
  private Date _date;

  public Vote(ForsakenGlobal plugin, int id, int userId, int voteSiteId, Date date) {
    super(plugin, id);
    _userId = userId;
    _voteSiteId = voteSiteId;
    _date = date;
  }

  public Vote(ForsakenGlobal plugin, int userId, int voteSiteId) {
    super(plugin);
    _userId = userId;
    _voteSiteId = voteSiteId;
    _date = new Date();
    insert();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete() {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void insert() {
    _plugin
        .getAPI()
        .getDatabase()
        .write(
            "INSERT INTO `forsaken_user_votes` (`user_id`, `vote_site_id`, `date`) VALUES (?, ?, ?)",
            getUserId(), getVoteSiteId(), getDate());
    refresh();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void refresh() {
    DatabaseResults query = _plugin
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT * FROM `forsaken_user_votes` WHERE `user_id`= ? AND `vote_site_id` = ? AND `date` = ?;", getUserId(), getVoteSiteId(), getDate());
    try {
      Vote v = VoteRegistrar.getVoteFactory().parseFromQuery(query, 0);
      _id = v.getId();
      _userId = v.getUserId();
      _voteSiteId = v.getVoteSiteId();
      _date = v.getDate();
    } catch (SQLDataException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update() {
  }

  /**
   * Gets the user who voted
   * 
   * @return
   */
  public int getUserId() {
    return _userId;
  }

  /**
   * Gets the id of the voting site
   * 
   * @return
   */
  public int getVoteSiteId() {
    return _voteSiteId;
  }

  public VoteSite getVoteSite() throws Exception {
    return VoteRegistrar.getVoteSiteFactory().get(getVoteSiteId());
  }

  /**
   * Gets the date this vote took place
   * 
   * @return
   */
  public Date getDate() {
    return _date;
  }

}
