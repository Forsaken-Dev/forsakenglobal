/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.managers;

import java.util.HashMap;
import java.util.Map;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.Request;

public class RequestManager {

  private static Map<String, Request> activeRequests = new HashMap<String, Request>();

  /**
   * Send a teleport request from one player to another
   * 
   * @param player
   *          Sender
   * @param player2
   *          Receiver
   */
  public static boolean sendTeleportRequest(String player1, String player2) {
    try {
      User sender = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(player1);
      User receiver = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(player2);
      addRequest(sender.getName(), receiver.getName(), false);
      receiver.sendMessage(player1 + " wants to teleport to you.");
      receiver.sendMessage("Type " + "/tpaccept " + "to accept.");

      sender.sendMessage("Your teleport request has been sent.");
      return true;
    } catch (Exception e) {
    }
    return false;
  }

  /**
   * Send a summon request from one player to another
   * 
   * @param player
   *          Sender
   * @param player2
   *          Receiver
   */
  public static boolean sendSummonRequest(String player1, String player2) {
    try {
      User sender = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(player1);
      User receiver = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(player2);
      addRequest(sender.getName(), receiver.getName(), true);

      receiver.sendMessage(player1 + " wants you to teleport to them.");
      receiver.sendMessage("Type " + "/tpaccept " + "to accept.");

      sender.sendMessage("Your summon request has been sent.");
      return true;
    } catch (Exception e) {
    }
    return false;

  }

  private static void addRequest(String sender, String receiver, boolean isSummon) throws Exception {
    if (activeRequests.containsKey(sender.toLowerCase())) {
      if (System.currentTimeMillis() - activeRequests.get(sender.toLowerCase()).createdTime >= 15 * 1000) { throw new Exception(
          "You already have an existing teleport request"); }
    }
    activeRequests.put(sender.toLowerCase(), new Request(sender, receiver, isSummon));
  }

  public static Request getPendingRequest(String receiver) {
    for (Request r : activeRequests.values()) {
      if (r.getReceiver().equalsIgnoreCase(receiver)) { return r; }
    }
    return null;
  }

  public static void removePendingRequest(String receiver) {
    String sender = "";
    for (Request r : activeRequests.values()) {
      if (r.getReceiver().equalsIgnoreCase(receiver)) {
        sender = r.getSender();
      }
    }
    if (!sender.isEmpty()) {
      activeRequests.remove(sender.toLowerCase());
    }
  }
}