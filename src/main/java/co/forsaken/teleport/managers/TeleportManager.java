/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.managers;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.tasks.DelayedCommandTask;
import co.forsaken.api.utils.WorldUtils;
import co.forsaken.teleport.factories.obj.Warp;
import co.forsaken.teleport.util.LocationUtils;

public class TeleportManager {

  private static Map<String, DelayedCommandTask> _teleports = new HashMap<String, DelayedCommandTask>();

  public static void executeTeleport(User sender, User receiver) {
    if (sender.teleport(receiver.getPlayer())) {
      sender.sendMessage("You have been teleported to " + receiver.getName());
      receiver.sendMessage(sender.getName() + " teleported to you");
    } else {
      receiver.sendMessage("Something went wrong and you could not teleport to " + receiver.getName());
    }
  }

  public static boolean executeTeleport(User sender, String receiverName) {
    try {
      User receiver = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(receiverName);
      executeTeleport(sender, receiver);
      return true;
    } catch (Exception e) {
    }
    return false;
  }

  public static boolean executeTeleport(String senderName, User receiver) {
    try {
      User sender = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(senderName);
      executeTeleport(sender, receiver);
      return true;
    } catch (Exception e) {
    }
    return false;
  }

  public static void executeWarp(User user, Warp warp) throws Exception {
    executeWarp(user, warp, false);
  }

  public static void executeWarp(User user, Warp warp, boolean force) throws Exception {
    if (!warp.isRandom()) {
      if (force || user.getSecondaryRank().getId() >= 10 || user.getPrimaryRankId() >= 4) {
        TeleportManager.overrideTeleport(user, getCommand(warp.getLocation()), true, null, null);
      } else {
        TeleportManager.executeTeleport(user, getCommand(warp.getLocation()), "Your warp to " + warp.getName() + " is warming up! Please stand still...",
            "Welcome to " + warp.getName() + ", you may be a little disoriented from the trip!");
      }
    } else {
      if (ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getName().equalsIgnoreCase("mine")) {
        TeleportManager.overrideTeleport(user, getCommand(LocationUtils.getRandomLocation(warp.getLocation().getWorld(), warp.getLocation()
            .getBlockX(), warp.getLocation().getBlockZ(), 0, 0, -1)), true, null, null);
      } else {
        TeleportManager.executeTeleport(user, getCommand(LocationUtils.getRandomLocation(warp.getLocation().getWorld(), warp.getLocation()
            .getBlockX(), warp.getLocation().getBlockZ(), 0, 0, -1)), "Your warp to " + warp.getName() + " is warming up! Please stand still...", "Welcome to "
            + warp.getName() + ", you may be a little disoriented from the trip!");
      }
    }
  }

  private static void overrideTeleport(User user, String command, boolean override, String startMsg, String finishMsg) throws Exception {
    if (_teleports.containsKey(user.getName().toLowerCase()) && !_teleports.get(user.getName().toLowerCase()).isCompleted) { throw new Exception(
        "You have a pending warp already, please wait to make another..."); }
    _teleports.put(user.getName().toLowerCase(), new DelayedCommandTask(ForsakenGlobal.getInstance(), user, command, startMsg, finishMsg, true,
        override));
  }

  public static void executeTeleport(User user, String command, String startMsg, String finishMsg) throws Exception {
    overrideTeleport(user, command, false, startMsg, finishMsg);
  }

  public static void executeTeleport(User user, Location location, String startMsg, String finishMsg) throws Exception {
    overrideTeleport(user, getCommand(location), false, startMsg, finishMsg);
  }

  private static String getCommand(Location location) {
    return "tp " + WorldUtils.getWorldId(location.getWorld().getName()) + " " + location.getBlockX() + " " + location.getBlockY() + " "
        + location.getBlockZ();
  }
}
