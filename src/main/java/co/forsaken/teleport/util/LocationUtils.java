/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.util;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public class LocationUtils {

  /**
   * Get a random location in a rectangle around the specified center position
   * 
   * @param world
   *          World of location, used in y position calcs
   * @param width
   *          Width of rectangle
   * @param height
   *          Height of rectangle
   * @param centerX
   *          Center X position
   * @param centerZ
   *          Center Z position
   * @param yPosition
   *          Y height for player to spawn at, -1 to use highest block.
   * @return
   */
  public static Location getRandomLocation(World world, int width, int height, int centerX, int centerZ, int yPosition) {
    Random rand = new Random();
    int randX = 0;
    int randZ = 0;
    Location loc = null;
    int index = 5;
    while (loc == null && index >= 0) {
      randX = (rand.nextInt(width) - (width / 2)) + centerX;
      randZ = (rand.nextInt(height) - (height / 2)) + centerZ;
      loc = getSafeLocation(new Location(world, randX, 0, randZ).getBlock(), 10);
      index--;
    }
    if (loc == null) {
      loc = new Location(world, randX, 0, randZ);
      loc.setY(loc.getWorld().getHighestBlockYAt(randX, randZ));
    }
    Material mat = Material.STONE;
    if (world.getName().equalsIgnoreCase("DIM-1")) {
      mat = Material.NETHERRACK;
    } else if (world.getName().equalsIgnoreCase("DIM1")) {
      mat = Material.ENDER_STONE;
    }

    for (int x = -1; x <= 1; x++) {
      for (int z = -1; z <= 1; z++) {
        loc.getWorld().getBlockAt(loc.getBlockX() + x, loc.getBlockY(), loc.getBlockZ() + z).setType(mat);
      }
    }
    return loc;
  }

  public static Location getSafeLocation(Block center, int radius) {
    for (int range = 1; range < radius + 1; range++) {
      // Start with the neg. range to encompass a radius, not just half
      // Every X within that range
      for (int x = -(range); x <= range; x++) {
        // Every Y within that X
        for (int y = 50; y <= 100; y++) {
          // Every Z within that Y within that X
          for (int z = -(range); z <= range; z++) {
            Block b = center.getWorld().getBlockAt(new Location(center.getWorld(), center.getX() + x, center.getY() + y, center.getZ() + z));
            // If the bottom block is solid and the block above it is not solid,
            // good enough broseph.
            if (b.getType().isSolid() && !b.getRelative(BlockFace.UP).getType().isSolid() && b.getType() != Material.BEDROCK) { return new Location(
                b.getWorld(), b.getX() + 0.5D, b.getY() + 0.5D, b.getZ() + 0.5D); }
          }
        }
      }
    }
    return null;
  }
}
