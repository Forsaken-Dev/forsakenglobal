/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.cmds.PeerTeleportCommands;

public class TeleportEvents extends BaseListener {
  private static final Random rand          = new Random();
  private static String       currentServer = "";

  public TeleportEvents(ForsakenGlobal plugin) {
    super(plugin);
    currentServer = _plugin.getServer().getMotd();
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerRespawn(PlayerRespawnEvent event) {
    Location loc = event.getPlayer().getBedSpawnLocation();
    if (loc == null) {
      try {
        loc = TeleportRegistrar.getWarpFactory().get("spawn").getLocation();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (loc != null) {
      event.setRespawnLocation(loc);
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
    try {
      if (event.getEntity() instanceof Player) {
        User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(((Player) event.getEntity()).getName());
        user.updateLastDamageTime();

      }
    } catch (Exception e) {
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerDeath(PlayerDeathEvent e) {
    PeerTeleportCommands._deathLocations.put(e.getEntity().getName().toLowerCase(), e.getEntity().getLocation());
  }

  // TODO remove this once you get the warps working for random destinations
  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerMove(PlayerMoveEvent event) {

    if (ForsakenGlobal.getInstance().getServer().getMotd().contains("dw")) {
      if (event.getFrom().getWorld().getName().equalsIgnoreCase("world")) {
        int x = event.getTo().getBlockX();
        int y = event.getTo().getBlockY();
        int z = event.getTo().getBlockZ();
        if (-46 <= x && x <= -44 && 181 <= y && y <= 185 && 4 <= z && z <= 6) {
          event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), -44, 255, 4));
        }
      }
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerTeleport(PlayerTeleportEvent e) {
    User user = null;
    try {
      user = _plugin.getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName());
      if (currentServer.contains("as") || currentServer.contains("bnb")) { return; }
    } catch (Exception e1) {
      e1.printStackTrace();
    }
    if (user == null || e.getFrom().getWorld().getName().equalsIgnoreCase(e.getTo().getWorld().getName())) { return; }
    if (!e.getTo().getWorld().getName().equalsIgnoreCase("world")) {
      user.sendMessage("&cThis world has &4PvP Enabled! &cBe careful of your surroundings!");
    }
    if (e.getTo().getWorld().getName().equalsIgnoreCase("DIM_MYST2") || e.getTo().getWorld().getName().equalsIgnoreCase("DIM-1")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM-100")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM-19")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM-31")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM1")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM20")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM6")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM7")
        || e.getTo().getWorld().getName().equalsIgnoreCase("DIM9")) {
      user.sendMessage("&cThis world resets every 2 weeks, we suggest you only mine here!");
    }
  }

  private void teleport(String username, String worldname, String realName, int xRange, int zRange) {
    try {
      User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(username);
      int newX = rand.nextInt(xRange) - (xRange / 2);
      int newZ = rand.nextInt(zRange) - (zRange / 2);
      while (-1000 <= newX && newX <= 1000) {
        newX = rand.nextInt(xRange) - (xRange / 2);
      }
      while (-1000 <= newZ && newZ <= 1000) {
        newZ = rand.nextInt(zRange) - (zRange / 2);
      }
      int newY = _plugin.getServer().getWorld(realName).getHighestBlockYAt(newX, newZ) + 1;
      if (worldname.equals("mining")) {
        newY = 84;
      }
      user.teleport(new Location(_plugin.getServer().getWorld(realName), newX, newY, newZ));
      user.sendMessage("&aYou have been teleported randomly to the &6" + worldname + " world");
      user.sendMessage("&aIf you need a friend to join you he can use /tpa (for a cost)");
    } catch (Exception e) {
    }
  }

}
