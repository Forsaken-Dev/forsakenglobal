/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.cmds;

import java.util.ArrayList;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.TeleportRegistrar;
import co.forsaken.teleport.factories.obj.Warp;
import co.forsaken.teleport.managers.TeleportManager;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;

public class WarpCommands {

  @Command(aliases = { "setwarp", "newwarp", "addwarp", "aw" }, desc = "Set a warp", usage = "[name]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions(value = { "teleport.setwarp" })
  public static void setWarp(CommandContext args, User user) throws CommandException {
    Location location = user.getPlayer().getLocation();
    String name = args.getString(0).toLowerCase();
    Warp warp = new Warp(ForsakenGlobal.getInstance(), location, name, false);
    warp.delete();
    warp.insert();
    user.sendMessage("Warp " + name + " created.");
  }

  @Command(aliases = { "delwarp", "deletewarp", "dw" }, desc = "Delete a warp", usage = "[name]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions(value = { "teleport.delwarp" })
  public static void delWarp(CommandContext args, User user) throws CommandException {
    String name = args.getString(0).toLowerCase();
    try {
      Warp warp = TeleportRegistrar.getWarpFactory().get(name);
      warp.delete();
      TeleportRegistrar.getWarpFactory().reload();
      user.sendMessage("Warp " + name + " deleted.");
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "warp", "w" }, desc = "Warp to a warp", usage = "[name]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions(value = { "teleport.warp" })
  public static void warp(CommandContext args, User user) throws CommandException {
    String name = args.getString(0).toLowerCase();
    try {
      Warp warp = TeleportRegistrar.getWarpFactory().get(name);
      TeleportManager.executeWarp(user, warp);
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Console
  @Command(aliases = { "force-warp" }, desc = "Forces a user to warp", min = 0, max = -1)
  @CommandPermissions(value = { "teleport.warp.admin" })
  public static void forceWarp(CommandContext args, User user) throws CommandException {
    String username = args.getString(0);
    String name = args.getString(1).toLowerCase();
    try {
      User sUser = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      if ((sUser.getPlayer() == null || (sUser.getPlayer() != null && !sUser.getPlayer().isOnline()))) {
        if (user != null) {
          user.sendMessage("&6" + username + " &eis not currently online");
        }
        return;
      }
      Warp warp = TeleportRegistrar.getWarpFactory().get(name);
      TeleportManager.executeWarp(sUser, warp, true);
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "spawn" }, desc = "Teleports to spawn", min = 0, max = -1)
  @CommandPermissions(value = { "teleport.warp" })
  public static void spawn(CommandContext args, User user) throws CommandException {
    ForsakenGlobal.getInstance().getServer().dispatchCommand(user.getPlayer(), "warp spawn");
  }

  @Command(aliases = { "mine" }, desc = "Teleports to mining age", min = 0, max = -1)
  @CommandPermissions(value = { "teleport.warp" })
  public static void mine(CommandContext args, User user) throws CommandException {
    ForsakenGlobal.getInstance().getServer().dispatchCommand(user.getPlayer(), "warp mine");
  }

  @Command(aliases = { "shop" }, desc = "Teleports to the shop", min = 0, max = -1)
  @CommandPermissions(value = { "teleport.warp" })
  public static void shop(CommandContext args, User user) throws CommandException {
    ForsakenGlobal.getInstance().getServer().dispatchCommand(user.getPlayer(), "warp shop");
  }

  @Command(aliases = { "warps", "listwarps" }, desc = "List all dem warps")
  @CommandPermissions(value = { "teleport.listwarps" })
  public static void list(CommandContext args, User user) throws CommandException {
    ArrayList<Warp> warps = TeleportRegistrar.getWarpFactory().getAllWarps();
    user.sendMessage("Found " + warps.size() + " warps");

    for (Warp warp : warps) {
      user.sendMessage(
          warp.getName() + " at " + "[" + (warp.isRandom() ? "RANDOM" : warp.getLocation().getWorld().getName() + ", " + warp.getLocation().getBlockX() + ", "
              + warp.getLocation().getBlockY() + ", " + warp.getLocation().getBlockZ()) + "]");
    }
  }

}
