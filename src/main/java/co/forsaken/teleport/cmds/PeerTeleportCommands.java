/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.cmds;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.managers.RequestManager;
import co.forsaken.teleport.managers.TeleportManager;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class PeerTeleportCommands {

  public static Map<String, Location> _deathLocations = new HashMap<String, Location>();

  @Command(aliases = { "summon", "tphere", "tpahere" }, desc = "Summons a user to your location", usage = "[user]", anyFlags = true, min = 1, max = 1)
  @CommandPermissions(value = { "teleport.summon", "teleport.summonreq" })
  public static void summonRequest(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use teleporting");
      return;
    }
    String player = args.getString(0);
    if (player.equalsIgnoreCase(user.getName())) { throw new CommandException("You cannot teleport to yourself"); }
    if (user.getPlayer().hasPermission("teleport.summon")) {
      if (!TeleportManager.executeTeleport(player, user)) throw new CommandException(player + " cannot be found!");
    } else if (user.getPlayer().hasPermission("teleport.summonreq")) {
      if (!RequestManager.sendSummonRequest(user.getName(), player)) { throw new CommandException(player + " cannot be found!"); }
    }
  }

  @Command(aliases = { "tp", "tpa", "teleport" }, desc = "Teleports to a user or location", usage = "[user/[x y z]]", anyFlags = true, min = 1, max = 4)
  @CommandPermissions(value = { "teleport.tp", "teleport.tpreq" })
  public static void teleportRequest(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use teleporting");
      return;
    }
    if (args.argsLength() == 1) {
      String player = args.getString(0);
      if (player.equalsIgnoreCase(user.getName())) { throw new CommandException("You cannot teleport to yourself"); }
      if (user.getPlayer().hasPermission("teleport.tp")) {
        if (!TeleportManager.executeTeleport(user, player)) throw new CommandException(player + " cannot be found!");
      } else if (user.getPlayer().hasPermission("teleport.tpreq")) {
        if (!RequestManager.sendTeleportRequest(user.getName(), player)) { throw new CommandException(player + " cannot be found!"); }
      }
    } else if (args.argsLength() >= 3) {
      if (((user == null || (user != null && user.getId() == 1)) && args.argsLength() == 4) || (user != null && user.getPlayer().hasPermission("teleport.tppos"))) {
        User sender = user;
        int x = 0;
        int y = 0;
        int z = 0;
        if (args.argsLength() == 4) {
          try {
            sender = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
            x = args.getInteger(1);
            y = args.getInteger(2);
            z = args.getInteger(3);
          } catch (Exception e) {
            throw new CommandException(e.getLocalizedMessage());
          }
        } else {
          x = args.getInteger(0);
          y = args.getInteger(1);
          z = args.getInteger(2);
        }
        sender.teleport(sender.getPlayer().getWorld().getName(), x, y, z);
      }
    }
  }

  @Command(aliases = { "back", "return", "ret" }, desc = "Returns to your death point", min = 0, max = 1)
  @CommandPermissions(value = { "teleport.back" })
  public static void back(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use teleporting");
      return;
    }
    if (!_deathLocations.containsKey(user.getName().toLowerCase())) { throw new CommandException("Your previous death point could not be found"); }
    int cost = (int) Math.ceil((int) ForsakenGlobal.getInstance().getAPI().getConfigManager().getInt("teleport.cost") * 1.5D);
    if (user.getSecondaryRankId() >= 10 || user.getPrimaryRankId() >= 4) {
      cost = 0;
    }
    if (cost > 0) {
      if (user.getCreditAmount() < cost) {
        user.sendMessage("You cannot afford to pay " + cost + " cR for this teleport. Your balance is only " + user.getCreditAmount() + " cR");
        return;
      }
      user.forceRemoveCredits(cost, "teleport-back");
    }
    user.teleport(_deathLocations.get(user.getName().toLowerCase()));
    user.sendMessage("&aWelcome to your last death point");
  }
}
