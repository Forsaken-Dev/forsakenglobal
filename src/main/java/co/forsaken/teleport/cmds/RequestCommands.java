/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.cmds;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.Request;
import co.forsaken.teleport.managers.RequestManager;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class RequestCommands {

  // TODO this system needs to be abstracted to the root api so that we can use
  // requests for anything and have it bind to these commands
  @Command(aliases = { "tpaccept" }, desc = "Accept a pending teleport request")
  @CommandPermissions(value = { "teleport.accept" })
  public static void accept(CommandContext args, User user) throws CommandException {
    Request req = RequestManager.getPendingRequest(user.getName());
    if (req == null) {
      user.sendMessage("You have no pending requests");
    } else {
      req.executeRequest();
    }
    RequestManager.removePendingRequest(user.getName());
  }

  @Command(aliases = { "tpdeny" }, desc = "Deny a pending teleport request")
  @CommandPermissions(value = { "teleport.deny" })
  public static void deny(CommandContext args, User user) throws CommandException {
    Request req = RequestManager.getPendingRequest(user.getName());
    if (req == null) {
      user.sendMessage("You have no pending requests");
    } else {
      user.sendMessage("Teleport request from " + req.getSender() + " denied");
      try {
        User sender = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(req.getSender());
        sender.sendMessage("Your teleport request to " + req.getReceiver() + " denied");
      } catch (Exception e) {
        throw new CommandException(e.getLocalizedMessage());
      }
    }
    RequestManager.removePendingRequest(req.getReceiver());
  }
}
