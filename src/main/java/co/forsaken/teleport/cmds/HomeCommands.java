/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.cmds;

import java.util.ArrayList;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.teleport.TeleportRegistrar;
import co.forsaken.teleport.factories.obj.Home;
import co.forsaken.teleport.managers.TeleportManager;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class HomeCommands {

  @Command(aliases = { "sethome" }, desc = "Set a home", usage = "[name]")
  @CommandPermissions(value = { "teleport.sethome" })
  public static void setHome(CommandContext args, User user) throws CommandException {

    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use homes");
      return;
    }
    Home home = new Home(ForsakenGlobal.getInstance(), user.getPlayer().getWorld().getName(), user.getPlayer().getLocation().getBlockX(), user
        .getPlayer().getLocation().getBlockY(), user.getPlayer().getLocation().getBlockZ(), "defhome", user.getName());
    if (args.argsLength() > 0) {
      if (!user.getPlayer().hasPermission("teleport.sethome.multiple")) { throw new CommandException(
          "You need to be a vip+ to set multiple homes! Visit http://www.forsaken.co/donate for more info."); }
      String homeName = args.getString(0).toLowerCase();
      int numHomes = TeleportRegistrar.getHomeFactory().getAllHomes(user.getName()).size();
      int numHomesAllowed = 1;
      if (user.getSecondaryRankId() == 9) {
        numHomesAllowed += 5;
      } else if (user.getSecondaryRankId() == 10) {
        numHomesAllowed += 10;
      } else {
        numHomesAllowed = -1;
      }

      if (numHomesAllowed != -1 && numHomes >= numHomesAllowed) {
        try {
          TeleportRegistrar.getHomeFactory().get(homeName, user.getName());
        } catch (Exception e) {
          throw new CommandException("You can only set " + numHomesAllowed
              + " homes currently. If you want to update/set new homes you will have to be under " + numHomesAllowed);
        }
      }
      home.setName(homeName);
    }
    home.delete();
    home.insert();

    TeleportRegistrar.getHomeFactory().reload();

    user.sendMessage("Your new home has been created!");
  }

  @Command(aliases = { "delhome" }, desc = "Delete a home", usage = "[name] <username>", min = 1, max = 2)
  @CommandPermissions(value = { "teleport.delhome" })
  public static void delHome(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use homes");
      return;
    }
    String homeName = (args.argsLength() > 0) ? args.getString(0) : "defhome";
    String username = (args.argsLength() == 2 && user.getPlayer().hasPermission("teleport.home.other") ? args.getString(1) : user.getName());

    try {
      Home home = TeleportRegistrar.getHomeFactory().get(homeName, username);
      home.delete();
      TeleportRegistrar.getHomeFactory().reload();
      user.sendMessage((homeName.equals("defhome") ? username + "'s home" : homeName) + " has faded into nothingness.");
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "home" }, desc = "Go to a home", usage = "[name] <username>", min = 0, max = 2)
  @CommandPermissions(value = { "teleport.home" })
  public static void home(CommandContext args, User user) throws CommandException {
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use homes");
      return;
    }
    String homeName = (args.argsLength() > 0) ? args.getString(0) : "defhome";
    String username = (args.argsLength() == 2 && user.getPlayer().hasPermission("teleport.home.other") ? args.getString(1) : user.getName());

    try {
      Home home = TeleportRegistrar.getHomeFactory().get(homeName, username);
      TeleportManager.executeTeleport(user, home.getLocation(), "&eYour warp to &6" + (homeName.equalsIgnoreCase("defhome") ? "" : homeName)
          + " home" + " &eis warming up!", "&eWelcome to &6" + (homeName.equalsIgnoreCase("defhome") ? "" : homeName) + " home"
          + "&e, you may be a little disoriented from the trip!");
    } catch (Exception e) {
      throw new CommandException(e.getLocalizedMessage());
    }
  }

  @Command(aliases = { "homes" }, desc = "List your homes", usage = "[name]", min = 0, max = 1)
  @CommandPermissions(value = { "teleport.listhomes" })
  public static void listHomes(CommandContext args, User user) throws CommandException {
    User usr = user;
    if (args.argsLength() == 1) {
      if (user.getPlayer().hasPermission("teleport.listhomes.other")) {
        try {
          usr = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
        } catch (Exception e) {
          throw new CommandException(e.getLocalizedMessage());
        }
      }
    }
    if (user.getPrimaryRankId() == 1) {
      user.sendMessage("&eYou need to /register an account before you can use homes");
      return;
    }
    ArrayList<Home> homes = TeleportRegistrar.getHomeFactory().getAllHomes(usr.getName());
    user.sendMessage("Found " + homes.size() + " homes for " + usr.getName());

    for (Home home : homes) {
      user.sendMessage(home.getName() + " at " + "[" + home.getWorldName() + ", " + home.getX() + ", " + home.getY() + ", " + home.getZ() + "]");
    }
  }
}
