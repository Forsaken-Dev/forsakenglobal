/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.factories;

import java.sql.SQLDataException;
import java.util.ArrayList;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.teleport.factories.obj.Home;

public class HomeFactory extends DatabaseFactory<Home> {

  public HomeFactory(ForsakenGlobal plugin) {
    super(plugin, "homes");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    String sql = "CREATE TABLE " + _tableName + " (" + "`id` int(10) unsigned NOT NULL AUTO_INCREMENT," + "`name` varchar(16) NOT NULL,"
        + "`owner` varchar(16) NOT NULL," + "`loc_world` varchar(16) NOT NULL," + "`loc_x` mediumint(9) NOT NULL," + "`loc_y` mediumint(9) NOT NULL,"
        + "`loc_z` mediumint(9) NOT NULL," + "`server_id` tinyint(4) NOT NULL," + "PRIMARY KEY (`id`)" + ") ENGINE=MyISAM DEFAULT CHARSET=latin1;";
    ForsakenGlobal.getInstance().getAPI().getDatabase().write(sql);
  }

  /**
   * Get a home for the specified name and owner
   * 
   * @param name
   * @param owner
   * @return
   * @throws Exception
   */
  public Home get(String name, String owner) throws Exception {
    for (Home h : getAll()) {
      if (h.getName().equalsIgnoreCase(name) && h.getOwner().equalsIgnoreCase(owner)) { return h; }
    }

    String sql = "SELECT * FROM " + this._tableName + " WHERE name=? AND owner=? AND server_id=?";
    DatabaseResults results = ForsakenGlobal.getInstance().getAPI().getDatabase()
        .readEnhanced(sql, name, owner, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());

    if ((results != null) && results.hasRows()) {
      try {
        Home home = parseFromQuery(results, 0);
        this._databaseObjects.put(name, home);
        return home;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    throw new Exception("This home does not exist.");
  }

  /**
   * Get all homes for specified user.
   * 
   * @param owner
   * @return
   */
  public ArrayList<Home> getAllHomes(String owner) {
    ArrayList<Home> homes = new ArrayList<Home>();

    String sql = "SELECT * FROM " + this._tableName + " WHERE owner=? AND server_id=?";
    try {
      DatabaseResults results = ForsakenGlobal.getInstance().getAPI().getDatabase()
          .readEnhanced(sql, owner, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());

      if ((results != null) && results.hasRows()) {
        for (int row = 0; row < results.rowCount(); row++) {
          homes.add(parseFromQuery(results, row));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return homes;
  }

  /**
   * /** {@inheritDoc}
   */
  @Override
  public Home parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("Error parsing home from query");
    return new Home(ForsakenGlobal.getInstance(), query.getString(row, "loc_world").equalsIgnoreCase("0") ? "world" : query.getString(row,
        "loc_world"), query.getInteger(row, "loc_x"), query.getInteger(row, "loc_y"), query.getInteger(row, "loc_z"), query.getString(row, "name"),
        query.getString(row, "owner"));
  }

}
