/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.factories.obj;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.teleport.TeleportRegistrar;

public class Home extends DatabaseObject<Home> {

  private String locWorld;
  private int    locX;
  private int    locY;
  private int    locZ;
  private String owner;
  private String name;

  public Home(ForsakenGlobal plugin, String locWorld, int locX, int locY, int locZ, String name, String owner) {
    super(plugin);

    this.locWorld = locWorld;
    this.locX = locX;
    this.locY = locY;
    this.locZ = locZ;
    this.name = name;
    this.owner = owner;
  }

  /**
   * Get location of this home
   * 
   * @return
   */
  public Location getLocation() {
    return new Location(_plugin.getAPI().getFactoryManager().getServerFactory().getWorld(locWorld), locX, locY, locZ);
  }

  /**
   * Get the name of the world
   * 
   * @return
   */
  public String getWorldName() {
    return this.locWorld;
  }

  /**
   * Get the X coordinate
   * 
   * @return
   */
  public int getX() {
    return this.locX;
  }

  /**
   * Get the y coordinate
   * 
   * @return
   */
  public int getY() {
    return this.locY;
  }

  /**
   * Get the z coordinate
   * 
   * @return
   */
  public int getZ() {
    return this.locZ;
  }

  /**
   * Set location of this home
   * 
   * @param location
   */
  public void setLocation(Location location) {
    this.locWorld = location.getWorld().getName();
    this.locX = location.getBlockX();
    this.locY = location.getBlockY();
    this.locZ = location.getBlockZ();
  }

  /**
   * Get the owner of this home
   * 
   * @return
   */
  public String getOwner() {
    return owner;
  }

  /**
   * Set the owner of this home
   * 
   * @param owner
   */
  public void setOwner(String owner) {
    this.owner = owner;
  }

  /**
   * Get the name of this home
   * 
   * @return
   */
  public String getName() {
    return name;
  }

  /**
   * Set the name of this home
   * 
   * @param name
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void delete() {
    String sql = "DELETE FROM forsaken_homes WHERE name=? AND owner=? AND server_id=?";
    try {
      ForsakenGlobal.getInstance().getAPI().getDatabase()
          .write(sql, getName(), getOwner(), ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void insert() {
    String sql = "INSERT INTO forsaken_homes (name, owner, loc_world, loc_x, loc_y, loc_z, server_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
    try {
      ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .write(sql, getName(), getOwner(), locWorld, locX, locY, locZ,
              ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void refresh() {

  }

  @Override
  public void update() {

  }

}
