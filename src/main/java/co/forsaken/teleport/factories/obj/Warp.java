/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.factories.obj;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseObject;
import co.forsaken.teleport.TeleportRegistrar;

public class Warp extends DatabaseObject<Warp> {

  // TODO Make the cost and rank fields actually useful.

  private Location location;
  private String   name;
  private int      cost;
  private int      rank;
  private boolean  isRandom;

  public Warp(ForsakenGlobal plugin, Location location, String name, boolean isRandom) {
    this(plugin, location, name, 0, 0, isRandom);
  }

  public Warp(ForsakenGlobal plugin, Location location, String name, int cost, int rank, boolean isRandom) {
    super(plugin);

    this.location = location;
    this.name = name;
    this.cost = cost;
    this.rank = rank;
    this.isRandom = isRandom;
  }

  /**
   * Get the location of this warp
   * 
   * @return
   */
  public Location getLocation() {
    return location;
  }

  /**
   * Set the location of this warp
   * 
   * @param location
   */
  public void setLocation(Location location) {
    this.location = location;
  }

  /**
   * Get the name of this warp
   * 
   * @return
   */
  public String getName() {
    return name;
  }

  /**
   * Set the name of this warp
   * 
   * @param name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Get the cost to tp to this warp
   * 
   * @return
   */
  public int getCost() {
    return cost;
  }

  /**
   * Set the cost to tp to this warp
   * 
   * @param cost
   */
  public void setCost(int cost) {
    this.cost = cost;
  }

  /**
   * Get the rank required to tp to this warp
   * 
   * @return
   */
  public int getRank() {
    return rank;
  }

  /**
   * Set the rank required to tp to this warp
   * 
   * @param rank
   */
  public void setRank(int rank) {
    this.rank = rank;
  }

  /**
   * Is this a random teleport warp
   * 
   * @return
   */
  public boolean isRandom() {
    return isRandom;
  }

  /**
   * Set this warp to be a random teleport warp
   * 
   * @param isRandom
   */
  public void setRandom(boolean isRandom) {
    this.isRandom = isRandom;
  }

  @Override
  public void delete() {
    String sql = "DELETE FROM " + TeleportRegistrar.getWarpFactory().getTableName() + " WHERE name=? AND server_id=?";
    try {
      ForsakenGlobal.getInstance().getAPI().getDatabase()
          .write(sql, getName(), ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void insert() {
    String sql = "INSERT INTO " + TeleportRegistrar.getWarpFactory().getTableName()
        + " (name, loc_world, loc_x, loc_y, loc_z, rank, cost, server_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    try {
      ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .write(sql, getName(), getLocation().getWorld().getName(), getLocation().getBlockX(), getLocation().getBlockY(), getLocation().getBlockZ(),
              getRank(), getCost(), ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void refresh() {
  }

  @Override
  public void update() {
  }
}
