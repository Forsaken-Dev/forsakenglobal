/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSuite.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSuite is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSuite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSuite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenTeleport.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenTeleport is licensed under the Forsaken Network License Version 1
 *
 * ForsakenTeleport is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenTeleport is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.teleport.factories;

import java.sql.SQLDataException;
import java.util.ArrayList;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.db.DatabaseFactory;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.teleport.factories.obj.Warp;

public class WarpFactory extends DatabaseFactory<Warp> {

  public WarpFactory(ForsakenGlobal ForsakenGlobal) {
    super(ForsakenGlobal, "warps");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createTable() {
    ForsakenGlobal.getInstance()
        .getAPI()
        .getDatabase()
        .write(
            "CREATE TABLE `" + _tableName + "` (" + "`id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT," + "`name` varchar(16) NOT NULL,"
                + "`loc_world` varchar(16) NOT NULL," + "`loc_x` int(9) NOT NULL," + "`loc_y` int(9) NOT NULL," + "`loc_z` int(9) NOT NULL,"
                + "`rank` int(4) NOT NULL DEFAULT '0'," + "`cost` int(8) unsigned NOT NULL DEFAULT '0'," + "`server_id` int(4) NOT NULL,"
                + "`is_random` int(4) NOT NULL DEFAULT '0'," + "PRIMARY KEY (`id`)" + ") ENGINE=MyISAM DEFAULT CHARSET=latin1;");
  }

  /**
   * Gets a warp from its name
   */
  public Warp get(String name) throws Exception {
    for (Warp w : getAll()) {
      if (w.getName().equalsIgnoreCase(name)) { return w; }
    }

    String sql = "SELECT * FROM " + this._tableName + " WHERE name=? AND server_id=?";
    DatabaseResults results = ForsakenGlobal.getInstance().getAPI().getDatabase()
        .readEnhanced(sql, name, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());

    if ((results != null) && results.hasRows()) {
      try {
        Warp warp = parseFromQuery(results, 0);
        this._databaseObjects.put(name, warp);
        return warp;
      } catch (SQLDataException e) {
        e.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    throw new Exception("This warp does not exist");
  }

  /**
   * Get all dem warps
   * 
   * @return
   */
  public ArrayList<Warp> getAllWarps() {
    ArrayList<Warp> warps = new ArrayList<Warp>();

    String sql = "SELECT * FROM " + this._tableName + " WHERE server_id=?";
    try {
      DatabaseResults results = ForsakenGlobal.getInstance().getAPI().getDatabase()
          .readEnhanced(sql, ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());

      if ((results != null) && results.hasRows()) {
        for (int row = 0; row < results.rowCount(); row++) {
          warps.add(parseFromQuery(results, row));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return warps;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Warp parseFromQuery(DatabaseResults query, int row) throws SQLDataException, Exception {
    if ((query == null) || !query.hasRows() || (query.rowCount() <= row)) throw new Exception("Warp could not be parsed from database");
    return new Warp(_plugin, new Location(ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory()
        .getWorld(query.getString(row, "loc_world")), (double) query.getInteger(row, "loc_x") + 0.5D, (double) query.getInteger(row, "loc_y"),
        query.getInteger(row, "loc_z") + 0.5D), query.getString(row, "name"), query.getInteger(row, "cost"), query.getInteger(row, "rank"),
        (query.getInteger(row, "is_random") == 1));
  }

}
