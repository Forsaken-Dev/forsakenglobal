/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.sessions.events;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.obj.Server;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.factories.obj.User.SessionTypes;
import co.forsaken.api.manager.ItemMenuManager;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.ItemUtils;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.SerializationUtils;
import co.forsaken.api.utils.db.DatabaseResults;
import co.forsaken.sessions.obj.SerializedPlayerData;
import co.forsaken.sessions.obj.SessionPlayer;
import co.forsaken.sessions.tasks.InventoryLoader;
import co.forsaken.sessions.tasks.TabUpdateTask;

public class ConnectionListener extends BaseListener {

  private static Map<String, String>         _userIps          = new HashMap<String, String>();
  private static List<String>                _newUsers         = new ArrayList<String>();
  private static Map<String, List<String>>   _serializedItems  = new HashMap<String, List<String>>();
  public static Map<String, InventoryLoader> _inventories      = new HashMap<String, InventoryLoader>();
  public static List<String>                 _savedInventories = new ArrayList<String>();
  public static Map<String, Integer>         _moveTaskIds      = new HashMap<String, Integer>();

  private static ItemStack                   _starterItem;

  public static void addIP(String username, String ip) {
    _userIps.put(username.toLowerCase(), ip);
    try {
      User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(username);
      user.addSession(ip, SessionTypes.LOGIN);
    } catch (Exception e) {
    }
  }

  public ConnectionListener(ForsakenGlobal plugin) {
    super(plugin);
    try {
      DatabaseResults query = ForsakenGlobal.getInstance().getAPI().getDatabase().readEnhanced("SELECT item FROM forsaken_starter_kits WHERE server_id = ?", ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
      if (query != null && query.hasRows()) {
        _starterItem = SerializationUtils.deserialize(query.getString(0, "item"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerRespawn(PlayerRespawnEvent event) {
    if (event.getPlayer() != null) {
      if (_serializedItems.containsKey(event.getPlayer().getName().toLowerCase())) {
        for (String item : _serializedItems.get(event.getPlayer().getName().toLowerCase())) {
          try {
            event.getPlayer().getInventory().addItem(SerializationUtils.deserialize(item));
          } catch (IllegalArgumentException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        _serializedItems.remove(event.getPlayer().getName().toLowerCase());
      }
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerDeath(PlayerDeathEvent event) {
    if (event.getEntity() != null) {
      try {
        User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getEntity().getName());
        ItemMenuManager.closeAny(user.getName());

        if (user.getOnlineSeconds() <= Math.pow(60, 2)) {
          _serializedItems.put(user.getName().toLowerCase(), new ArrayList<String>());
          for (ItemStack item : event.getDrops()) {
            try {
              _serializedItems.get(user.getName().toLowerCase()).add(SerializationUtils.serialize(item));
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          event.getDrops().clear();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
  public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
    // if
    // (ForsakenGlobal.getInstance().getServer().getMotd().contains("dw_main")
    // ||
    // ForsakenGlobal.getInstance().getServer().getMotd().contains("dw_donor"))
    // {
    // if (event.getName().equalsIgnoreCase("vanZeben")) {
    // try {
    // new SerializedPlayerData(event.getName()).apply();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }
    // }
  }

  /**
   * Let everyone know when a user has actually joined
   * 
   * @param event
   */
  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
  public void onPlayerJoin(PlayerJoinEvent event) {
    event.setJoinMessage(null);
    if (event.getPlayer() != null) {
      try {
        User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
        Server currentS = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent();
        String current = currentS.getName();
        user.connect(current);
        ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), new Runnable() {
          public void run() {
            TabUpdateTask.updateAll();
          }
        }, 20L);
        if (current.contains("dw_main") || current.contains("dw_donor")) {
          // if (!user.getName().equalsIgnoreCase("vanZeben")) {
          ForsakenGlobal.getInstance().getServer()
              .dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "loadinventory " + event.getPlayer().getName());
          // }
        } else if (current.equals("as_main") && event.getPlayer().getWorld().getName().equalsIgnoreCase("skyworld")) {
          ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "force-warp " + event.getPlayer().getName() + " spawn");
          user.sendMessage("&eYou logged into a world that doesnt exist, use /island home to get to your home");
        }
        if (_newUsers.contains(event.getPlayer().getName().toLowerCase())) {
          _newUsers.remove(event.getPlayer().getName().toLowerCase());
          ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "force-warp " + event.getPlayer().getName() + " spawn");
          ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage("&7Welcome &6" + user.getName() + " &7to the Forsaken Network's &6" + currentS.getFriendlyName() + " server", true);
          if (_starterItem != null) {
            List<ItemStack> i = new ArrayList<ItemStack>();
            i.add(_starterItem);
            ItemUtils.giveItemToPlayer(event.getPlayer(), i);
            user.sendMessage("&7Have some items to help get you started");
          }
        }
        MessageUtils.send(user.getPlayer(), _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getWelcomeMessage().replaceAll("%name%", user.getName()));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private List<PlayerQueuePosition> playerQueue = new ArrayList<PlayerQueuePosition>();

  public class PlayerQueuePosition {
    public String playername;
    public long   updateTime;

    public PlayerQueuePosition(String playername) {
      this.playername = playername;
      this.updateTime = System.currentTimeMillis();
    }
  }

  @EventHandler(priority = EventPriority.NORMAL)
  public void onPlayerPortalEvent(PlayerPortalEvent event) {
    if (event.getPlayer().isOp()) { return; }
    String current = "";
    try {
      current = _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName();
      if (current.equalsIgnoreCase("lobby")) {
        event.setCancelled(true);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void onPlayerLogin(PlayerLoginEvent event) {
    User user = null;
    try {
      user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());

      DatabaseResults query = _plugin
          .getAPI()
          .getDatabase()
          .readEnhanced("SELECT COUNT(*) FROM forsaken_user_sessions WHERE user_id = ? AND server_id = ? GROUP BY user_id;", user.getId(),
              _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId());
      if (query == null || (query != null && !query.hasRows())) {
        _newUsers.add(event.getPlayer().getName().toLowerCase());
      } else if (query != null) {
        if (Integer.parseInt(query.rawResults.get(0).get(0) + "") < 1) {
          _newUsers.add(event.getPlayer().getName().toLowerCase());
        }
      }
    } catch (Exception e) {
      user = _plugin.getAPI().getFactoryManager().getUserFactory().createUser(event.getPlayer().getName());
      _newUsers.add(event.getPlayer().getName().toLowerCase());
    }
    if (user == null) {
      event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Something went wrong with the login, please try again.");
      event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
      return;
    }
    try {
      if (_plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName().equalsIgnoreCase("donor")
          && !(user.getSecondaryRank().getDonationRequired() > 0 || user.getPrimaryRank().isStaff())) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
            "This server is restricted to donors only. Donate to get access at &ewww.forsaken.co/donate");
        event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
      }
    } catch (Exception e) {
    }

    if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) { return; }

    int totalSlots = _plugin.getServer().getMaxPlayers();
    Player[] playersOnline = _plugin.getServer().getOnlinePlayers();

    if (playersOnline.length >= totalSlots - 2) {
      int onlineSeconds = user.getOnlineSeconds();

      if (user.getPrimaryRank().isStaff() || user.getSecondaryRank().getDonationRequired() > 0 || onlineSeconds <= (5 * 60)) {
        permitLogin(user.getPrimaryRank().isStaff());
      } else {
        String queueCheck = queueCheck(totalSlots, playersOnline.length, event.getPlayer().getName());
        if (!queueCheck.isEmpty()) {
          event.setResult(PlayerLoginEvent.Result.KICK_FULL);
          event.disallow(event.getResult(), MessageUtils.format(queueCheck, true));
        }
      }
    } else {
      if (!playerQueue.isEmpty()) {
        String queueCheck = queueCheck(totalSlots, playersOnline.length, event.getPlayer().getName());
        if (!queueCheck.isEmpty()) {
          event.setResult(PlayerLoginEvent.Result.KICK_FULL);
          event.disallow(event.getResult(), MessageUtils.format(queueCheck, true));
        }
      }
    }

    if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) { return; }

    final Player player = event.getPlayer();
    if (!_plugin.getServer().getOnlineMode()) {
      // if (!event.getAddress().getHostAddress().contains("127.0.0.1") &&
      // !event.getAddress().getHostAddress().contains("localhost") &&
      // !event.getPlayer().isOp()) {
      // event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
      // "Sorry, you can't connect directly to our servers");
      // return;
      // }
      _plugin.getServer().getScheduler().runTaskLater(_plugin, new Runnable() {
        public void run() {
          _plugin.sendPluginMessage("IP", "", player);
        }
      }, 1 * 20);
    } else {
      addIP(user.getName(), event.getAddress().getHostAddress());
    }
  }

  private String queueCheck(int totalSlots, int playersOnline, String playerName) {
    int queuePosition = getQueuePosition(playerName);
    if (totalSlots - 1 - playersOnline > queuePosition) {
      this.playerQueue.remove(queuePosition);
    } else {
      String msg = "&6You have been added to our login queue!";
      msg += "\n&ePosition &6" + (queuePosition + 1) + " &eof &6" + playerQueue.size() + " &ein queue.";
      msg += "\n&eReconnect within &63 minutes &eto keep your place.";
      msg += "\n&eYou can &adonate &efor a reserved slot @ &ahttp://forsaken.co/donate";
      return msg;
    }
    return "";
  }

  private void permitLogin(boolean staff) {
    for (int i = 0; i < 3; i++) {
      for (Player p : _plugin.getServer().getOnlinePlayers()) {
        User u = null;
        int onlineSeconds = 999;
        try {
          u = _plugin.getAPI().getFactoryManager().getUserFactory().get(p.getName());
          onlineSeconds = u.getOnlineSeconds();
        } catch (Exception e) {
        }

        if ((u == null || (u.getPrimaryRankId() == i && u.getSecondaryRankId() <= 2)) && onlineSeconds > (5 * 60)) {
          kickPlayer(p, staff);
          return;
        }
      }
    }
  }

  private static final int QUEUE_POSITION_LENGTH = 3 * 60;

  private int getQueuePosition(String name) {
    int queuePosition = -1;
    for (int i = 0; i < this.playerQueue.size(); i++) {
      PlayerQueuePosition pqp = this.playerQueue.get(i);
      if (System.currentTimeMillis() - pqp.updateTime >= (QUEUE_POSITION_LENGTH * 1000)) {
        this.playerQueue.remove(i--);
      } else if (pqp.playername.equalsIgnoreCase(name)) {
        queuePosition = i;
        pqp.updateTime = System.currentTimeMillis();
        break;
      }
    }
    if (queuePosition == -1) {
      if (playerQueue.size() == 0) {
        queuePosition = 0;
      } else {
        queuePosition = 0;
      }
      playerQueue.add(queuePosition, new PlayerQueuePosition(name));
    }
    return queuePosition;
  }

  private void kickPlayer(Player p, boolean staff) {
    String msg = "&6You have been kicked from the server!";
    msg += "\n&eYour slot was needed for a &6" + (staff ? "staff member" : "donor");
    msg += "\n&eIf you want to prevent this from happening again";
    msg += "\n&eyou can &adonate &efor a reserved slot @ &ahttp://forsaken.co/donate";
    msg += "\n&eYou have been placed in the &6login queue &eat position &a" + getQueuePosition(p.getName());
    p.kickPlayer(MessageUtils.format(msg, true));
  }

  /**
   * Let everyone know when a user leaves and close the session
   * 
   * @param event
   */
  @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
  public void onPlayerQuit(PlayerQuitEvent event) {
    event.setQuitMessage(null);
    if (event.getPlayer() != null) {
      try {
        final User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(event.getPlayer().getName());
        String current = _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName();

        user.disconnect();
        ItemMenuManager.closeAny(user.getName());
        if (current.contains("dw_main") || current.contains("dw_donor")) {
          if (_savedInventories.contains(event.getPlayer().getName().toLowerCase())) {
            _savedInventories.remove(event.getPlayer().getName().toLowerCase());
          } else {
            saveInventory(user);
          }
          if (_moveTaskIds.containsKey(event.getPlayer().getName().toLowerCase())) {
            _plugin.getServer().getScheduler().cancelTask(_moveTaskIds.get(event.getPlayer().getName().toLowerCase()));
            _moveTaskIds.remove(event.getPlayer().getName().toLowerCase());
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onCommandPreprocess(ServerCommandEvent event) {
    if (event.getCommand().toLowerCase().contains("save-all")) {
      try {
        String current = _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName();
        if (current.contains("dw_main") || current.contains("dw_donor")) {
          for (Player p : _plugin.getServer().getOnlinePlayers()) {
            if (!_savedInventories.contains(p.getName().toLowerCase())) {
              try {
                User user =
                    _plugin.getAPI().getFactoryManager().getUserFactory().get(p.getName());
                ItemMenuManager.closeAny(user.getName());
                saveInventory(user);
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
    if (event.getMessage().split(" ")[0].equalsIgnoreCase("/save-all")) {
      if (event.getMessage().toLowerCase().contains("save-all")) {
        try {
          String current = _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName();
          if (current.contains("dw_main") || current.contains("dw_donor")) {
            for (Player p : _plugin.getServer().getOnlinePlayers()) {
              try {
                User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(p.getName());
                ItemMenuManager.closeAny(user.getName());

                saveInventory(user);
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static void saveInventory(User user) {
    // if (user.getName().equalsIgnoreCase("vanZeben")) {
    // try {
    // user.getPlayer().saveData();
    // new SerializedPlayerData(user.getName()).save();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }
    if (_inventories.containsKey(user.getName().toLowerCase()) &&
        _inventories.get(user.getName().toLowerCase())._cancelled) {
      new SessionPlayer(user).insert();

      ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.WARNING,
          "[INVENTORY_" + user.getName() + "] Saved inventory");
    } else {
      ForsakenGlobal.getInstance().getAPI().getMessageManager()
          .log(Level.WARNING, "[INVENTORY_" + user.getName() +
              "] Inventory tried to save when loader was not cancelled");
    }
  }

}
