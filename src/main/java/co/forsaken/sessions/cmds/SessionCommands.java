/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.sessions.cmds;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Rank;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.StringMgmt;
import co.forsaken.sessions.events.ConnectionListener;
import co.forsaken.sessions.tasks.InventoryLoader;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;

public class SessionCommands {

  @Console
  @Command(aliases = { "saveinventory" }, desc = "Saves a users inventory", usage = "[username] [remove]", anyFlags = true, min = 2, max = 2)
  @CommandPermissions("sessions.saveinventory")
  public static void saveInv(CommandContext args, User user) throws CommandException {
    try {
      User usr = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      if (!ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getName().equalsIgnoreCase("lobby")) {
        ConnectionListener._savedInventories.add(usr.getName().toLowerCase());
        ConnectionListener.saveInventory(usr);
        if (args.argsLength() == 2) {
          usr.getPlayer().getInventory().clear();
          usr.getPlayer().getInventory().setArmorContents(new ItemStack[] { null, null, null, null });
          usr.getPlayer().updateInventory();
          usr.sendMessage("&aSaving your inventory!");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Console
  @Command(aliases = { "loadinventory" }, desc = "Loads a users inventory", usage = "[username]", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("sessions.loadinventory")
  public static void loadInv(CommandContext args, User user) throws CommandException {
    try {
      User usr = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      if (usr.getPlayer() != null && usr.getPlayer().isOnline()) {
        if (!ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getName().equalsIgnoreCase("lobby")) {
          ConnectionListener._inventories.put(usr.getName().toLowerCase(), new InventoryLoader(ForsakenGlobal.getInstance(), usr.getName()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Console
  @Command(aliases = { "resetplayerfile", "rpf" }, desc = "Resets a users player file", usage = "[username]", anyFlags = true, min = 1, max = -1)
  @CommandPermissions("sessions.resetplayer")
  public static void resetPlayer(CommandContext args, User user) throws CommandException {
    try {
      User usr = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      if (usr.getPlayer() != null) {
        if (usr.getPlayer().isOnline()) {
          usr.getPlayer().kickPlayer("Your player file is being reset, please relog");
        }
        File file = new File("world/players/" + usr.getPlayer().getName() + ".dat");
        String msg = "Could not find" + usr.getPlayer().getName() + "'s player file";
        if (file.exists()) {
          file.delete();
          msg = "Reset " + usr.getPlayer().getName() + "'s player file";
        }
        if (user != null) {
          user.sendMessage(msg);
          msg = user.getName() + " " + msg;
        }
        ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.INFO, msg);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Console
  @Command(aliases = { "move" }, desc = "Sends a user over to a server", usage = "[username] [server]", anyFlags = true, min = 2, max = 2)
  @CommandPermissions("sessions.move")
  public static void move(CommandContext args, User user) throws CommandException {
    try {
      final User usr = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(args.getString(0));
      if (usr != null) {
        ForsakenGlobal.getInstance().sendPluginMessage("Connect", args.getString(1), usr.getPlayer());
        ConnectionListener._moveTaskIds.put(usr.getName().toLowerCase(),
            ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), new Runnable() {
              public void run() {
                Player p = ForsakenGlobal.getInstance().getServer().getPlayer(usr.getName());
                if (p != null && p.isOnline()) {
                  ForsakenGlobal.getInstance().getServer()
                      .dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "loadinventory " + usr.getName());
                }
              }
            }, 1 * 20).getTaskId());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Command that kicks all users from the game with a message
   * 
   * @param args
   * @param user
   * @throws CommandException
   */
  @Command(aliases = { "kickall" }, desc = "Kicks all users from the server with a message", usage = "<message...>", anyFlags = true, min = 0, max = -1)
  @CommandPermissions("api.kickall")
  public static void kickAll(CommandContext args, User user) throws CommandException {
    String msg = (args.argsLength() > 0 ? args.getJoinedStrings(0) : "You have been kicked by " + user.getFormattedName());
    try {
      String current = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getName();
      for (Player p : ForsakenGlobal.getInstance().getServer().getOnlinePlayers()) {
        if (p != null && !p.getName().toLowerCase().equalsIgnoreCase(user.getName().toLowerCase())) {
          if (current.equalsIgnoreCase("dw_main") || current.equalsIgnoreCase("dw_donor")) {
            User u = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(p.getName());
            ConnectionListener.saveInventory(u);
            ConnectionListener._savedInventories.add(u.getName().toLowerCase());
          }
          if (!ForsakenGlobal.getInstance().getServer().getOnlineMode() && !current.equalsIgnoreCase("lobby")) {
            p.sendMessage(MessageUtils.format("&l&c" + msg, true));
            try {
              ForsakenGlobal
                  .getInstance()
                  .sendPluginMessage(
                      "Connect",
                      (ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getName().equalsIgnoreCase("lobby") ? "main"
                          : "lobby"), p);
            } catch (Exception e) {
              ForsakenGlobal.getInstance().sendPluginMessage("Connect", "lobby", p);
            }
          } else {
            p.kickPlayer(msg);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    ForsakenGlobal.getInstance().getAPI().getMessageManager().log(Level.INFO, "Kicked all players from game");
  }

  @Command(aliases = { "reloadUserData" }, desc = "Kicks all users from the server with a message", usage = "<message...>", anyFlags = true, min = 0, max = -1)
  @Console
  @CommandPermissions("api.kickall")
  public static void reloadUser(CommandContext args, User user) throws CommandException {
    String name = args.getString(0);
    Player p = ForsakenGlobal.getInstance().getServer().getPlayer(name);
    p.loadData();
  }

  @Command(aliases = { "list", "who", "online", "users" }, desc = "Shows a list of all users", usage = "<server>", flags = "", min = 0, max = 1)
  @CommandPermissions("sessions.list")
  public static void sendMsg(CommandContext args, User user) throws CommandException {
    List<String> players = new ArrayList<String>();
    if (args.argsLength() == 0) {
      try {
        for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getOnlineAny()) {
          if (u.getActiveServer() != null) {
            players.add(u.getName());
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      try {
        for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getOnlineAny()) {
          if ((u.getActiveServer() != null) && u.getActiveServer().equalsIgnoreCase(args.getString(0))) {
            players.add(u.getName());
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (players.isEmpty()) {
      if (args.argsLength() >= 1) {
        String servers = args.getString(0);
        ForsakenGlobal.getInstance().getAPI().getMessageManager()
            .sendMessage(user.getPlayer(), "&eThere is no one on our &6" + servers + " &eserver");
      } else {
        ForsakenGlobal.getInstance().getAPI().getMessageManager().sendMessage(user.getPlayer(), "&eThere is no one online");
      }
    } else {
      Map<Integer, ArrayList<String>> formattedPlayers = new HashMap<Integer, ArrayList<String>>();
      for (String s : players) {
        User user2 = null;
        try {
          user2 = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(s);
        } catch (Exception e) {
          e.printStackTrace();
        }
        if (user2 != null) {

          if ((user2.getSecondaryRankId() > 1) && user2.getPrimaryRankId() <= 2) {
            if (formattedPlayers.get(user2.getSecondaryRankId()) == null) {
              formattedPlayers.put(user2.getSecondaryRankId(), new ArrayList<String>());
            }
            formattedPlayers.get(user2.getSecondaryRankId()).add(user2.getName());
          } else if (user2.getPrimaryRankId() > 0) {
            if (formattedPlayers.get(user2.getPrimaryRankId()) == null) {
              formattedPlayers.put(user2.getPrimaryRankId(), new ArrayList<String>());
            }
            formattedPlayers.get(user2.getPrimaryRankId()).add(user2.getName());
          }
        }
      }
      List<Rank> ranks = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getRankFactory().getAll();
      Collections.sort(ranks, new Comparator<Rank>() {
        public int compare(Rank o1, Rank o2) {
          if (o1.getOrder() == o2.getOrder()) return 0;
          return o1.getOrder() > o2.getOrder() ? -1 : 1;
        }
      });
      String msg = "&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-[  &6%server% Online ("
          + players.size()
          + ")  &7]-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&/";

      if ((args.argsLength() >= 1)) {
        msg = msg.replaceAll("%server%", args.getString(0));
      } else {
        msg = msg.replaceAll("%server%", "All");
      }
      for (Rank r : ranks) {
        if (formattedPlayers.get(r.getId()) != null) {
          msg += "&l&" + r.getMCColour() + r.getName() + "s&r: &" + r.getMCColour() + StringMgmt.join(formattedPlayers.get(r.getId()), "&7, &" + r.getMCColour()) + "&/";
        }
      }
      msg += "&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-";
      user.sendMessage(msg);
    }
  }
}
