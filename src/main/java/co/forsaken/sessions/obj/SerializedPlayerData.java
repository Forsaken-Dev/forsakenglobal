/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.sessions.obj;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.SerializationUtils;

public class SerializedPlayerData implements Serializable {

  private String  _username;
  private String  _playerNbt;
  private boolean _hasDataInSQL = false;

  public SerializedPlayerData(String name) throws IOException {
    _username = name;
    SerializedPlayerData data = loadDataFromSQL(name);
    if (data != null) {
      _hasDataInSQL = true;
      _username = data._username;
      _playerNbt = data._playerNbt;
    } else {
      _playerNbt = SerializationUtils.serialize(SerializationUtils.loadPlayerNbtFromFile(name));
    }
  }

  public void apply() throws IOException {
    SerializationUtils.savePlayerNbtToFile(_username, SerializationUtils.deserializeNbt(_playerNbt));
  }

  public void save() {
    try {
      String playerData = serialize(this);
      if (_hasDataInSQL) {
        ForsakenGlobal.getInstance().getAPI().getDatabase()
            .write("UPDATE forsaken_user_serialization SET serialized_player = ? WHERE user_name = ?;", playerData, _username);
      } else {
        ForsakenGlobal.getInstance().getAPI().getDatabase()
            .write("INSERT INTO forsaken_user_serialization (user_name, serialized_player) VALUES (?, ?);", _username, playerData);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static SerializedPlayerData loadDataFromSQL(String username) {
    HashMap<Integer, ArrayList<Object>> query = ForsakenGlobal.getInstance().getAPI().getDatabase()
        .readRaw("SELECT serialized_player FROM forsaken_user_serialization WHERE user_name = ?", username);
    if (query != null && query.size() > 0) {
      try {
        return deserialize(query.get(1).get(0) + "");
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  private static SerializedPlayerData deserialize(String s) throws IOException, ClassNotFoundException {
    byte[] data = Base64Coder.decode(s);
    ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
    Object o = ois.readObject();
    ois.close();
    return (SerializedPlayerData) o;
  }

  private static String serialize(SerializedPlayerData sp) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(baos);
    oos.writeObject(sp);
    oos.close();
    return new String(Base64Coder.encode(baos.toByteArray()));
  }
}
