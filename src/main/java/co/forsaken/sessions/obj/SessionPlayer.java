/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/**
 * 
 */
package co.forsaken.sessions.obj;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.SerializationUtils;
import co.forsaken.api.utils.db.DatabaseResults;

import com.comphenix.protocol.wrappers.nbt.NbtCompound;

/**
 * @author vanZeben
 */
public class SessionPlayer implements Serializable {

  private static final long        serialVersionUID = -5212769510466519915L;

  private int                      _userId;
  private HashMap<Integer, String> _inventory       = new HashMap<Integer, String>();
  private HashMap<String, String>  _attributes      = new HashMap<String, String>();

  public SessionPlayer(User user) {
    _userId = user.getId();
    PlayerInventory inv = user.getPlayer().getInventory();
    for (int i = 0; i < inv.getSize(); i++) {
      ItemStack item = inv.getItem(i);
      if (item != null) {
        try {
          _inventory.put(i, SerializationUtils.serialize(item));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    for (int i = 0; i < user.getPlayer().getInventory().getArmorContents().length; i++) {
      if (user.getPlayer().getInventory().getArmorContents()[i] != null) {
        try {
          _inventory.put(-1 - i, SerializationUtils.serialize(user.getPlayer().getInventory().getArmorContents()[i]));
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    _attributes.put("exp", user.getPlayer().getExp() + "");
    _attributes.put("exhaustion", user.getPlayer().getExhaustion() + "");
    _attributes.put("fire_ticks", user.getPlayer().getFireTicks() + "");
    _attributes.put("food_level", user.getPlayer().getFoodLevel() + "");
    _attributes.put("health", user.getPlayer().getHealth() + "");
    _attributes.put("health_scale", user.getPlayer().getHealthScale() + "");
    _attributes.put("level", user.getPlayer().getLevel() + "");
    _attributes.put("saturation", user.getPlayer().getSaturation() + "");

  }

  @SuppressWarnings("deprecation")
  public void apply(User user) {
    user.getPlayer().getInventory().clear();
    for (int i = 0; i < user.getPlayer().getInventory().getSize(); i++) {
      if (_inventory.containsKey(i)) {
        try {
          ItemStack item = SerializationUtils.deserialize(_inventory.get(i));
          user.getPlayer().getInventory().setItem(i, item);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    ItemStack[] armour = new ItemStack[user.getPlayer().getInventory().getArmorContents().length];
    for (int i = 0; i < user.getPlayer().getInventory().getArmorContents().length; i++) {
      if (_inventory.containsKey(-1 - i)) {
        try {
          ItemStack item = SerializationUtils.deserialize(_inventory.get(-1 - i));
          armour[i] = item;
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        armour[i] = null;
      }
    }
    user.getPlayer().getInventory().setArmorContents(armour);
    user.getPlayer().updateInventory();
    user.getPlayer().setExp(Float.parseFloat(_attributes.get("exp")));
    user.getPlayer().setExhaustion(Float.parseFloat(_attributes.get("exhaustion")));
    user.getPlayer().setFireTicks(Integer.parseInt(_attributes.get("fire_ticks")));
    user.getPlayer().setFoodLevel(Integer.parseInt(_attributes.get("food_level")));
    user.getPlayer().setHealth(Float.parseFloat(_attributes.get("health")) > 20.0 ? 20.0 : Float.parseFloat(_attributes.get("health")));
    user.getPlayer().setHealthScale(Float.parseFloat(_attributes.get("health_scale")));
    user.getPlayer().setLevel(Integer.parseInt(_attributes.get("level")));
    user.getPlayer().setSaturation(Float.parseFloat(_attributes.get("saturation")));
  }

  public static SessionPlayer getSessionPlayer(User user) {
    HashMap<Integer, ArrayList<Object>> query = ForsakenGlobal.getInstance().getAPI().getDatabase()
        .readRaw("SELECT session_player FROM forsaken_session_players WHERE user_id = ? ORDER BY date DESC LIMIT 1", user.getId());
    if (query != null && query.size() > 0) {
      try {
        return deserialize(query.get(1).get(0) + "");
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  private static SessionPlayer deserialize(String s) throws IOException, ClassNotFoundException {
    byte[] data = Base64Coder.decode(s);
    ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
    Object o = ois.readObject();
    ois.close();
    return (SessionPlayer) o;
  }

  private static String serialize(SessionPlayer sp) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(baos);
    oos.writeObject(sp);
    oos.close();
    return new String(Base64Coder.encode(baos.toByteArray()));
  }

  public void remove() {
    // ForsakenGlobal
    // .getInstance().getAPI().getDatabase().write("DELETE FROM forsaken_session_players WHERE user_id = ?;",
    // _userId);
  }

  public void insertIgnore() {
    try {
      String inv = serialize(this);
      ForsakenGlobal.getInstance().getAPI().getDatabase()
          .write("INSERT IGNORE INTO forsaken_session_players (user_id, session_player, date) VALUES (?, ?, NOW());", _userId, inv);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void insert() {
    try {
      String inv = serialize(this);
      DatabaseResults query = ForsakenGlobal.getInstance().getAPI().getDatabase()
          .readEnhanced("SELECT user_id FROM forsaken_session_players WHERE user_id = ?", _userId);
      if (query != null && query.hasRows()) {
        ForsakenGlobal.getInstance().getAPI().getDatabase()
            .write("UPDATE forsaken_session_players SET session_player = ? WHERE user_id = ?;", inv, _userId);
      } else {
        ForsakenGlobal.getInstance().getAPI().getDatabase()
            .write("INSERT INTO forsaken_session_players (user_id, session_player, date) VALUES (?, ?, NOW());", _userId, inv);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
