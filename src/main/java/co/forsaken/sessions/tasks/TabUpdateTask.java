/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSessions.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSessions is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSessions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSessions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/**
 * 
 */
package co.forsaken.sessions.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcsg.double0negative.tabapi.TabAPI;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Rank;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.MessageUtils;

/**
 * @author vanZeben
 */
public class TabUpdateTask implements Runnable {

  private ForsakenGlobal _plugin;
  private int            _taskId        = -1;
  private int            tabUpdateDelay = 10;

  public TabUpdateTask(ForsakenGlobal plugin) {
    _plugin = plugin;
    _taskId = plugin.getServer().getScheduler().runTaskTimer(_plugin, this, 10, tabUpdateDelay * 20).getTaskId();
  }

  public void run() {
    updateAll();
  }

  public static void updateAll() {
    try {
      for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
        updateTab(u);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void updateTab(User user) throws Exception {
    if (!ForsakenGlobal.isTabAPIEnabled()) { return; }
    TabAPI.clearTab(user.getPlayer());
    int numStaff = 0;
    int numOnline = 0;
    int currentOnline = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getAllOnline().size();
    int maxY = TabAPI.getVertSize();
    List<String> players = new ArrayList<String>();
    for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getOnlineAny()) {
      players.add(u.getName());
    }

    Map<Integer, ArrayList<String>> formattedPlayers = new HashMap<Integer, ArrayList<String>>();
    for (String s : players) {
      User user2 = null;
      try {
        user2 = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(s);
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (user2 != null) {
        if (user2.getPrimaryRank().isStaff()) {
          numStaff++;
        }
        numOnline++;
        if ((user2.getSecondaryRankId() > 1) && user2.getPrimaryRankId() <= 2) {
          if (formattedPlayers.get(user2.getSecondaryRankId()) == null) {
            formattedPlayers.put(user2.getSecondaryRankId(), new ArrayList<String>());
          }
          formattedPlayers.get(user2.getSecondaryRankId()).add(user2.getName());
        } else if (user2.getPrimaryRankId() > 0) {
          if (formattedPlayers.get(user2.getPrimaryRankId()) == null) {
            formattedPlayers.put(user2.getPrimaryRankId(), new ArrayList<String>());
          }
          formattedPlayers.get(user2.getPrimaryRankId()).add(user2.getName());
        }
      }
    }
    List<Rank> ranks = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getRankFactory().getAll();
    Collections.sort(ranks, new Comparator<Rank>() {
      public int compare(Rank o1, Rank o2) {
        if (o1.getOrder() == o2.getOrder()) return 0;
        return o1.getOrder() > o2.getOrder() ? -1 : 1;
      }
    });

    int staffY = 1;
    int allY = 1;
    int currentY = 1;
    String[][] msgs = new String[TabAPI.getHorizSize()][TabAPI.getVertSize()];
    msgs[0][0] = "&l&a-Staff-";
    msgs[1][0] = "&l&b-Global-";
    msgs[2][0] = "&l&e-Current-";
    Ranks: for (Rank r : ranks) {
      if (formattedPlayers.get(r.getId()) != null) {
        if (r.isStaff()) {
          for (String s : formattedPlayers.get(r.getId())) {
            if (staffY > msgs[0].length - 1) {
              msgs[0][staffY - 1] = "&7+" + (numStaff - 17) + " more";
              continue Ranks;
            }
            msgs[0][staffY] = "&" + r.getMCColour() + s;
            if (msgs[0][staffY].length() > 16) {
              msgs[0][staffY] = msgs[0][staffY].substring(0, 16);
            }
            staffY++;
          }
        } else {
          for (String s : formattedPlayers.get(r.getId())) {
            if (allY > msgs[0].length - 1) {
              msgs[1][allY - 1] = "&7+" + (numOnline - 17) + " more";
              continue Ranks;
            }
            msgs[1][allY] = "&" + r.getMCColour() + s;
            if (msgs[1][allY].length() > 16) {
              msgs[1][allY] = msgs[1][allY].substring(0, 16);
            }
            allY++;
          }
        }
      }
    }

    for (User u : ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
      if (currentY > msgs[0].length - 1) {
        msgs[2][allY - 1] = "&7+" + (currentOnline - 17) + " more";
        break;
      }
      msgs[2][currentY] = " &" + (u.getPrimaryRankId() <= 1 ? (u.getSecondaryRankId() >= 9 ? u.getSecondaryRank().getMCColour() : "F") : u.getPrimaryRank().getMCColour()) + u.getName();
      if (msgs[2][currentY].length() > 16) {
        msgs[2][currentY] = msgs[2][currentY].substring(0, 16);
      }
      currentY++;
    }

    for (int x = 0; x < msgs.length; x++) {
      for (int y = 0; y < msgs[x].length; y++) {
        String msg = "" + TabAPI.nextNull();
        int ping = -1;
        if (msgs[x][y] != null && !msgs[x][y].isEmpty()) {
          msg = MessageUtils.cleanMsgEnding(MessageUtils.format(msgs[x][y], true) + TabAPI.nextNull());
          ping = 0;
        }
        if (msg.length() > 16) {
          msg = msg.substring(0, 16);
        }
        TabAPI.setTabString(ForsakenGlobal.getInstance(), user.getPlayer(), y, x, msg, ping);

      }
    }
    TabAPI.updatePlayer(user.getPlayer());
  }
}
