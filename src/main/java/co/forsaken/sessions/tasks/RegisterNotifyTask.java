/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSessions.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSessions is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSessions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSessions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.sessions.tasks;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;

public class RegisterNotifyTask implements Runnable {

  private ForsakenGlobal _plugin;

  public RegisterNotifyTask(ForsakenGlobal plugin) {
    plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, this, 0, 20 * 60 * 5);
    _plugin = plugin;
  }

  public void run() {
    try {
      for (User u : _plugin.getAPI().getFactoryManager().getUserFactory().getAllOnline()) {
        if (u.getPrimaryRankId() == 1 && u.shouldReceiveLogMessages()) {
          u.sendMessage("&8[&l&c!&8] &l&eRegister with &6/register &eto get access to teleport, homes, more vote tokens, and better support through our forums!");
          u.sendMessage("&8-   &7Toggle these messages with /debug");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
