/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSessions.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSessions is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSessions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSessions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.sessions.tasks;

import java.text.DecimalFormat;
import java.util.Date;

import net.minecraft.server.v1_6_R3.MinecraftServer;

import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;

public class ServerSessionUpdater implements Runnable {

  private ForsakenGlobal _plugin;
  private double         _lastTPS     = 0.0;
  private Date           _lastRunDate = new Date();

  public ServerSessionUpdater(ForsakenGlobal plugin) {
    plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, this, 0, 10 * 20);
    _plugin = plugin;
  }

  public void run() {
    try {
      _plugin
          .getAPI()
          .getDatabase()
          .writeNoError("INSERT INTO forsaken_server_updates (server_id, tps, player_count) VALUES (?,?,?);",
              _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getId(), MinecraftServer.currentTPS,
              _plugin.getServer().getOnlinePlayers().length);
      if (_lastTPS - 4D > MinecraftServer.currentTPS && MinecraftServer.currentTick > 60 * 20 && MinecraftServer.currentTPS <= 17D) {
        String msg = "";
        for (Player p : _plugin.getServer().getOnlinePlayers()) {
          try {
            User user = _plugin.getAPI().getFactoryManager().getUserFactory().get(p.getName());
            if (user.getLastLogin().after(_lastRunDate)) {
              if (!msg.isEmpty()) {
                msg += ", ";
              }
              msg += user.getName();
            }
          } catch (Exception e) {
          }
        }
        msg = "The server TPS has dropped from " + new DecimalFormat("#.##").format(_lastTPS) + " to "
            + new DecimalFormat("#.##").format(MinecraftServer.currentTPS) + " on "
            + _plugin.getAPI().getFactoryManager().getServerFactory().getCurrent().getName() + " in the last 10 seconds. "
            + (msg.isEmpty() ? "" : "Players who have logged in are: " + msg);
        _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "s " + msg);
      }
    } catch (Exception e1) {
    }
    _lastTPS = MinecraftServer.currentTPS;
    _lastRunDate = new Date();
  }
}
