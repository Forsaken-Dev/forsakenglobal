/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/*
 * This file is part of ForsakenSessions.
 *
 * Copyright © 2012-2013, ForsakenNetwork LLC <http://www.forsaken.com/>
 * ForsakenSessions is licensed under the Forsaken Network License Version 1
 *
 * ForsakenSessions is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenSessions is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
/**
 * 
 */
package co.forsaken.sessions.tasks;

import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.sessions.obj.SessionPlayer;

/**
 * @author vanZeben
 */
public class InventoryLoader implements Runnable {

  private ForsakenGlobal _plugin;
  private int            _taskId        = -1;
  private String         _username;
  private SessionPlayer  _initialInv    = null;
  private int            _numChecks     = 0;
  private String         _loggingPrefix = "";
  public boolean         _cancelled     = false;
  private User           user           = null;

  public InventoryLoader(ForsakenGlobal plugin, String username) {
    _plugin = plugin;
    _username = username;
    _loggingPrefix = "[INVENTORY_" + _username + "] ";
    _taskId = plugin.getServer().getScheduler().runTaskTimer(_plugin, this, 0, 20).getTaskId();
  }

  public void run() {
    if (_cancelled && _taskId != -1) {
      _plugin.getServer().getScheduler().cancelTask(_taskId);
      return;
    }
    if (user == null) {
      try {
        user = _plugin.getAPI().getFactoryManager().getUserFactory().get(_username);
      } catch (Exception e) {
      }
    }
    if (user == null || (user != null && (user.getPlayer() == null || (user.getPlayer() != null && !user.getPlayer().isOnline())))) {
      _cancelled = true;
      _plugin.getAPI().getMessageManager().log(Level.WARNING, _loggingPrefix + "User not found, cancelling load");
      if (_initialInv != null) {
        _initialInv.insertIgnore();
        _plugin.getAPI().getMessageManager().log(Level.WARNING, _loggingPrefix + "Inserting cached inventory");
      }
      return;
    }
    if (_initialInv == null) {
      _initialInv = new SessionPlayer(user);
      user.getPlayer().getInventory().clear();
      user.getPlayer().getInventory().setArmorContents(new ItemStack[] { null, null, null, null });
      user.getPlayer().updateInventory();
      _plugin.getAPI().getMessageManager().log(Level.WARNING, _loggingPrefix + "Initial Inventory Cached");
      return;
    }
    _numChecks++;

    _plugin.getAPI().getMessageManager().log(Level.WARNING, _loggingPrefix + "Checking inventory [" + _numChecks + "]");
    SessionPlayer inv = SessionPlayer.getSessionPlayer(user);
    if (inv == null) {
      if (_numChecks <= 5) {
        _plugin.getAPI().getMessageManager()
            .log(Level.WARNING, _loggingPrefix + "Failed to retrieve inventory, checking again [" + _numChecks + "]");
      } else {
        _plugin.getAPI().getMessageManager()
            .log(Level.WARNING, _loggingPrefix + "Could not find a saved inventory restoring cached");
        _initialInv.apply(user);
        _cancelled = true;
      }
    } else {
      inv.apply(user);
      inv.remove();
      _plugin.getAPI().getMessageManager().log(Level.WARNING, _loggingPrefix + "Loaded inventory [" + _numChecks + "]");
      _cancelled = true;
    }

  }
}
