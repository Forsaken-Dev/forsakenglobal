/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import ru.tehkode.permissions.bukkit.PermissionsEx;
import co.forsaken.announcements.AnnouncementTask;
import co.forsaken.api.APIListener;
import co.forsaken.api.APIRegistrar;
import co.forsaken.api.Registrar;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.irc.IRCBotWrapper;
import co.forsaken.api.manager.APIManager;
import co.forsaken.api.manager.FactoryManager;
import co.forsaken.api.manager.ItemMenuManager;
import co.forsaken.api.manager.MessageManager.Level;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.db.DatabaseConnection;
import co.forsaken.bans.cmds.BanCommands;
import co.forsaken.bans.events.BanEvents;
import co.forsaken.chat.cmds.ChatRegistrars;
import co.forsaken.chat.events.ChatEvents;
import co.forsaken.donate.DonateEvents;
import co.forsaken.donate.DonateRegistrar;
import co.forsaken.money.MoneyGiveTask;
import co.forsaken.money.MoneyRegistrar;
import co.forsaken.registration.cmds.RegistrationRegistrar;
import co.forsaken.server.ServerListener;
import co.forsaken.server.ServerMoveRegistrar;
import co.forsaken.server.lobby.LobbyRegistrar;
import co.forsaken.server.lobby.WelcomeBarText;
import co.forsaken.sessions.cmds.SessionCommands;
import co.forsaken.sessions.events.ConnectionListener;
import co.forsaken.sessions.tasks.RegisterNotifyTask;
import co.forsaken.sessions.tasks.ServerSessionUpdater;
import co.forsaken.sessions.tasks.SessionUpdater;
import co.forsaken.sessions.tasks.TabUpdateTask;
import co.forsaken.teleport.TeleportEvents;
import co.forsaken.teleport.cmds.HomeCommands;
import co.forsaken.teleport.cmds.PeerTeleportCommands;
import co.forsaken.teleport.cmds.RequestCommands;
import co.forsaken.teleport.cmds.ServerCommands;
import co.forsaken.teleport.cmds.WarpCommands;
import co.forsaken.vote.VoteRegistrar;
import co.forsaken.vote.VoteRemindTask;
import co.forsaken.vote.VoteUtils;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.WrappedCommandException;

public class ForsakenGlobal extends JavaPlugin implements PluginMessageListener {

  private static IRCBotWrapper _ircBot = null;

  public static boolean isMystcraftEnabled() {
    return _mystcraftEnabled;
  }

  public static ForsakenGlobal getInstance() {
    return _instance;
  }

  private static boolean _tabAPIEnabled = false;

  public static boolean isTabAPIEnabled() {
    return _tabAPIEnabled;
  }

  public static ForsakenGlobal _instance;
  private DatabaseConnection   _databaseConnection;
  private FactoryManager       _factoryManager;
  public static boolean        _loadedProperly   = false;
  private static boolean       _mystcraftEnabled = false;
  private static long          lastButcherTime   = 0;
  public static final int      BUTCHER_TIME      = 15;
  protected APIManager         _apiManager;

  private Registrar            _registrar;
  private static PermissionsEx _permissions;
  public static boolean        _GPEnabled        = false;

  public static IRCBotWrapper getBaseIRCBot() {
    if (_ircBot == null) {
      _ircBot = new IRCBotWrapper(ForsakenGlobal.getInstance());
    }
    return _ircBot;
  }

  private void checkMystcraft() {
    File file = new File("mods/");
    if ((file != null) && file.isDirectory()) {
      for (File f : file.listFiles()) {
        if (f.getName().contains("myst")) {
          _mystcraftEnabled = true;
        }
      }
    }
  }

  public FactoryManager getFactoryManager() {
    if (_factoryManager == null) {
      _factoryManager = new FactoryManager(this);
    }
    return _factoryManager;
  }

  public Registrar getRegistrar() {
    if (_registrar == null) {
      _registrar = new Registrar(this);
    }
    return _registrar;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
    try {
      _registrar.getCommands().execute(cmd.getName(), args, sender, getAPI().getFactoryManager().getUserFactory().get(sender.getName()));
    } catch (CommandPermissionsException e) {
      getAPI().getMessageManager().sendMessage(sender, "&eYou don't have permission.");
    } catch (MissingNestedCommandException e) {
      getAPI().getMessageManager().sendMessage(sender, "&e" + e.getUsage());
    } catch (CommandUsageException e) {
      getAPI().getMessageManager().sendMessage(sender, "&e" + e.getMessage());
      getAPI().getMessageManager().sendMessage(sender, "&e" + e.getUsage());
    } catch (WrappedCommandException e) {
      if (e.getCause() instanceof NumberFormatException) {
        getAPI().getMessageManager().sendMessage(sender, "&eNumber expected, string received instead.");
      } else {
        getAPI().getMessageManager().sendMessage(sender, "&cAn error has occurred. See console.");
        e.printStackTrace();
      }
    } catch (CommandException e) {
      getAPI().getMessageManager().sendMessage(sender, "&e" + e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return true;
  }

  public APIManager getAPI() {
    if (_apiManager == null) {
      _apiManager = new APIManager(this);
    }
    return _apiManager;
  }

  public DatabaseConnection getDatabaseConnection() {
    if (_databaseConnection == null) {
      _databaseConnection = new DatabaseConnection(this, getAPI().getConfigManager().getString("mysql.server.address") + ":"
          + getAPI().getConfigManager().getInt("mysql.server.port"), getAPI().getConfigManager().getString("mysql.database.name"), getAPI()
          .getConfigManager().getString("mysql.database.username"), getAPI().getConfigManager().getString("mysql.database.password"));
    }
    return _databaseConnection;
  }

  public void loadConfig() {
    getAPI().getConfigManager().addDefault("mysql.server.address", "sql.forsaken.co");
    getAPI().getConfigManager().addDefault("mysql.server.port", 3306);
    getAPI().getConfigManager().addDefault("mysql.database.name", "minecraft");
    getAPI().getConfigManager().addDefault("mysql.database.username", "root");
    getAPI().getConfigManager().addDefault("mysql.database.password", "root");
    getAPI().getConfigManager().addDefault("mysql.table.prefix", "forsaken_");
    getAPI().getConfigManager().addDefault("mysql.debug", true);

    getAPI().getConfigManager().addDefault("irc.server.ip", "irc.forsaken.co");
    getAPI().getConfigManager().addDefault("irc.server.port", 6667);
    getAPI().getConfigManager().addDefault("irc.server.password", "");
    getAPI().getConfigManager().addDefault("irc.server.use_ssl", false);
    getAPI().getConfigManager().addDefault("irc.server.num_reconnects", 5);

    getAPI().getConfigManager().addDefault("api.user.teleport_damage_delay", 7);
    getAPI().getConfigManager().addDefault("website.url", "http://forum.forsaken.co");
    getAPI().getConfigManager().addDefault("website.api.hash", "API_HASH");
    getAPI().getConfigManager().addDefault("server.messages.prefix", "&ct&l&4F&r&cn");
    getAPI().getConfigManager().addDefault("server.restarting_message", "Server is restarting, please reconnect in a few minutes");

    getAPI().getConfigManager().addDefault("vote.reminder.enabled", true);
    getAPI().getConfigManager().addDefault("vote.reminder.seconds", Integer.valueOf(10 * 60));
    getAPI().getConfigManager().addDefault("teleport.cost", 75);
  }

  @Override
  public void onDisable() {
    super.onEnable();
    getAPI().getFactoryManager();
    if (_ircBot != null) {
      _ircBot.quit();
    }
    for (Player p : getServer().getOnlinePlayers()) {
      p.kickPlayer(MessageUtils.format(getAPI().getConfigManager().getString("server.restarting_message"), true));
    }
  }

  public void forwardPluginMessage(String server, String channel, String message) {
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    try {
      out.writeUTF("Forward");
      out.writeUTF(server);
      out.writeUTF(channel);

      ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
      DataOutputStream msgout = new DataOutputStream(msgbytes);
      msgout.writeUTF(message);

      out.writeShort(msgbytes.toByteArray().length);
      out.write(msgbytes.toByteArray());
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.getServer().sendPluginMessage(this, "BungeeCord", b.toByteArray());
  }

  public void sendPluginMessage(String channel, String arg, Player player) {
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    try {
      out.writeUTF(channel);
      if ((arg != null) && !arg.isEmpty()) {
        out.writeUTF(arg);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (player != null) {
      player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
    } else {
      this.getServer().sendPluginMessage(this, "BungeeCord", b.toByteArray());
    }
  }

  public void onPluginMessageReceived(String channel, Player player, byte[] message) {
    try {
      if (!channel.equals("BungeeCord")) return;
      DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
      String subChannel = in.readUTF();
      if (subChannel.equalsIgnoreCase("IP")) {
        String ip = in.readUTF();
        in.readInt();
        ConnectionListener.addIP(player.getName(), ip);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NumberFormatException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static PermissionsEx getPermissions() {
    return _permissions;
  }

  private AnnouncementTask _announcementTask;

  @Override
  public void onEnable() {
    _instance = this;
    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);

    loadConfig();
    getAPI().getConfigManager().saveConfig();
    registerEvents();
    registerCommands();
    _tabAPIEnabled = getServer().getPluginManager().getPlugin("TabAPI") != null;
    _permissions = (PermissionsEx) getServer().getPluginManager().getPlugin("PermissionsEx");

    _GPEnabled = getServer().getPluginManager().getPlugin("GriefPrevention") != null;

    if (_tabAPIEnabled) {
      new TabUpdateTask(this);
    }
    checkMystcraft();
    getAPI().getFactoryManager();
    getAPI().getMessageManager().log(Level.INFO, "Mystcraft has " + (isMystcraftEnabled() ? "" : "not ") + "been detected");
    getAPI().getDatabase().setShowQueries(getAPI().getConfigManager().getBoolean("mysql.debug"));
    getBaseIRCBot();
    if (!getServer().getMotd().contains("lobby")) {
      lastButcherTime = System.currentTimeMillis();
      getServer().getScheduler().runTaskTimer(this, new Runnable() {
        public void run() {
          if (System.currentTimeMillis() - lastButcherTime >= (BUTCHER_TIME * 60 * 1000)) {
            getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall");
            getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall item");
            if (!getInstance().getServer().getMotd().contains("bnb") && !getInstance().getServer().getMotd().contains("as")) {
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall sheep");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall pig");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall chicken");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall squid");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall slime");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall cow");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall ocelot");
              getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "cofh killall mushroomcow");
            }
            getInstance().getAPI().getMessageManager()
                .sendGlobalMessage("&eAll monsters/animals and item drops have been removed, to free up entity count...", true, true);
            lastButcherTime = System.currentTimeMillis();
          } else if (System.currentTimeMillis() - lastButcherTime >= ((BUTCHER_TIME - 1) * 60 * 1000)) {
            getInstance().getAPI().getMessageManager().sendGlobalMessage("&eRemoving monsters/animals and item drops in &61 minute&e!", true, true);
          }

        }
      }, (BUTCHER_TIME - 1) * 60 * 20, 60 * 20);
    } else if (getServer().getMotd().equalsIgnoreCase("as_main")) {
      getServer().getScheduler().runTaskTimer(this, new Runnable() {
        public void run() {
          Set<OfflinePlayer> ops = getInstance().getServer().getBannedPlayers();
          for (OfflinePlayer op : ops) {
            try {
              User user = getInstance().getAPI().getFactoryManager().getUserFactory().get(op.getName());
              if (user.getSecondaryRank().getDonationRequired() > 0.0F || user.getPrimaryRank().isStaff()) {
                getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "pardon " + op.getName());
                getInstance().getServer().dispatchCommand(getInstance().getServer().getConsoleSender(), "dev delete " + op.getName());
              }
            } catch (Exception e) {
            }
          }
        }
      }, 20, 60 * 20);
    }
    if (getAPI().getConfigManager().getBoolean("vote.reminder.enabled")) {
      new VoteRemindTask(this);
    }
    new MoneyGiveTask(this);
    if (!getServer().getMotd().equals("lobby")) {
      new ServerSessionUpdater(this);
    } else {
      new WelcomeBarText(this);
    }
    _announcementTask = new AnnouncementTask(this);
    new SessionUpdater(this);
    new RegisterNotifyTask(this);
    getServer().getScheduler().runTaskLater(this, new Runnable() {
      public void run() {
        if (getServer().getMotd().equals("as_main")) {
          if (getServer().getWorld("skyworld") == null) {
            getServer().createWorld(new WorldCreator("skyworld"));
          }
          getServer().getWorld("skyworld").loadChunk(0, 0);
        }
      }
    }, 0L);
    if (getServer().getMotd().equalsIgnoreCase("lobby") && !VoteUtils.hasAddedTopVoters()) {
      VoteUtils.upgradeTop(2);
    }
  }

  public AnnouncementTask getAnnouncementTask() {
    return _announcementTask;
  }

  protected void registerEvents() {
    new ConnectionListener(this);
    new APIListener(this);
    new ChatEvents(this);
    new DonateEvents(this);
    new ItemMenuManager(this);
    new VoteRegistrar(this);
    if (getServer().getMotd().equalsIgnoreCase("lobby")) {
      new LobbyRegistrar(this);
    } else if (getServer().getMotd().contains("dw")) {
      new ServerListener(this);
    }
    new TeleportEvents(this);
    new BanEvents(this);
  }

  protected void registerCommands() {
    getRegistrar().register(APIRegistrar.class);
    getRegistrar().register(SessionCommands.class);
    getRegistrar().register(ChatRegistrars.class);
    getRegistrar().register(DonateRegistrar.class);
    getRegistrar().register(MoneyRegistrar.class);
    getRegistrar().register(RegistrationRegistrar.class);
    getRegistrar().register(VoteRegistrar.class);
    if (getServer().getMotd().equalsIgnoreCase("lobby")) {
      getRegistrar().register(LobbyRegistrar.class);
    }
    getRegistrar().register(ServerMoveRegistrar.class);
    getRegistrar().register(HomeCommands.class);
    getRegistrar().register(PeerTeleportCommands.class);
    getRegistrar().register(RequestCommands.class);
    getRegistrar().register(ServerCommands.class);
    getRegistrar().register(WarpCommands.class);
    getRegistrar().register(BanCommands.class);
  }
}
