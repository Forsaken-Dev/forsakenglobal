/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.server;

import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.utils.MessageUtils;

public class ServerListener extends BaseListener {

  public ServerListener(ForsakenGlobal plugin) {
    super(plugin);
  }

  @EventHandler
  public void onBlockPlace(BlockPlaceEvent e) {
    if (e.getBlock().getTypeId() == 1503 || e.getBlock().getTypeId() == 2530 || e.getBlock().getTypeId() == 1500 || e.getBlock().getTypeId() == 1227 || e.getBlock().getTypeId() == 1228 || e.getBlock().getTypeId() == 1230) {
      if (!e.getPlayer().getWorld().getName().equalsIgnoreCase("DIM_MYST2")) {
        e.setBuild(false);
        e.setCancelled(true);
        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ANVIL_LAND, 1, 0);
        MessageUtils.send(e.getPlayer(), "&8[&l&c!&e&8] &eSorry, this item can only be used in &6/mine");
      }
    }
  }
}
