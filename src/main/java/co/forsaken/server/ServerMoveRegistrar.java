/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.server;

import java.sql.SQLDataException;

import org.bukkit.Location;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.WorldEditUtils;
import co.forsaken.api.utils.db.DatabaseResults;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class ServerMoveRegistrar {

  @Command(aliases = { "savemyhouse", "smh" }, desc = "Saves your house around you", usage = "[radius]", anyFlags = true, min = 1, max = 2)
  @CommandPermissions("savemyhouse")
  public static void saveMyHouse(CommandContext args, User user) throws CommandException {
    if (!ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("dw_old_main") && !ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("dw_old_donor")) {
      user.sendMessage("&eYou can only transfer your house from the old DW Main and DW Donor servers");
      return;
    }
    DatabaseResults query = getSavedHouseData(user);
    if (query != null && query.hasRows()) {
      user.sendMessage("&eAt this time you can only transfer one chunk of land from the old servers to the new ones, and you have already trasnferred yours");
      return;
    }

    if (args.argsLength() < 2) {
      user.sendMessage("&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-[  &6House  &7]-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-&8-&7-");
      user.sendMessage("&7Make sure you are standing in the &6center &7of the area you want to move");
      user.sendMessage("&7You can define a &6radius &7to take with you. Maximum size is a 41x41x41 (20 radius)");
      user.sendMessage("&cPlease note that some blocks &6may not transfer &c(Any sort of multipart or cables),");
      user.sendMessage("&8-  &cso if you want to keep them, we suggest picking them up beforehand");
      user.sendMessage("&n&7You only get to save and transfer one region at this time, so make it count!");
      user.sendMessage("&aWhen you are ready to get going, type &6/saveMyHouse <radius> confirm");
    } else if (args.getString(1).equalsIgnoreCase("confirm")) {
      int radius = args.getInteger(0);
      if (radius > 20) {
        user.sendMessage("&eThe maximum radius you can define is &620 blocks&e");
        return;
      }

      String schematicName = "servermove-" + user.getName().toLowerCase();
      Location[] region = { user.getPlayer().getLocation().subtract(radius, radius, radius), user
          .getPlayer().getLocation().add(radius, radius, radius) };
      Location origin = user.getPlayer().getLocation();
      if (radius % 2 == 0) {
        origin = origin.add(1, 0, 0);
      }
      try {
        ForsakenGlobal
            .getInstance()
            .getAPI()
            .getDatabase()
            .write("INSERT INTO forsaken_base_moves (user_id, initial_server_id, initial_world, initial_x_coord, initial_y_coord, initial_z_coord, radius) VALUES(?,?,?,?,?,?,?);", user.getId(),
                ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId(),
                user.getPlayer().getWorld().getName(), user.getPlayer().getLocation().getBlockX(), user.getPlayer().getLocation().getBlockY(), user.getPlayer().getLocation().getBlockZ(), radius);

        WorldEditUtils.saveSchematicFromCoords(schematicName, user.getPlayer().getWorld().getName(), origin, region[0], region[1]);
        WorldEditUtils.setAreaWithBlock(user.getPlayer().getWorld().getName(),
            region[0], region[1], "0");
        ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "cofh killall item");
        user.sendMessage("&eSaved your house, feel free to login to the new server and &6/loadMyHouse");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Command(aliases = { "loadmyhouse", "lmh" }, desc = "Loads your house", usage = "", anyFlags = true, min = 0, max = -1)
  @CommandPermissions("savemyhouse")
  public static void loadMyHose(CommandContext args, User user) throws CommandException {
    if (!ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("dw_main") && !ForsakenGlobal.getInstance().getServer().getMotd().equalsIgnoreCase("dw_donor")) {
      user.sendMessage("&eYou can only transfer your house to the DW Main and DW Donor servers");
      return;
    }
    DatabaseResults query = getSavedHouseData(user);
    if (query == null || (query != null && !query.hasRows())) {
      user.sendMessage("&eYou have not yet saved your house, load it up with &6/saveMyHouse");
      return;
    }
    try {
      if (query != null && query.hasRows() && query.getInteger(0, "final_server_id") != -1) {
        user.sendMessage("&eYou cannot paste the same house twice, talk to an admin if you need help");
        return;
      }
    } catch (SQLDataException e1) {
      e1.printStackTrace();
    }
    String schematicName = "servermove-" + user.getName().toLowerCase();
    WorldEditUtils.pasteSchematicAtLocation(user.getPlayer().getWorld().getName(), schematicName, user.getPlayer().getLocation());
    user.sendMessage("&ePasted your house around you");
    try {
      ForsakenGlobal
          .getInstance()
          .getAPI()
          .getDatabase()
          .write("UPDATE forsaken_base_moves SET final_server_id = ?, final_world = ?, final_x_coord = ?, final_y_coord = ?, final_z_coord = ? WHERE id = ?", ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().getCurrent().getId(),
              user.getPlayer().getWorld().getName(), user.getPlayer().getLocation().getBlockX(), user.getPlayer().getLocation().getBlockY(), user.getPlayer().getLocation().getBlockZ(), query.getInteger(0, "id"));
    } catch (SQLDataException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static DatabaseResults getSavedHouseData(User user) {
    DatabaseResults query = ForsakenGlobal.getInstance().getAPI().getDatabase().readEnhanced("SELECT * FROM forsaken_base_moves WHERE user_id = ?", user.getId());
    return query;
  }
}
