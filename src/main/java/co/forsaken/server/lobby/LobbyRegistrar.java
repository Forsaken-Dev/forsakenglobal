/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.server.lobby;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.BaseListener;
import co.forsaken.api.factories.obj.Server;
import co.forsaken.api.factories.obj.User;
import co.forsaken.api.utils.FireworkUtils;
import co.forsaken.api.utils.ItemUtils;
import co.forsaken.api.utils.MessageUtils;
import co.forsaken.api.utils.WorldEditUtils;
import co.forsaken.api.utils.db.DatabaseResults;

public class LobbyRegistrar extends BaseListener {
  private ItemStack                      _showPlayersItem;
  private ItemStack                      _hidePlayersItem;
  private List<String>                   _playersHiding   = new ArrayList<String>();
  private static Map<String, Location[]> _serverLocations = new HashMap<String, Location[]>();
  private static Map<String, Boolean>    _onlineStatus    = new HashMap<String, Boolean>();

  static {
    initLocations();
  }

  public LobbyRegistrar(ForsakenGlobal plugin) {
    super(plugin);
    ItemStack item = new ItemStack(Material.COMPASS);
    ArrayList<String> lore = new ArrayList<String>();
    lore.add(MessageUtils.format("&o&dClick to open the server list", true));
    items.add(ItemUtils.getItemName(item, MessageUtils.format("&l&5Server Teleporter &r&o&f(Click With Me)", true), lore));
    lore.clear();
    _showPlayersItem = ItemUtils.getItemName(new ItemStack(Material.REDSTONE_TORCH_OFF), MessageUtils.format("&l&aShow Players &r&o&f(Click with me)", true), lore);
    _hidePlayersItem = ItemUtils.getItemName(new ItemStack(Material.REDSTONE_TORCH_ON), MessageUtils.format("&l&cHide Players &r&o&f(Click with me)", true), lore);
    items.add(_hidePlayersItem);
    plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
      public void run() {
        initNPC();
      }
    }, 5 * 20L);
  }

  private ArrayList<ItemStack> items = new ArrayList<ItemStack>();

  @EventHandler()
  public void onPlayerJoin(PlayerJoinEvent e) {
    try {
      init(e.getPlayer());
    } catch (Exception e2) {
      e2.printStackTrace();
    }
    final Player player = e.getPlayer();
    ForsakenGlobal.getInstance().getServer().getScheduler().runTaskLater(ForsakenGlobal.getInstance(), new Runnable() {
      public void run() {
        try {
          FireworkUtils.playRandomFirework(player.getWorld(), new Location(player.getWorld(), -125D, 110D, -78D));
          ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "play " + player.getName() + " lobbyMusic silent");
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    }, 40L);
    updateHidden();
  }

  private void init(Player p) throws Exception {
    p.getInventory().clear();
    User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(p.getName());
    if (!user.isBanned()) {
      ItemUtils.giveItemToPlayer(p, items);
    }
    p.setCompassTarget(new Location(_plugin.getServer().getWorld("world"), -125, 103, -136));
    p.teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
    p.setFlying(false);
    p.setGameMode(GameMode.SURVIVAL);
  }

  private void updateHidden() {
    for (Player p : _plugin.getServer().getOnlinePlayers()) {
      updateHidden(p);
    }
  }

  private void updateHidden(Player p) {
    if (_playersHiding.contains(p.getName().toLowerCase())) {
      for (Player p1 : _plugin.getServer().getOnlinePlayers()) {
        p.hidePlayer(p1);
      }
    } else {
      for (Player p1 : _plugin.getServer().getOnlinePlayers()) {
        p.showPlayer(p1);
      }
    }
  }

  @EventHandler()
  public void onBlockPlayer(BlockPlaceEvent e) {
    if (e.getBlock().getType() == _hidePlayersItem.getType() || e.getBlock().getType() == _showPlayersItem.getType() && !e.getPlayer().isOp()) {
      e.setCancelled(true);
      e.setBuild(false);
    }
  }

  @EventHandler()
  public void onPlayerInteract(PlayerInteractEvent e) {
    if (e.getPlayer().getItemInHand().getType() == Material.COMPASS) {
      _plugin.getAPI().getItemMenuManager().addMenu(new ServerMenu(_plugin, e.getPlayer().getName()));
    } else if (e.getPlayer().getItemInHand().getType() == _hidePlayersItem.getType()) {
      if (!_playersHiding.contains(e.getPlayer().getName().toLowerCase())) {
        _playersHiding.add(e.getPlayer().getName().toLowerCase());
        updateHidden(e.getPlayer());
      }
      e.getPlayer().getInventory().setItem(e.getPlayer().getInventory().first(_hidePlayersItem), _showPlayersItem);
    } else if (e.getPlayer().getItemInHand().getType() == _showPlayersItem.getType()) {
      if (_playersHiding.contains(e.getPlayer().getName().toLowerCase())) {
        _playersHiding.remove(e.getPlayer().getName().toLowerCase());
        updateHidden(e.getPlayer());
      }
      e.getPlayer().getInventory().setItem(e.getPlayer().getInventory().first(_showPlayersItem), _hidePlayersItem);
    }
  }

  @EventHandler()
  public void onPlayerDeath(PlayerDeathEvent e) {
    if (e.getEntity().getKiller() instanceof Player) {
      User killer = null;
      try {
        killer = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(e.getEntity().getKiller().getName());
      } catch (Exception e1) {
        e1.printStackTrace();
      }
      if (killer == null) { return; }
      killer.forceAddCredits(10, "player_kill");
      if (killer.shouldReceiveLogMessages()) {
        killer.sendMessage("&6+10 Credits &efor killing " + e.getEntity().getName() + "!");
        killer.sendMessage("&8-   &7Toggle these messages with /debug");
      }
    }
    e.getDrops().clear();
  }

  @EventHandler()
  public void onPlayerRespawn(PlayerRespawnEvent e) {
    try {
      init(e.getPlayer());
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }

  @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
  public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
    try {
      User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName());
      if (user.isBanned() && !e.getMessage().split(" ")[0].equalsIgnoreCase("/register")) {
        user.sendMessage("&l&cYou are banned from our servers, you can only use the lobby to register a forum account with &e/register");
        e.setCancelled(true);
      }
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }

  @EventHandler()
  public void onPlayerDropItem(PlayerPickupItemEvent e) {
    if (!e.getPlayer().isOp()) {
      e.setCancelled(true);
    }
  }

  @EventHandler()
  public void onPlayerDropItem(PlayerDropItemEvent e) {
    if (!e.getPlayer().isOp()) {
      e.setCancelled(true);
    }
  }

  @EventHandler()
  public void onPlayerMove(PlayerMoveEvent e) {
    if (e.getTo().getBlock().getType() == Material.PORTAL) {
      e.setCancelled(true);
      e.getPlayer().teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
      try {
        User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName());
        if (!user.isBanned()) {
          String server = getServerName(e.getTo());
          if (!server.isEmpty()) {
            e.getPlayer().teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
            if ((server.toLowerCase().contains("donor") && !(user.getSecondaryRank().getDonationRequired() > 0 || user.getPrimaryRank().isStaff()))) {
              user.sendMessage("&6You do not have permission to enter a donor server");
            } else {
              user.sendMessage("&6Moving you over to the " + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(server).getFriendlyName() + " server");
              _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "move " + e.getPlayer().getName() + " " + server);
            }
          } else {
            _plugin.getAPI().getItemMenuManager().addMenu(new ServerMenu(_plugin, e.getPlayer().getName()));
          }
        }
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    }
    if (e.getTo().getY() <= 30) {
      e.setCancelled(true);
      e.getPlayer().setFallDistance(0.0F);
      e.getPlayer().teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
    }
  }

  @EventHandler()
  public void onPlayerPortalEvent(PlayerPortalEvent e) {
    e.setCancelled(true);

    try {
      User user = ForsakenGlobal.getInstance().getAPI().getFactoryManager().getUserFactory().get(e.getPlayer().getName());
      if (!user.isBanned()) {
        String server = getServerName(e.getTo());
        if (!server.isEmpty()) {
          e.getPlayer().teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
          if ((server.toLowerCase().contains("donor") && !(user.getSecondaryRank().getDonationRequired() > 0 || user.getPrimaryRank().isStaff()))) {
            user.sendMessage("&6You do not have permission to enter a donor server");
          } else {
            user.sendMessage("&6Moving you over to the " + ForsakenGlobal.getInstance().getAPI().getFactoryManager().getServerFactory().get(server).getFriendlyName() + " server");
            _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "move " + e.getPlayer().getName() + " " + server);
          }
        } else {
          _plugin.getAPI().getItemMenuManager().addMenu(new ServerMenu(_plugin, e.getPlayer().getName()));
          e.getPlayer().teleport(new Location(_plugin.getServer().getWorld("world"), -125.5D, 102D, -78.5D, 0F, 0F));
        }
      }
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }

  public String getTopDonor() {
    DatabaseResults query = ForsakenGlobal
        .getInstance()
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT zu.name as name, SUM( vs.amount ) AS count FROM `forsaken_user_donations` vs, `forsaken_users` zu WHERE zu.id = vs.user_id AND (YEAR(`date`) = YEAR(NOW( )) AND MONTH(`date`) = MONTH(NOW( ))) GROUP BY vs.user_id  ORDER BY count DESC LIMIT 1");
    if (query != null && query.hasRows()) {
      try {
        return query.getString(0, "name");
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    }
    return "vanZeben";
  }

  public String getTopVoter() {
    DatabaseResults query = ForsakenGlobal
        .getInstance()
        .getAPI()
        .getDatabase()
        .readEnhanced(
            "SELECT zu.name as name, COUNT( * ) AS count FROM `forsaken_vote_sites` vs, `forsaken_user_votes` uv, `forsaken_users` zu WHERE zu.id = uv.user_id AND (YEAR(`date`) = YEAR(NOW( )) AND MONTH(`date`) = MONTH(NOW( ))) GROUP BY uv.user_id  ORDER BY count DESC LIMIT 1");
    if (query != null && query.hasRows()) {
      try {
        return query.getString(0, "name");
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    }
    return "vanZeben";
  }

  public void initNPC() {
    // TOP DONOR
    ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "npc sel 48");
    ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "npc rename " + getTopDonor());
    // TOP VOTER
    ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "npc sel 50");
    ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "npc rename " + getTopVoter());
  }

  private static void initLocations() {
    _serverLocations.put("dw_main", new Location[] { new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 101, -65), new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 107, -59) });
    _onlineStatus.put("dw_main", false);
    _serverLocations.put("bteam_main", new Location[] { new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -132, 101, -65), new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -132, 107, -59) });
    _onlineStatus.put("dw_donor", false);
    _serverLocations.put("dw_donor", new Location[] { new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 101, -57), new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 107, -51) });
    _onlineStatus.put("as_main", false);
    _serverLocations.put("bnb_main", new Location[] { new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -132, 101, -57), new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -132, 107, -51) });
    _onlineStatus.put("bnb_main", false);
    _serverLocations.put("as_main", new Location[] { new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 101, -49), new Location(ForsakenGlobal.getInstance().getServer().getWorld("world"), -118, 107, -43) });
    _onlineStatus.put("bteam_main", false);
    // _serverLocations.put("", new Location[] { new
    // Location( ForsakenGlobal.getInstance().getServer().getWorld("world"),
    // -132, 101, -65), new
    // Location( ForsakenGlobal.getInstance().getServer().getWorld("world"),
    // -132, 107, -59) });
    // _serverLocations.put("", new Location[] { new
    // Location( ForsakenGlobal.getInstance().getServer().getWorld("world"),
    // -118, 101, -65), new
    // Location( ForsakenGlobal.getInstance().getServer().getWorld("world"),
    // -118, 107, -59) });
  }

  public static void updateServer(Server s) {
    if (s.isOnline() == _onlineStatus.get(s.getName())) {
      return;
    } else {
      _onlineStatus.put(s.getName(), s.isOnline());
    }
    if (s.isOnline()) {
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 1 &" + s.getTextColour() + "&l" + s.getFriendlyName() + "!");
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 2 &" + s.getTextColour() + s.getFriendlyIp());
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 3 &5Version: &d" + s.getVersion());
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 4 &a{online: " + s.getName() + "}/128");
      WorldEditUtils.replaceAreaWithBlock("world", _serverLocations.get(s.getName())[0], _serverLocations.get(s.getName())[1], "portal", "air");
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage("&8[&l&c!&r&8] &l&" + s.getTextColour() + s.getFriendlyName() + " &ahas started back up!", false, true);
    } else {
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 1 &" + s.getTextColour() + "&l" + s.getFriendlyName() + "!");
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 2 &" + s.getTextColour() + s.getFriendlyIp());
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 3 &5Version: &d" + s.getVersion());
      ForsakenGlobal.getInstance().getServer().dispatchCommand(ForsakenGlobal.getInstance().getServer().getConsoleSender(), "hd setline " + s.getName() + " 4 &cCurrently Offline");
      WorldEditUtils.replaceAreaWithBlock("world", _serverLocations.get(s.getName())[0], _serverLocations.get(s.getName())[1], "air", "portal");
      ForsakenGlobal.getInstance().getAPI().getMessageManager().sendGlobalMessage("&8[&l&c!&r&8] &l&" + s.getTextColour() + s.getFriendlyName() + " &chas gone down :(", false, true);
    }
  }

  public String getServerName(Location loc) {
    for (String s : _serverLocations.keySet()) {
      Location[] locs = _serverLocations.get(s);
      if (locs[0].getBlockX() <= loc.getBlockX() && loc.getBlockX() <= locs[1].getBlockX() &&
          locs[0].getBlockY() <= loc.getBlockY() && loc.getBlockY() <= locs[1].getBlockY() &&
          locs[0].getBlockZ() <= loc.getBlockZ() && loc.getBlockZ() <= locs[1].getBlockZ()) { return s; }
    }
    return "";
  }
}
