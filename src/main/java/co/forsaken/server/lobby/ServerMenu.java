/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.server.lobby;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.factories.obj.Server;
import co.forsaken.api.obj.BaseItemMenu;
import co.forsaken.api.utils.ItemUtils;
import co.forsaken.api.utils.MessageUtils;

public class ServerMenu extends BaseItemMenu {

  public ServerMenu(ForsakenGlobal plugin, String playerName) {
    super(plugin, "Choose a server", playerName);
  }

  @Override
  protected void loadItems() {
    ItemStack item = new ItemStack(159);
    ArrayList<String> lore = new ArrayList<String>();
    for (Server s : _plugin.getAPI().getFactoryManager().getServerFactory().getAll()) {
      if (s.getName().equals(_plugin.getServer().getMotd()) || (s.getName().contains("donor") &&
          !(getUser().getSecondaryRank().getDonationRequired() > 0 || getUser().getPrimaryRank().isStaff()))) {
        continue;
      }
      ItemStack item2 = item.clone();
      if (s.isOnline()) {
        double tps = s.getTPS();
        short tpsColor = 5;
        char lagColour = 'a';
        char baseLagColour = '2';
        if (tps >= 18D) {
          tpsColor = 5;
          lagColour = 'a';
          baseLagColour = '2';
        } else if (tps >= 10.5) {
          tpsColor = 4;
          lagColour = 'e';
          baseLagColour = '6';
        } else {
          tpsColor = 14;
          lagColour = 'c';
          baseLagColour = '4';
        }
        int tpsPercent = 100 - ((int) Math.round((tps / 20D) * 100D));
        int playerCount = s.getPlayerCount();
        int maxPlayers = s.getMaxPlayers();
        char playerColour = 'a';
        char basePlayerColour = '2';
        if ((double) playerCount / (double) maxPlayers <= 0.40D) {
          playerColour = 'a';
          basePlayerColour = '2';
        } else if ((double) playerCount / (double) maxPlayers <= 0.60D) {
          playerColour = 'e';
          basePlayerColour = '6';
        } else {
          playerColour = 'c';
          basePlayerColour = '4';
        }
        item2.setDurability(tpsColor);
        lore.add("&7(Click to join the server)");
        lore.add("&a");

        lore.add("&5Direct IP: &d" + s.getFriendlyIp());
        lore.add("&5Version: &d" + s.getVersion());
        lore.add("&a");
        lore.add("&" + basePlayerColour + "Players: &" + playerColour + playerCount + "/" + maxPlayers);
        lore.add("&" + baseLagColour + "Lag: &" + lagColour + tpsPercent + "%");
        addItem(ItemUtils.getItemName(item2, MessageUtils.format("&fConnect to " + s.getFriendlyName(), true), lore));
        lore.clear();
      } else {

        item2.setDurability((short) 14);
        lore.add("&cThis server is offline");
        lore.add("&a");
        lore.add("&4Staff have been notified");
        addItem(ItemUtils.getItemName(item2, MessageUtils.format("&f" + s.getFriendlyName(), true), lore));
        lore.clear();
      }
    }
  }

  @Override
  public boolean clickSlot(int slot, ItemStack itemInSlot, ItemStack itemInHand, boolean isShiftClick, boolean isRightClick, boolean isLeftClick) {
    if (itemInSlot != null && itemInSlot.getItemMeta() != null && itemInSlot.getItemMeta().getLore().size() >= 3) {
      for (Server s : _plugin.getAPI().getFactoryManager().getServerFactory().getAll()) {
        if (s.getFriendlyName().equalsIgnoreCase(itemInSlot.getItemMeta().getDisplayName().substring(itemInSlot.getItemMeta().getDisplayName().indexOf("Connect to ") + 11))) {
          if (s.isOnline()) {
            _plugin.getServer().dispatchCommand(_plugin.getServer().getConsoleSender(), "move " + playerName + " " + s.getName());
          } else {
            getUser().sendMessage("&cThis server is currently offline. The staff have been notified");
          }
          break;
        }
      }
    }
    return false;
  }

  @Override
  protected void itemMissing(ItemStack item) {
  }

}
