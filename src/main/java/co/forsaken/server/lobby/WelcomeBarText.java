/*
 * This file is part of ForsakenGlobal.
 *
 * Copyright © 2012-2013,
 * 									ForsakenNetwork LLC
 * 									<http://www.forsaken.com/>
 * ForsakenGlobal is licensed under the Forsaken Network License Version 1
 *
 * ForsakenGlobal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the Forsaken Network License Version 1.
 *
 * ForsakenGlobal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the Forsaken Network License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License.
 */
package co.forsaken.server.lobby;

import org.bukkit.entity.Player;

import co.forsaken.ForsakenGlobal;
import co.forsaken.api.utils.BossbarUtils;
import co.forsaken.api.utils.MessageUtils;

public class WelcomeBarText implements Runnable {

  private static String _baseMessage = "&l&%colour%FORSAKEN &r&f- Welcome to our realm!";
  private static char[] _colours     = { 'b', '3', '9', '5', 'd', 'c', '6', 'e', 'a' };
  private static long   _time        = Math.round((40 / _colours.length));
  private int           _index       = 0;

  public WelcomeBarText(ForsakenGlobal plugin) {
    setText(_baseMessage.replaceAll("%colour%", _colours[_index % _colours.length] + ""));
    plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, this, 0, _time);
  }

  @Override
  public void run() {
    setText(_baseMessage.replaceAll("%colour%", _colours[_index % _colours.length] + ""));
    _index++;
  }

  public void setText(String text) {
    for (Player p : ForsakenGlobal.getInstance().getServer().getOnlinePlayers()) {
      BossbarUtils.displayBar(p, MessageUtils.format(text, true), 100F, 0, true);
    }
  }
}
