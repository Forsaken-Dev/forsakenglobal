<h1>Forsaken Suite</h1>

<h3>Welcome to Forsaken Suite</h3>

This is the suite of general purpose plugins running on the Forsaken Network modded servers.

Contributions are welcome in the form of pull requests, or the reporting of bugs through the issue tracker.

That is all.<br>Goodbye.
